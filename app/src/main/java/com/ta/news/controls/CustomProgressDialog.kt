package com.ta.news.controls

import android.animation.ObjectAnimator
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.animation.AccelerateDecelerateInterpolator
import com.ta.news.R
import kotlinx.android.synthetic.main.custom_progress_dialog.*


/**
 * Created by arthtilva on 25-Apr-17.
 */
class CustomProgressDialog(context: Context) : Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.custom_progress_dialog)

        val animation = ObjectAnimator.ofFloat(loader, "rotationY", 0.0f, 360f)
        animation.duration = 2400
        animation.repeatCount = ObjectAnimator.INFINITE
        animation.interpolator = AccelerateDecelerateInterpolator()
        animation.start()

    }

    fun setTitle(message: String?) {
        tv_title.visibility = View.VISIBLE
        tv_title.text = message
    }

    override fun setTitle(string: Int) {
        tv_title.visibility = View.VISIBLE
        tv_title.setText(string)
    }
}