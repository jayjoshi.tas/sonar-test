package com.ta.news.controls

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Typeface
import android.os.Build
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatCheckBox
import com.ta.news.R

class CCheckbox : AppCompatCheckBox {
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        applyCustomFont(context, attrs)
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        applyCustomFont(context, attrs)
    }

    private fun applyCustomFont(context: Context, attrs: AttributeSet) {
        val textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL)
        typeface = selectTypeface(context, textStyle)
        isAllCaps = false
    }

    private fun selectTypeface(context: Context, textStyle: Int): Typeface {
        return when (textStyle) {
            Typeface.BOLD -> Typeface.createFromAsset(context.assets, context.getString(R.string.bold))
            Typeface.NORMAL -> Typeface.createFromAsset(context.assets, context.getString(R.string.light)) // regular
            else -> Typeface.createFromAsset(context.assets, context.getString(R.string.light)) // regular
        }
    }

    companion object {
        const val ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android"
    }
}