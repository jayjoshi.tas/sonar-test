package com.ta.news.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ta.news.R
import kotlinx.android.synthetic.main.row_supported_gateway.view.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class SupportAdapter(var activity: Activity) : RecyclerView.Adapter<SupportAdapter.ItemsViewHolder>() {
    override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
        when (position) {
            0 -> itemsViewHolder.itemView.image.setImageResource(R.drawable.visa)
            1 -> itemsViewHolder.itemView.image.setImageResource(R.drawable.paytm)
            2 -> itemsViewHolder.itemView.image.setImageResource(R.drawable.phonepe)
            3 -> itemsViewHolder.itemView.image.setImageResource(R.drawable.gpay)
            4 -> itemsViewHolder.itemView.image.setImageResource(R.drawable.mastercard)
            5 -> itemsViewHolder.itemView.image.setImageResource(R.drawable.jiomoney)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemsViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.row_supported_gateway, viewGroup, false)
        return ItemsViewHolder(v)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return 4
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}