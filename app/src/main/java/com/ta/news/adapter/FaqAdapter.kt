package com.ta.news.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.ta.news.R
import com.ta.news.pojo.FaqPojo
import kotlinx.android.synthetic.main.row_faq.view.*
import kotlinx.android.synthetic.main.row_plans.view.mainLayout

/**
 * Created by arthtilva on 17-10-2016.
 */
class FaqAdapter(var activity: Activity, var list: ArrayList<FaqPojo>) : RecyclerView.Adapter<FaqAdapter.ItemsViewHolder>() {

    override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
        val plan = list[position]
        itemsViewHolder.itemView.question.text = plan.question
        itemsViewHolder.itemView.answer.text = plan.answer

        itemsViewHolder.itemView.mainLayout.setOnClickListener {
            if (itemsViewHolder.itemView.answer.isVisible) {
                itemsViewHolder.itemView.expand.setImageResource(R.drawable.expand)
            } else {
                itemsViewHolder.itemView.expand.setImageResource(R.drawable.minimize)
            }
            itemsViewHolder.itemView.answer.isVisible = !itemsViewHolder.itemView.answer.isVisible
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemsViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.row_faq, viewGroup, false)
        return ItemsViewHolder(v)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}