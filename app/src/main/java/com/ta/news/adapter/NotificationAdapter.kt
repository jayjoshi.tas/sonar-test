package com.ta.news.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.ta.news.R
import com.ta.news.activity.post.NewsDetailActivity
import com.ta.news.activity.NotificationDetailActivity
import com.ta.news.pojo.Notification
import kotlinx.android.synthetic.main.row_notification.view.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class NotificationAdapter(var activity: Activity, var list: List<Notification>) : RecyclerView.Adapter<NotificationAdapter.ItemsViewHolder>() {
    override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
        val `object` = list[position]
        val d = Date(`object`.time)
        val dateFormat = SimpleDateFormat("dd-MM-yyyy")
        val date = dateFormat.format(d)
        itemsViewHolder.itemView.tvTime.text = date
        itemsViewHolder.itemView.tvMessage.text = `object`.message
        itemsViewHolder.itemView.mainLayout.setBackgroundColor(if (`object`.isRead) ContextCompat.getColor(activity, R.color.white) else ContextCompat.getColor(activity, R.color.t_grey))
        itemsViewHolder.itemView.mainLayout.setOnClickListener {
            if (`object`.postId != -1) { //Toast.makeText(activity, object.postId + "==", Toast.LENGTH_SHORT).show();
                activity.startActivity(
                        Intent(activity, NewsDetailActivity::class.java)
                                .putExtra("openCmd", 6)
                                .putExtra("postId", `object`.postId)
                )
            } else {
                activity.startActivity(Intent(
                        activity, NotificationDetailActivity::class.java)
                        .putExtra("id", `object`.id)
                )
            }
        }


    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemsViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.row_notification, viewGroup, false)
        return ItemsViewHolder(v)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}