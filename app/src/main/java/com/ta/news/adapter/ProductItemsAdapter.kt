package com.ta.news.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ta.news.R
import com.ta.news.activity.product.ProductDetailsActivity
import com.ta.news.pojo.ProductPojo
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.row_product_item.view.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class ProductItemsAdapter(var activity: Activity, var list: List<ProductPojo>) : RecyclerView.Adapter<ProductItemsAdapter.ItemsViewHolder>() {
    override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
        Glide.with(activity).load(StoreUserData(activity).getString(Constants.URL) + list.get(position).image.get(0).image).into(itemsViewHolder.itemView.image)
        itemsViewHolder.itemView.tvName.text = list.get(position).name
        itemsViewHolder.itemView.tvPrice.text = "₹ ${list.get(position).salePrice}"
        itemsViewHolder.itemView.setOnClickListener {
            activity.startActivity(Intent(activity, ProductDetailsActivity::class.java).putExtra("pojo", list[0]))
        }

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemsViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.row_product_item, viewGroup, false)
        return ItemsViewHolder(v)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}