package com.ta.news.adapter

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.messaging.FirebaseMessaging
import com.ta.news.R
import com.ta.news.controls.CustomDialog
import com.ta.news.activity.auth.PhoneAuthActivity
import com.ta.news.pojo.StateList.StatePojo
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.row_state.view.*
import java.util.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class StateAdapter(var activity: Activity, var list: ArrayList<StatePojo>) : RecyclerView.Adapter<StateAdapter.ItemsViewHolder>() {
    var storeUserData = StoreUserData(activity)

    override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
        val pojo = list[position]
        itemsViewHolder.itemView.tvTitle.text = "${pojo.lcl} - ${pojo.nm}"
        itemsViewHolder.itemView.mainLayout.setOnClickListener {
            if (!pojo.live) {
                val dialog = CustomDialog(activity)
                dialog.show()
                dialog.setCancelable(false)
                dialog.setMessage(pojo.down)
                dialog.setPositiveButton(R.string.ok, View.OnClickListener { v: View? -> activity.finish() })
            } else {
                FirebaseMessaging.getInstance().subscribeToTopic(pojo.nm)
               // storeUserData.setString(Constants.USER_LANGUAGE, pojo.lng)
                Log.i("TAG", "onBindViewHolder: "+pojo.nm)
                storeUserData.setString(Constants.USER_STATE, "Gujarat")
                storeUserData.setString(Constants.URL, pojo.url)
                /**
                 * STAGING
                 * */
                //storeUserData.setString(Constants.URL,"http://5.9.62.174/staging/")
                activity.startActivity(Intent(activity, PhoneAuthActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
            }
        }


    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemsViewHolder {
        return ItemsViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.row_state, viewGroup, false))
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


}