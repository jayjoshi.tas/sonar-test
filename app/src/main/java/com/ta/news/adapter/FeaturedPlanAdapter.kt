package com.ta.news.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.ta.news.R
import com.ta.news.activity.plan.SelectFeaturedPostActivity
import com.ta.news.pojo.PlansPojo
import kotlinx.android.synthetic.main.row_featured_plans.view.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class FeaturedPlanAdapter(var activity: Activity, var list: ArrayList<PlansPojo>) : RecyclerView.Adapter<FeaturedPlanAdapter.ItemsViewHolder>() {

    override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
        val plan = list[position]
        itemsViewHolder.itemView.tvTitle.text = plan.planName
        itemsViewHolder.itemView.tvPrice.text = "₹" + plan.price
        itemsViewHolder.itemView.tvDays.text = plan.planDay + activity.getString(R.string.days)
        itemsViewHolder.itemView.tvRecommended.isVisible = plan.isRecommended == 1
        itemsViewHolder.itemView.mainLayout.setOnClickListener {
            activity.startActivity(Intent(activity, SelectFeaturedPostActivity::class.java).putExtra("featuredPlan", plan))
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemsViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.row_featured_plans, viewGroup, false)
        return ItemsViewHolder(v)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}