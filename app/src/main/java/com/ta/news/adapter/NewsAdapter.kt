package com.ta.news.adapter

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.activeandroid.query.Select
import com.bumptech.glide.Glide
import com.facebook.ads.AdOptionsView
import com.facebook.ads.NativeAd
import com.facebook.ads.NativeAdsManager
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.firebase.analytics.FirebaseAnalytics
import com.ta.news.R
import com.ta.news.activity.*
import com.ta.news.activity.auth.ProfileActivity
import com.ta.news.activity.post.CommentActivity
import com.ta.news.activity.post.NewsDetailActivity
import com.ta.news.activity.post.SpamActivity
import com.ta.news.pojo.NewsPojo
import com.ta.news.pojo.OfflineMedia
import com.ta.news.pojo.OfflineNews
import com.ta.news.utils.Constants
import com.ta.news.utils.OnLoadMoreListener
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.row_facebook_native_ad.view.*
import kotlinx.android.synthetic.main.row_google_native_ad.view.*
import kotlinx.android.synthetic.main.row_news.view.*
import kotlinx.android.synthetic.main.row_news_ad.view.*


/**
 * Created by arthtilva on 17-10-2016.
 */
class NewsAdapter(var activity: Activity, mRecyclerView: RecyclerView,
                  var list: ArrayList<NewsPojo>, var categoryId: Int,
                  var nativeAdsManager: NativeAdsManager) : RecyclerView.Adapter<RecyclerView.ViewHolder?>() {
    var unifiedNativeAd = ArrayList<UnifiedNativeAd>()
    var googleAd = 0
    private val VIEW_TYPE_ITEM = 0
    private val VIEW_TYPE_AD = 1
    private var mOnLoadMoreListener: OnLoadMoreListener? = null
    private var isLoading = false
    private val visibleThreshold = 5
    private var lastVisibleItem = 0
    private var totalItemCount = 0

    fun setOnLoadMoreListener(mOnLoadMoreListener: OnLoadMoreListener?) {
        this.mOnLoadMoreListener = mOnLoadMoreListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_AD -> {
                val view = LayoutInflater.from(activity).inflate(R.layout.row_news_ad, parent, false)
                AdViewHolder(view)
            }
            else -> {
                val view = LayoutInflater.from(activity).inflate(R.layout.row_news, parent, false)
                ItemsViewHolder(view)
            }
        }

    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return if (list == null) 0 else list.size
    }

    fun setLoaded() {
        isLoading = false
    }

    override fun getItemViewType(position: Int): Int {
        return if (list[position].isAd) VIEW_TYPE_AD else VIEW_TYPE_ITEM
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    class AdViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


    init {
        val linearLayoutManager = mRecyclerView.layoutManager as LinearLayoutManager?
        mRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                totalItemCount = linearLayoutManager!!.itemCount
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()
                if (!isLoading && totalItemCount <= lastVisibleItem + visibleThreshold) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener!!.onLoadMore()
                    }
                    isLoading = true
                }
            }
        })
    }

    override fun onBindViewHolder(itemsViewHolder: RecyclerView.ViewHolder, position: Int) {
        if (itemsViewHolder is ItemsViewHolder && !list[position].isAd) {
            val newsPojo = list[position]
            if (newsPojo.firstName != null && newsPojo.firstName.isNotEmpty()) {
                itemsViewHolder.itemView.tvName.text = newsPojo.firstName + " " + newsPojo.lastName
            } else {
                itemsViewHolder.itemView.tvName.text = StoreUserData(activity).getString(Constants.USER_FIRST_NAME) + " " + StoreUserData(activity).getString(Constants.USER_LAST_NAME)
            }
            itemsViewHolder.itemView.tvName.setOnClickListener { v: View? -> activity.startActivity(Intent(activity, ProfileActivity::class.java).putExtra("userId", newsPojo.userId)) }

            Utils.loadUserImage(activity, if (newsPojo.profileImage != null) newsPojo.profileImage.replace(" ".toRegex(), "%20") else "", itemsViewHolder.itemView.profileImage)
            itemsViewHolder.itemView.tvTitle.text = newsPojo.newsTitle
            itemsViewHolder.itemView.tvDescription.text = newsPojo.newsDetails
            itemsViewHolder.itemView.tvDate.text = newsPojo.addDate
            itemsViewHolder.itemView.image.visibility = View.GONE
            itemsViewHolder.itemView.viewPager.adapter = NewsMediaImageAdapter(activity, newsPojo.mediaArray)
            itemsViewHolder.itemView.viewPager.setHasFixedSize(true)
            itemsViewHolder.itemView.viewPager.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            if (newsPojo.mediaArray != null && newsPojo.mediaArray.size > 0) {
                itemsViewHolder.itemView.viewPager.visibility = View.VISIBLE
            } else {
                itemsViewHolder.itemView.viewPager.visibility = View.GONE
            }
            if (newsPojo.isVerified) {
                itemsViewHolder.itemView.tvName.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(activity, R.drawable.verified), null)
            } else {
                itemsViewHolder.itemView.tvName.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
            }
            itemsViewHolder.itemView.ivSave.setOnClickListener {
                val news = Select().from(OfflineNews::class.java).where("postId = ?", newsPojo!!.postId).executeSingle<OfflineNews>()
                if (news != null) {
                    (activity as MainActivity).showAlert(activity, activity.getString(R.string.news_already_saved))
                } else {
                    val offlineNews = OfflineNews()
                    offlineNews.newsDetails = newsPojo.newsDetails
                    offlineNews.newsTitle = newsPojo.newsTitle
                    offlineNews.postId = newsPojo.postId
                    offlineNews.addDate = newsPojo.addDate
                    offlineNews.firstName = newsPojo.firstName
                    offlineNews.lastName = newsPojo.lastName
                    offlineNews.mobileNo = newsPojo.mobileNo
                    offlineNews.address = newsPojo.address
                    offlineNews.approxPrice = newsPojo.approxPrice
                    offlineNews.profileImage = newsPojo.profileImage
                    offlineNews.isVerified = newsPojo.isVerified
                    offlineNews.userId = newsPojo.userId
                    Log.i("TAG", "onClick: " + newsPojo.firstName + "\n" + newsPojo.lastName + "\n" + newsPojo.mobileNo)
                    offlineNews.save()
                    for (media in newsPojo.mediaArray) {
                        var offlineMedia = OfflineMedia()
                        offlineMedia.postId = newsPojo.postId
                        offlineMedia.folderPath = media.folderPath
                        offlineMedia.mediaName = media.mediaName
                        offlineMedia.thumbFolderPath = media.thumbFolderPath
                        offlineMedia.thumbImage = media.thumbImage
                        offlineMedia.type = media.type
                        offlineMedia.save()
                    }
                    (activity as MainActivity).showAlert(activity, activity.getString(R.string.news_saved_successfully))
                }
            }
            itemsViewHolder.itemView.ivComment.setOnClickListener {
                activity.startActivity(Intent(activity, CommentActivity::class.java)
                        .putExtra("position", position)
                        .putExtra("pojo", newsPojo))
            }
            itemsViewHolder.itemView.btnShare.setOnClickListener {
                Utils.openShare(activity, newsPojo)
            }

            itemsViewHolder.itemView.mainLayout.setOnClickListener {
                activity.startActivity(Intent(activity, NewsDetailActivity::class.java)
                        .putExtra("position", position)
                        .putExtra("categoryId", categoryId)
                        .putExtra("pojo", newsPojo)
                )
            }
            Log.i("TAG", "onBindViewHolder: " + newsPojo.userId)
            Log.i("TAG", "onBindViewHolder: " + StoreUserData(activity).getString(Constants.USER_ID))

            itemsViewHolder.itemView.spam.isVisible = newsPojo.userId.toString() != StoreUserData(activity).getString(Constants.USER_ID)

            itemsViewHolder.itemView.spam.setOnClickListener {
                activity.startActivity(Intent(activity, SpamActivity::class.java).putExtra("pojo", newsPojo))
            }
        } else if (itemsViewHolder is AdViewHolder && list[position].isAd) {
            //itemsViewHolder.itemView.adView.visibility = View.GONE
            itemsViewHolder.itemView.ad_container.visibility = View.GONE
            val adViewHolder = itemsViewHolder as AdViewHolder
            if (nativeAdsManager.isLoaded) {
                itemsViewHolder.itemView.ad_container.visibility = View.VISIBLE
                val ad: NativeAd? = nativeAdsManager.nextNativeAd()
                if (ad != null && !ad.isAdInvalidated) {
                    ad?.let { nonNullAd ->
                        list[position].isAdShown = true
                        if (adViewHolder.itemView.ad_container.childCount > 0) {
                            adViewHolder.itemView.ad_container.removeAllViews()
                        }
                        val view = LayoutInflater.from(activity).inflate(R.layout.row_facebook_native_ad, null)
                        adViewHolder.itemView.ad_container.addView(view)
                        view.native_ad_title.text = nonNullAd.advertiserName
                        view.native_ad_body.text = nonNullAd.adBodyText
                        view.native_ad_social_context.text = nonNullAd.adSocialContext
                        view.native_ad_call_to_action.text = nonNullAd.adCallToAction
                        view.native_ad_call_to_action.visibility = if (nonNullAd.hasCallToAction()) View.VISIBLE else View.INVISIBLE
                        val adOptionsView = AdOptionsView(activity, nonNullAd, view.fb_native_ad_container)
                        view.ad_choices_container.addView(adOptionsView, 0)

                        val clickableViews = ArrayList<View>()
                        clickableViews.add(view.native_ad_icon)
                        clickableViews.add(view.native_ad_media)
                        clickableViews.add(view.native_ad_call_to_action)
                        nonNullAd.registerViewForInteraction(
                                view.fb_native_ad_container,
                                view.native_ad_media,
                                view.native_ad_icon,
                                clickableViews
                        )
                    }

                    FirebaseAnalytics.getInstance(activity).logEvent("facebookAd", Utils.getUserDetails(activity))
                } else {
                    Log.i("TAG", "Facebook native ad is invalidated")
                }

            } else if (unifiedNativeAd.size > 0) {
                itemsViewHolder.itemView.ad_container.visibility = View.VISIBLE
                val ad: UnifiedNativeAd = unifiedNativeAd[googleAd]
                if (adViewHolder.itemView.ad_container.childCount > 0) {
                    adViewHolder.itemView.ad_container.removeAllViews()
                }
                val view = LayoutInflater.from(activity).inflate(R.layout.row_google_native_ad, null)
                adViewHolder.itemView.ad_container.addView(view)

                Glide.with(activity).load(ad.icon?.uri).error(R.drawable.app_icon).into(view.ad_icon)
                view.ad_headline.text = ad.headline
                view.ad_advertiser.text = ad.advertiser
                view.ad_body.text = ad.body
                view.ad_price.text = ad.price
                view.ad_store.text = ad.store
                view.ad_call_to_action.text = ad.callToAction
                view.ad_media.setMediaContent(ad.mediaContent)
                if (ad.starRating != null) {
                    view.ad_stars.isVisible = true
                    view.ad_stars.rating = ad.starRating.toFloat()
                } else {
                    view.ad_stars.isVisible = false
                }
                view.ad_call_to_action.visibility = if (ad.callToAction.isNotEmpty()) View.VISIBLE else View.INVISIBLE


                view.ad_view.mediaView = view.ad_media
                view.ad_view.callToActionView = view.ad_call_to_action
                view.ad_view.priceView = view.ad_price
                view.ad_view.bodyView = view.ad_body
                view.ad_view.advertiserView = view.ad_advertiser
                view.ad_view.storeView = view.ad_store
                view.ad_view.starRatingView = view.ad_stars
                view.ad_view.headlineView = view.ad_headline
                view.ad_view.iconView = view.ad_icon

                view.ad_view.setNativeAd(ad)
                googleAd++
                if (googleAd == unifiedNativeAd.size) {
                    googleAd = 0
                }
                FirebaseAnalytics.getInstance(activity).logEvent("googleAd", Utils.getUserDetails(activity))
            } else {
                /*itemsViewHolder.itemView.adView.visibility = View.VISIBLE
                itemsViewHolder.itemView.adView.loadAd(AdRequest.Builder().build())
                itemsViewHolder.itemView.adView.adListener = object : AdListener() {
                    override fun onAdFailedToLoad(p0: Int) {
                        super.onAdFailedToLoad(p0)
                        FirebaseAnalytics.getInstance(activity).logEvent("AdFailed", Utils.getUserDetails(activity))
                    }
                }*/


                /*AppLovinSdk.getInstance(activity).nativeAdService.loadNextAd(object : AppLovinNativeAdLoadListener {
                    override fun onNativeAdsLoaded(nativeAds: List<AppLovinNativeAd>) {
                        // List of one native ad loaded; do something with this, e.g. render into your custom view.
                    }

                    override fun onNativeAdsFailedToLoad(errorCode: Int) {
                        // Native ads failed to load for some reason, likely a network error.
                        // Compare errorCode to the available constants in AppLovinErrorCodes.
                        if (errorCode == AppLovinErrorCodes.NO_FILL) {
                            // No ad was available for this placement
                        }
                        // else if (errorCode == .... ) { ... }
                    }
                })*/
            }
        }
    }
}