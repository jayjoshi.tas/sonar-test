package com.ta.news.adapter

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ta.news.R
import com.ta.news.activity.ImageActivity
import com.ta.news.pojo.MediaPOJO
import com.ta.news.pojo.News
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.row_media.view.*
import java.util.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class MediaAdapter(var activity: Activity, var list: ArrayList<MediaPOJO>) : RecyclerView.Adapter<MediaAdapter.ItemsViewHolder>() {
    override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
        val `object` = list[position]
        Log.i("TAG", "onBindViewHolder: " + `object`.type)
        when (`object`.type) {
            News.IMAGE -> {
                if (`object`.thumbImage.isNullOrEmpty()) {
                    Glide.with(activity).load((StoreUserData(activity).getString(Constants.URL) + `object`.folderPath + `object`.mediaName).replace(" ".toRegex(), "%20")).into(itemsViewHolder.itemView.image)
                } else {
                    Glide.with(activity).load((StoreUserData(activity).getString(Constants.URL) + `object`.thumbFolderPath + `object`.thumbImage).replace(" ".toRegex(), "%20")).into(itemsViewHolder.itemView.image)
                }
            }
            News.AUDIO -> itemsViewHolder.itemView.image.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.audio))
            News.VIDEO -> itemsViewHolder.itemView.image.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.video))
            else -> {
            }
        }
        itemsViewHolder.itemView.image.setOnClickListener {
            if (`object`.type == News.IMAGE) {
                activity.startActivity(Intent(activity, ImageActivity::class.java)
                        .putExtra("list", list)
                        .putExtra("position", position)
                        .putExtra("cmd", Constants.CMD_NEWS)
                        //.putExtra("url", Constants.NEWS_BASE_URL + `object`.folderPath + `object`.mediaName)
                )
            } else {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                when (`object`.type) {
                    News.IMAGE -> intent.setDataAndType(Uri.parse(StoreUserData(activity).getString(Constants.URL) + `object`.folderPath + `object`.mediaName), "image/*")
                    News.AUDIO -> intent.setDataAndType(Uri.parse(StoreUserData(activity).getString(Constants.URL) + `object`.folderPath + `object`.mediaName), "audio/*")
                    News.VIDEO -> intent.setDataAndType(Uri.parse(StoreUserData(activity).getString(Constants.URL) + `object`.folderPath + `object`.mediaName), "video/*")
                    News.PDF -> intent.setDataAndType(Uri.parse(StoreUserData(activity).getString(Constants.URL) + `object`.folderPath + `object`.mediaName), "application/pdf")
                    else -> intent.setDataAndType(Uri.parse(StoreUserData(activity).getString(Constants.URL) + `object`.folderPath + `object`.mediaName), "*/*")
                }
                try {
                    activity.startActivity(intent)
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemsViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.row_media, viewGroup, false)
        return ItemsViewHolder(v)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}