package com.ta.news.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.ta.news.R
import com.ta.news.pojo.PlansPojo
import kotlinx.android.synthetic.main.row_plans.view.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class PlanAdapter(var activity: Activity, var list: ArrayList<PlansPojo>) : RecyclerView.Adapter<PlanAdapter.ItemsViewHolder>() {


    var selected = -1

    override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
        val plan = list[position]
        itemsViewHolder.itemView.tvTitle.text = plan.planName
        itemsViewHolder.itemView.tvPrice.text = "₹" + plan.price
        if (selected == -1 && plan.isRecommended == 1) {
            selected = position
        }
        itemsViewHolder.itemView.tvDetails.isVisible = plan.isRecommended == 1
        if (selected == position) {
            itemsViewHolder.itemView.mainLayout.setBackgroundColor(ContextCompat.getColor(activity, R.color.t_grey))
        } else {
            itemsViewHolder.itemView.mainLayout.setBackgroundColor(ContextCompat.getColor(activity, R.color.white))
        }

        itemsViewHolder.itemView.mainLayout.setOnClickListener {
            selected = position
            notifyDataSetChanged()
            // planSelect.onSelect(`object`)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemsViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.row_plans, viewGroup, false)
        return ItemsViewHolder(v)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}