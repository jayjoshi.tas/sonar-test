package com.ta.news.adapter

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ta.news.R
import com.ta.news.activity.ImageActivity
import com.ta.news.pojo.MediaPOJO
import com.ta.news.pojo.News
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.row_slider_image.view.*
import java.util.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class NewsMediaImageAdapter(var activity: Activity, var list: ArrayList<MediaPOJO>?) : RecyclerView.Adapter<NewsMediaImageAdapter.ItemsViewHolder>() {
    override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
        val pojo = list!![position]
        if (pojo.type == News.IMAGE) {
            if (pojo.thumbImage.isNullOrEmpty()) {
                Glide.with(activity).load((StoreUserData(activity).getString(Constants.URL) + pojo.folderPath + pojo.mediaName).replace(" ".toRegex(), "%20")).into(itemsViewHolder.itemView.image)
            } else {
                Glide.with(activity).load((StoreUserData(activity).getString(Constants.URL) + pojo.thumbFolderPath + pojo.thumbImage).replace(" ".toRegex(), "%20")).into(itemsViewHolder.itemView.image)
            }
        } else if (pojo.type == News.AUDIO) {
            itemsViewHolder.itemView.image.setImageResource(R.drawable.audio)
        } else if (pojo.type == News.VIDEO) {
            itemsViewHolder.itemView.image.setImageResource(R.drawable.video)
        }
        itemsViewHolder.itemView.image.setOnClickListener {

            if (pojo.type == News.IMAGE) {
                activity.startActivity(Intent(activity, ImageActivity::class.java)
                        .putExtra("list", list)
                        .putExtra("position", position)
                        .putExtra("cmd", Constants.CMD_NEWS)
                        //.putExtra("url", Constants.NEWS_BASE_URL + `object`.folderPath + `object`.mediaName)
                )
            } else {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                when (pojo.type) {
                    News.IMAGE -> intent.setDataAndType(Uri.parse(StoreUserData(activity).getString(Constants.URL) + pojo.folderPath + pojo.mediaName), "image/*")
                    News.AUDIO -> intent.setDataAndType(Uri.parse(StoreUserData(activity).getString(Constants.URL) + pojo.folderPath + pojo.mediaName), "audio/*")
                    News.VIDEO -> intent.setDataAndType(Uri.parse(StoreUserData(activity).getString(Constants.URL) + pojo.folderPath + pojo.mediaName), "video/*")
                    News.PDF -> intent.setDataAndType(Uri.parse(StoreUserData(activity).getString(Constants.URL) + pojo.folderPath + pojo.mediaName), "application/pdf")
                    else -> intent.setDataAndType(Uri.parse(StoreUserData(activity).getString(Constants.URL) + pojo.folderPath + pojo.mediaName), "*/*")
                }
                try {
                    activity.startActivity(intent)
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemsViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.row_slider_image, viewGroup, false)
        return ItemsViewHolder(v)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return if (list == null) 0 else list!!.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}