package com.ta.news.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.activeandroid.query.Select
import com.ta.news.R
import com.ta.news.controls.CustomDialog
import com.ta.news.fragment.SavedNewsFragment
import com.ta.news.activity.post.NewsDetailActivity
import com.ta.news.activity.auth.ProfileActivity
import com.ta.news.pojo.MediaPOJO
import com.ta.news.pojo.OfflineMedia
import com.ta.news.pojo.OfflineNews
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.row_saved_news.view.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class SavedNewsAdapter(var activity: Activity, var list: List<OfflineNews>?, var refreshListener: SavedNewsFragment.OnRefreshListener) : RecyclerView.Adapter<SavedNewsAdapter.ItemsViewHolder>() {
    override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
        val newsPojo = list!![position]
        if (newsPojo.firstName != null && newsPojo.firstName.isNotEmpty()) {
            itemsViewHolder.itemView.tvName.text = newsPojo.firstName + " " + newsPojo.lastName
        } else {
            itemsViewHolder.itemView.tvName.text = StoreUserData(activity).getString(Constants.USER_FIRST_NAME) + " " + StoreUserData(activity).getString(Constants.USER_LAST_NAME)
        }
        itemsViewHolder.itemView.tvName.setOnClickListener { v: View? -> activity.startActivity(Intent(activity, ProfileActivity::class.java).putExtra("userId", newsPojo.userId)) }

        Utils.loadUserImage(activity, if (newsPojo.profileImage != null) newsPojo.profileImage.replace(" ".toRegex(), "%20") else "", itemsViewHolder.itemView.profileImage)
        itemsViewHolder.itemView.tvTitle.text = newsPojo.newsTitle
        itemsViewHolder.itemView.tvDescription.text = newsPojo.newsDetails
        itemsViewHolder.itemView.tvDate.text = newsPojo.addDate
        var mediaArray = ArrayList<MediaPOJO>()
        var offList = Select().from(OfflineMedia::class.java).where("postId = ?", newsPojo!!.postId).execute<OfflineMedia>()
        if (offList != null) {
            for (media in offList) {
                var live = MediaPOJO()
                live.folderPath = media.folderPath
                live.mediaName = media.mediaName
                live.thumbFolderPath = media.thumbFolderPath
                live.thumbImage = media.thumbImage
                live.type = media.type
                mediaArray.add(live)
            }
        }

        itemsViewHolder.itemView.viewPager.adapter = NewsMediaImageAdapter(activity, mediaArray)
        itemsViewHolder.itemView.viewPager.setHasFixedSize(true)
        itemsViewHolder.itemView.viewPager.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        if (mediaArray != null && mediaArray.size > 0) {
            itemsViewHolder.itemView.viewPager.visibility = View.VISIBLE
        } else {
            itemsViewHolder.itemView.viewPager.visibility = View.GONE
        }
        if (newsPojo.isVerified) {
            itemsViewHolder.itemView.tvName.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(activity, R.drawable.verified), null)
        } else {
            itemsViewHolder.itemView.tvName.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
        }
        //onClick
        itemsViewHolder.itemView.mainLayout.setOnClickListener {
            activity.startActivity(
                    Intent(activity, NewsDetailActivity::class.java)
                            .putExtra("postId", newsPojo.postId)
                            .putExtra("isOffline", true)
            )
        }
        itemsViewHolder.itemView.ivDelete.setOnClickListener {
            if (newsPojo != null) {
                val dialog = CustomDialog(activity)
                dialog.show()
                dialog.setMessage(R.string.delete_confirmation)
                dialog.setPositiveButton(R.string.yes, View.OnClickListener {
                    dialog.dismiss()
                    val news = Select().from(OfflineNews::class.java).where("postId = ?", newsPojo.postId).executeSingle<OfflineNews>()
                    news?.delete()!!
                    val media = Select().from(OfflineMedia::class.java).where("postId = ?", newsPojo.postId).execute<OfflineMedia>()
                    if (media != null) {
                        for (m in media) {
                            m.delete()
                        }
                    }
                    refreshListener.onRefresh()
                })
                dialog.setNegativeButton(R.string.no, View.OnClickListener { dialog.dismiss() })
            }
        }

        /*itemsViewHolder.itemView.ivComment.setOnClickListener {
            activity.startActivity(Intent(activity, CommentActivity::class.java)
                    .putExtra("position", position)
                    .putExtra("pojo", newsPojo))
        }
        itemsViewHolder.itemView.btnShare.setOnClickListener {
            Utils.openShare(activity, newsPojo)
        }*/
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemsViewHolder {
        val view = LayoutInflater.from(activity).inflate(R.layout.row_saved_news, parent, false)
        return ItemsViewHolder(view)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return if (list == null) 0 else list!!.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}