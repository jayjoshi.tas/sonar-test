package com.ta.news.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.ta.news.R
import com.ta.news.activity.FeaturedPlanActivity
import com.ta.news.activity.plan.PlansActivity
import com.ta.news.pojo.PlanTypePojo
import kotlinx.android.synthetic.main.row_plans.view.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class PlanTypeAdapter(var activity: Activity, var list: ArrayList<PlanTypePojo>) : RecyclerView.Adapter<PlanTypeAdapter.ItemsViewHolder>() {

    override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
        val plan = list[position]
        itemsViewHolder.itemView.tvTitle.text = plan.title
        if (plan.amount != null && plan.amount > 0) {
            itemsViewHolder.itemView.tvPrice.isVisible = true
            itemsViewHolder.itemView.tvPrice.text = "₹" + plan.amount
        } else {
            itemsViewHolder.itemView.tvPrice.isVisible = false
        }
        itemsViewHolder.itemView.tvDetails.text = plan.description

        itemsViewHolder.itemView.mainLayout.setOnClickListener {
            when (plan.type) {
                "post" -> {
                    activity.startActivity(Intent(activity, PlansActivity::class.java))
                }
                "featuredPost" -> {
                    activity.startActivity(Intent(activity, FeaturedPlanActivity::class.java))
                }
                "removeAds" -> {
                }
            }
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemsViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.row_plans, viewGroup, false)
        return ItemsViewHolder(v)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}