package com.ta.news.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.ta.news.R;
import com.ta.news.pojo.Media;
import com.ta.news.utils.Constants;
import com.ta.news.utils.StoreUserData;

import java.util.List;

import androidx.viewpager.widget.PagerAdapter;


public class HomeSliderPagerAdapter extends PagerAdapter {

    Activity mContext;
    LayoutInflater mLayoutInflater;
    List<Media> slider;

    public HomeSliderPagerAdapter(Activity context, List<Media> slider) {
        mContext = context;
        this.slider = slider;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return slider.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.row_home_slider, container, false);
        ImageView imageView = itemView.findViewById(R.id.image);
        Glide.with(mContext).load(new StoreUserData(mContext).getString(Constants.URL) + slider.get(position).getImage()).into(imageView);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}