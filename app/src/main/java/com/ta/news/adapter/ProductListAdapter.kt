package com.ta.news.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ta.news.R
import com.ta.news.activity.product.CategoryProductActivity
import com.ta.news.pojo.ProductCategory
import kotlinx.android.synthetic.main.row_product_list.view.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class ProductListAdapter(var activity: Activity, var list: List<ProductCategory>) : RecyclerView.Adapter<ProductListAdapter.ItemsViewHolder>() {
    override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
        itemsViewHolder.itemView.tvCategory.text = list[position].name
        itemsViewHolder.itemView.rvProductList.adapter = ProductItemsAdapter(activity, list[position].productArray)
        itemsViewHolder.itemView.tvViewAll.setOnClickListener {
            activity.startActivity(Intent(activity, CategoryProductActivity::class.java)
                    .putExtra("category", list[position]))
        }

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemsViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.row_product_list, viewGroup, false)
        return ItemsViewHolder(v)

    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}