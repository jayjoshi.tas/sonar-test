package com.ta.news.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ta.news.R
import com.ta.news.activity.post.NewPostAddActivity
import kotlinx.android.synthetic.main.row_add_media.view.*
import java.io.File

/**
 * Created by arthtilva on 17-10-2016.
 */
class AddMediaAdapter(var activity: Activity, var list: List<String>, var itemClickListener: NewPostAddActivity.ItemClickListener) : RecyclerView.Adapter<AddMediaAdapter.ItemsViewHolder>() {
    override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
        val `object` = list[position]
        if (position == 0) {
            itemsViewHolder.itemView.image.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.upload))
        } else {
            if (`object`.isNotEmpty()) {
                val file = File(`object`)
                if (file.exists()) {
                    Glide.with(activity).load(File(`object`)).into(itemsViewHolder.itemView.image)
                }
            }
        }
        itemsViewHolder.itemView.mainLayout.setOnClickListener { v ->
            itemClickListener.onClick(v, position)

        }
        itemsViewHolder.itemView.delete.setOnClickListener { v ->
            itemClickListener.onClick(v, position)

        }
        if (`object` == "Add") {
            itemsViewHolder.itemView.delete.visibility = View.GONE
        } else {
            itemsViewHolder.itemView.delete.visibility = View.VISIBLE
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemsViewHolder {
        return ItemsViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.row_add_media, viewGroup, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}