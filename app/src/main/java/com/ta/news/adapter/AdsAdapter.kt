package com.ta.news.adapter

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ta.news.R
import com.ta.news.fragment.CallAdvOptionsFragment
import com.ta.news.fragment.WhatsAppFragment
import com.ta.news.pojo.News.Ads
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.row_ads.view.*
import java.util.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class AdsAdapter(var activity: Activity, var list: ArrayList<Ads>, var itemClickListener: WhatsAppFragment.ItemClickListener) : RecyclerView.Adapter<AdsAdapter.ItemsViewHolder>() {
    override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
        val `object` = list[position]
        Glide.with(activity).load(StoreUserData(activity).getString(Constants.URL) + `object`.folderPath + `object`.adImage!!.replace(" ".toRegex(), "%20")).into(itemsViewHolder.itemView.image)
        itemsViewHolder.itemView.mainLayout.setOnClickListener {
            val callAdvOptionsFragment = CallAdvOptionsFragment(`object`)
            var bundle = Bundle()
            callAdvOptionsFragment.arguments = bundle
            callAdvOptionsFragment.show(
                    (activity as AppCompatActivity).supportFragmentManager,
                    CallAdvOptionsFragment::class.java.simpleName
            )
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemsViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.row_ads, viewGroup, false)
        return ItemsViewHolder(v)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}