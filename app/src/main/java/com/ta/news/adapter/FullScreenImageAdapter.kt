package com.ta.news.adapter

import android.app.Activity
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.ta.news.R
import com.ta.news.activity.ImageActivity
import com.ta.news.pojo.MediaPOJO
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.row_full_screen_image.view.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class FullScreenImageAdapter(var activity: Activity, var list: ArrayList<MediaPOJO>, var itemClickListener: ImageActivity.ItemClickListener) : RecyclerView.Adapter<FullScreenImageAdapter.ItemsViewHolder>() {
    override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
        val pojo = list[position]
        var url = ""
        if (activity.intent.getIntExtra("cmd", -1) == Constants.CMD_NEWS) {
            url = (StoreUserData(activity).getString(Constants.URL) + pojo.folderPath + pojo.mediaName).replace(" ".toRegex(), "%20")
        } else if (activity.intent.getIntExtra("cmd", -1) == Constants.CMD_BOOK) {
            url = (StoreUserData(activity).getString(Constants.URL) + pojo.folderPath + pojo.mediaName).replace(" ".toRegex(), "%20")
        }
        Glide.with(activity).asBitmap().load(url).listener(object : RequestListener<Bitmap?> {
            override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Bitmap?>, isFirstResource: Boolean): Boolean {
                return false
            }

            override fun onResourceReady(resource: Bitmap?, model: Any, target: Target<Bitmap?>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                // bitmap = resource!!
                itemClickListener.onClick(resource!!)
                return false
            }
        }).into(itemsViewHolder.itemView.image)

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemsViewHolder {
        return ItemsViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.row_full_screen_image, viewGroup, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}