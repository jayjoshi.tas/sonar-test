package com.ta.news.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ta.news.R
import com.ta.news.activity.product.ProductDetailsActivity
import com.ta.news.pojo.ProductPojo
import com.ta.news.utils.Constants
import com.ta.news.utils.OnLoadMoreListener
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.row_product_item.view.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class CategoryProductAdapter(var activity: Activity, mRecyclerView: RecyclerView, var list: List<ProductPojo>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val VIEW_TYPE_ITEM = 0
    private val VIEW_TYPE_LOADING = 1
    lateinit var mOnLoadMoreListener: OnLoadMoreListener
    private var isLoading = false
    private val visibleThreshold = 5
    private var lastVisibleItem = 0
    private var totalItemCount = 0

    override fun onBindViewHolder(itemsViewHolder: RecyclerView.ViewHolder, position: Int) {

        Glide.with(activity).load(StoreUserData(activity).getString(Constants.URL) + list.get(position).image[0].image).into(itemsViewHolder.itemView.image)
        itemsViewHolder.itemView.tvName.text = list.get(position).name
        itemsViewHolder.itemView.tvPrice.text = "₹ ${list.get(position).salePrice}"

        itemsViewHolder.itemView.setOnClickListener {
            activity.startActivity(Intent(activity, ProductDetailsActivity::class.java).putExtra("pojo", list[0]))
        }

    }

    fun setOnLoadMoreListener(mOnLoadMoreListener: OnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_ITEM) {
            val view = LayoutInflater.from(activity).inflate(R.layout.row_product_viewall, parent, false)
            return ItemsViewHolder(view)
        } else {
            val view = LayoutInflater.from(activity).inflate(R.layout.layout_loading_item, parent, false)
            return LoadingViewHolder(view)
        }

    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return if (list == null) 0 else list.size
    }

    fun setLoaded() {
        isLoading = false
    }

    override fun getItemViewType(position: Int): Int {
        return if (list[position].isLoader) VIEW_TYPE_LOADING else VIEW_TYPE_ITEM
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private inner class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    init {
        val linearLayoutManager = mRecyclerView.layoutManager as GridLayoutManager?
        mRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                totalItemCount = linearLayoutManager!!.itemCount
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()
                if (!isLoading && totalItemCount <= lastVisibleItem + visibleThreshold) {
                    mOnLoadMoreListener.onLoadMore()
                    isLoading = true
                }
            }
        })
    }

}