package com.ta.news.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ta.news.R
import com.ta.news.activity.ImageActivity
import com.ta.news.pojo.MediaPOJO
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.row_ads.view.*
import java.util.*

/**
 * Created by arthtilva on 17-10-2016.
 */
class BookImagesAdapter(var activity: Activity, var list: ArrayList<MediaPOJO>) : RecyclerView.Adapter<BookImagesAdapter.ItemsViewHolder>() {
    override fun onBindViewHolder(itemsViewHolder: ItemsViewHolder, position: Int) {
        val `object` = list[position]
        Glide.with(activity).load(StoreUserData(activity).getString(Constants.URL)  + `object`.folderPath + `object`.images.replace(" ".toRegex(), "%20")).into(itemsViewHolder.itemView.image)
        itemsViewHolder.itemView.mainLayout.setOnClickListener {
            activity.startActivity(Intent(activity, ImageActivity::class.java)
                    .putExtra("cmd", Constants.CMD_BOOK)
                    .putExtra("position",position)
                    .putExtra("list", list)
                    //.putExtra("url", Constants.BOOKS_BASE_URL + `object`.folderPath + `object`.images)
            )
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ItemsViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.row_ads, viewGroup, false)
        return ItemsViewHolder(v)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}