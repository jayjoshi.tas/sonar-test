package com.ta.news.activity

import android.app.Dialog
import android.content.Context
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MenuItem
import android.view.Window
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.ta.news.R
import com.ta.news.utils.StoreUserData
import es.voghdev.pdfviewpager.library.RemotePDFViewPager
import es.voghdev.pdfviewpager.library.adapter.PDFPagerAdapter
import es.voghdev.pdfviewpager.library.remote.DownloadFile
import kotlinx.android.synthetic.main.activity_content_category.toolbar
import kotlinx.android.synthetic.main.activity_pdf.*
import kotlinx.android.synthetic.main.custom_dialog.btnPositive
import kotlinx.android.synthetic.main.gif_dialog.*


class PdfActivity : BaseActivity(), DownloadFile.Listener {
    lateinit var remotePDFViewPager: RemotePDFViewPager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = this
        storeUserData = StoreUserData(activity)
        setContentView(R.layout.activity_pdf)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val upArrow = ContextCompat.getDrawable(activity, R.drawable.left_arrow)
        upArrow?.setColorFilter(ContextCompat.getColor(activity, R.color.white), PorterDuff.Mode.SRC_ATOP)
        supportActionBar?.setHomeAsUpIndicator(upArrow)

        showProgress()
        remotePDFViewPager = RemotePDFViewPager(activity, intent.getStringExtra("url"), this)

        if (!storeUserData.getBoolean("showcase_pdf")) {
            GifDialog(activity, R.raw.swipe_left_crop, getString(R.string.swipe_left_message)).show()
        }

    }

    override fun onSuccess(url: String?, destinationPath: String?) {
        dismissProgress()
        pdfViewer.adapter = PDFPagerAdapter(this, destinationPath)
        remotePDFViewPager.adapter = pdfViewer.adapter
    }

    override fun onFailure(e: Exception?) {
        e?.printStackTrace()
    }

    override fun onProgressUpdate(progress: Int, total: Int) {
        Log.i("TAG", "onProgressUpdate: $progress $total")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean { //handle arrow click here
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    class GifDialog(context: Context, var gif: Int, var message: String) : Dialog(context) {

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(R.layout.gif_dialog)
            StoreUserData(context).setBoolean("showcase_pdf", true)
            tvMessage.text = message
            Glide.with(context)
                    .load(gif)
                    .into(image);
            btnPositive.setOnClickListener {
                dismiss()
                if (gif == R.raw.swipe_left_crop)
                    Handler().postDelayed({
                        GifDialog(context, R.raw.swipe_right_crop, context.getString(R.string.swipe_right_message)).show()
                    }, 700)
            }
        }
    }
}