package com.ta.news.activity.post

import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.gson.GsonBuilder
import com.ta.news.R
import com.ta.news.activity.BaseActivity
import com.ta.news.adapter.CommentAdapter
import com.ta.news.pojo.CommentPojo
import com.ta.news.pojo.Comments
import com.ta.news.pojo.NewsPojo
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.activity_comment.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.Reader
import java.io.StringReader
import java.lang.reflect.Modifier
import java.text.SimpleDateFormat
import java.util.*

class CommentActivity : BaseActivity() {

    var page = 1
    lateinit var layoutManager: LinearLayoutManager
    var totalPages = 1
    lateinit var adapter: CommentAdapter
    lateinit var newsPojo: NewsPojo
    var lstComments = ArrayList<CommentPojo>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comment)
        activity = this
        storeUserData = StoreUserData(activity)
        newsPojo = intent.getSerializableExtra("pojo") as NewsPojo
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val upArrow = ContextCompat.getDrawable(activity, R.drawable.left_arrow)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            upArrow?.colorFilter = BlendModeColorFilter(ContextCompat.getColor(activity, R.color.white), BlendMode.SRC_ATOP)
        } else {
            upArrow?.setColorFilter(ContextCompat.getColor(activity, R.color.white), PorterDuff.Mode.SRC_ATOP)
        }
        supportActionBar?.setHomeAsUpIndicator(upArrow)
        lstComments.clear()
        if (storeUserData.getString(Constants.USER_ID) == newsPojo.userId.toString()) {
            notification.visibility = View.VISIBLE
            notification.isChecked = newsPojo.notificationStatus == 1
        } else {
            notification.visibility = View.GONE
        }
        layoutManager = LinearLayoutManager(activity)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = layoutManager
        layoutManager.reverseLayout = true
        adapter = CommentAdapter(activity, recyclerView, lstComments, newsPojo)
        recyclerView.adapter = adapter
        btn_send.setOnClickListener {
            when {
                storeUserData.getInt(Constants.USER_STATUS) != 1 -> {
                    showAlert(activity, storeUserData.getString(Constants.REASON_MESSAGE))
                }
                Utils.isEmpty(ed_comment) -> {
                    showAlert(activity, getString(R.string.please_enter_comment))
                }
                else -> {
                    postComment()
                }
            }
        }
        adapter.setOnLoadMoreListener(object : CommentAdapter.OnLoadMoreListener {
            override fun onLoadMore() {
                if (page < totalPages) {
                    lstComments.add(CommentPojo(true))
                    Handler().post { adapter.notifyItemInserted(lstComments.size - 1) }
                    page += 1
                    comments
                    Log.i("TAG", "onCreateView: 3")
                }
            }
        })
        notification.setOnCheckedChangeListener { buttonView, isChecked -> commentStatus(isChecked) }
        comments
        loadBannerAds(banner_container, googleAdView, R.string.fb_comment_detail_banner)
    }

    fun postComment() {
        progressDialog.show()
        val result = Utils.CallApi(activity).addComment(
                storeUserData.getString(Constants.USER_ID),
                newsPojo.postId,
                newsPojo.userId,
                ed_comment.text.toString().trim(),
                storeUserData.getString(Constants.USER_FIRST_NAME) + " " + storeUserData.getString(Constants.USER_LAST_NAME),
                newsPojo.notificationStatus
        )
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (progressDialog.isShowing) progressDialog.dismiss()

                if (response.code() != 200) {
                    serverAlert(activity)
                    return
                }
                val responseData = response.body()!!.string()
                Log.i("TAG", "onResponse: $responseData")
                if (!TextUtils.isEmpty(responseData)) {
                    val jsonObject = JSONObject(responseData)
                    if (jsonObject.getBoolean("success")) {
                        val pojo = CommentPojo()
                        pojo.firstName = storeUserData.getString(Constants.USER_FIRST_NAME)
                        pojo.lastName = storeUserData.getString(Constants.USER_LAST_NAME)
                        pojo.countryCode = storeUserData.getString(Constants.USER_COUNTRY_CODE)
                        pojo.mobileNo = storeUserData.getString(Constants.USER_PHONE_NUMBER)
                        pojo.comment = ed_comment.text.toString()
                        pojo.id = jsonObject.getInt("commentId")
                        pojo.addDate = SimpleDateFormat("MMM dd''yy hh:mm a", Locale.ENGLISH).format(Date())
                        pojo.userId = storeUserData.getString(Constants.USER_ID).toInt()
                        adapter.addItem(pojo)
                        layoutManager.scrollToPosition(0)
                        ed_comment.setText("")
                    } else {
                        Snackbar.make(btn_send, jsonObject.getString("message"), Snackbar.LENGTH_LONG).show()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                if (progressDialog.isShowing) progressDialog.dismiss()
                t.printStackTrace()
            }
        })
    }

    fun commentStatus(status: Boolean) {
        progressDialog.show()
        val result = Utils.CallApi(activity).changeStatus(
                newsPojo.postId,
                if (status) 1 else 0
        )
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (progressDialog.isShowing) progressDialog.dismiss()
                if (response.code() != 200) {
                    serverAlert(activity)
                    return
                }
                val responseData = response.body()!!.string()
                Log.i("TAG", "onResponse: $responseData")
                if (responseData != null && responseData.isNotEmpty()) {
                    val jsonObject = JSONObject(responseData)
                    if (!jsonObject.getBoolean("success")) { //edComment.setText("");
                        newsPojo.notificationStatus = if (status) 1 else 0
                        Constants.lstNews[intent.getIntExtra("position", -1)] = newsPojo
                        Snackbar.make(btn_send, jsonObject.getString("message"), Snackbar.LENGTH_LONG).show()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                if (progressDialog.isShowing) progressDialog.dismiss()
                t.printStackTrace()
            }
        })
    }

    val comments: Unit
        get() {
            if (page == 1) {
                progressDialog.show()
            }
            val result = Utils.CallApi(activity).getComments(
                    newsPojo.postId, page
            )
            result.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    if (page == 1) {
                        if (progressDialog.isShowing) progressDialog.dismiss()
                    }

                    if (response.code() != 200) {
                        serverAlert(activity)
                        return
                    }
                    val responseData = response.body()!!.string()
                    Log.i("TAG", "onResponse: $responseData")
                    val reader: Reader = StringReader(responseData)
                    val gson = GsonBuilder()
                            .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                            .serializeNulls()
                            .create()
                    val comment = gson.fromJson(reader, Comments::class.java)
                    if (comment.success) {
                        totalPages = comment.totalPages
                        if (page == 1) {
                            lstComments.clear()
                        } else {
                            lstComments.removeAt(lstComments.size - 1)
                            adapter.notifyItemRemoved(lstComments.size)
                        }
                        lstComments.addAll(comment.data)
                        Log.i("TAG", "onResponse: " + "page = " + page + "-\n total - " + totalPages + "\n" + lstComments.size)
                        recyclerView.postDelayed({
                            adapter.notifyDataSetChanged()
                            adapter.setLoaded()
                        }, 100)
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    if (progressDialog.isShowing) progressDialog.dismiss()
                    t.printStackTrace()
                }
            })
        }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}