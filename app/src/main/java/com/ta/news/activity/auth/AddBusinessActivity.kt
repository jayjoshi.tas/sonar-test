package com.ta.news.activity.auth

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import com.ta.news.R
import com.ta.news.activity.BaseActivity
import com.ta.news.controls.CustomDialog
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.activity_add_business.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AddBusinessActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_business)
        activity = this
        storeUserData = StoreUserData(activity)

        getProfile()





        //edName.setKeyListener(DigitsKeyListener.getInstance("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "));

        btnAdd.setOnClickListener(View.OnClickListener {

            if (Utils.isEmpty(edName)) {
                showAlert(activity, getString(R.string.enter_name))
            } else if (Utils.isEmpty(edAddressfield)) {
                showAlert(activity, getString(R.string.enter_address))
            } else if (Utils.isEmpty(edMobilefield)) {
                showAlert(activity, getString(R.string.enter_mobile_number))
            } else if (Utils.isEmpty(edContactPerson)) {
                showAlert(activity, getString(R.string.pls_add_contact_person))
            } else {
                addUpdate()
            }
        })


    }



    fun addUpdate() {
        progressDialog.show()
        val result = Utils.CallApi(activity).addUpdatebusiness(
                storeUserData.getString(Constants.USER_ID), edName.text.toString().trim(), edAddressfield.text.toString().trim(), edMobilefield.text.toString().trim(), edContactPerson.text.toString().trim()
        )
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (progressDialog.isShowing) progressDialog.dismiss()
                if (response.code() != 200) {
                    serverAlert(activity)
                    return
                }
                val responseData = response.body()!!.string()
                Log.i("TAG", "onResponse: " + responseData)
                val Alertdialog = CustomDialog(activity)
                Alertdialog.show()
                Alertdialog.setCancelable(false)
                Alertdialog.setMessage("Business Added Successfully")
                Alertdialog.setPositiveButton(R.string.ok)
                Alertdialog.setPositiveButton(R.string.ok, View.OnClickListener {
                    getProfile()
                    // finish()
                    Alertdialog.dismiss()
                })

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                if (progressDialog.isShowing) progressDialog.dismiss()
                t.printStackTrace()
            }
        })
    }


    fun getProfile() {

        if (storeUserData.getString(Constants.USER_ID).isNotEmpty()) {
            progressDialog.show()
            val result = Utils.CallApi(activity).profileById(storeUserData.getString(Constants.USER_ID).toInt())
            result.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    if (progressDialog.isShowing) progressDialog.dismiss()

                    val responseData = response.body()!!.string()
                    Log.i("response", "onResponse: $responseData")
                    if (!TextUtils.isEmpty(responseData)) {
                        val jsonObject = JSONObject(responseData)
                        //{"success":true,"userId":"3","firstName":"arth","lastName":"","email":"tilva.arth@gmail.com","countryCode":"","mobileNo":"+919033975383","address":"gg","pincode":"360005"}
                        if (jsonObject.getBoolean("success")) {
                            var jsonarr = jsonObject.getJSONArray("businessInformation")
                            val jObject = jsonarr.getJSONObject(0)
                            edName.setText(jObject.getString("name").toString())
                            edAddressfield.setText(jObject.getString("address").toString())
                            edMobilefield.setText(jObject.getString("contactNo").toString())
                            edContactPerson.setText(jObject.getString("contactPerson").toString())

                        } else {
                            showAlert(activity, jsonObject.getString("message"))
                        }
                    }

                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    if (progressDialog.isShowing) progressDialog.dismiss()
                    t.printStackTrace()
                }
            })
        } else {
        }


    }


}