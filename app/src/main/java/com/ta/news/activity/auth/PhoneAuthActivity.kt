package com.ta.news.activity.auth

import android.content.Intent
import android.os.Bundle
import com.ta.news.R
import com.ta.news.activity.BaseActivity
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.activity_phone_auth.*

class PhoneAuthActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = this
        storeUserData = StoreUserData(activity)
        setContentView(R.layout.activity_phone_auth)
        if (storeUserData.getString(Constants.USER_COUNTRY_CODE).isNotEmpty()) {
            countryCode.setCountryForPhoneCode(storeUserData.getString(Constants.USER_COUNTRY_CODE).replace("+", "").toInt())
            fieldPhoneNumber.setText(storeUserData.getString(Constants.USER_PHONE_NUMBER))
        }
        buttonStartVerification.setOnClickListener {
            if (Utils.isEmpty(fieldPhoneNumber) || fieldPhoneNumber.text.toString().startsWith("+") || fieldPhoneNumber.text.toString().startsWith("0")) {
                showAlert(activity, getString(R.string.please_enter_phone_number))
            } else {
                startActivity(Intent(activity, VerificationActivity::class.java)
                        .putExtra("countryCode", "+" + countryCode.selectedCountryCode.toString())
                        .putExtra("phoneNumber", fieldPhoneNumber.text.toString())
                )
            }
        }
    }
}