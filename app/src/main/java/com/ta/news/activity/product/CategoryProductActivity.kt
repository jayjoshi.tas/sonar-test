package com.ta.news.activity.product

import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import com.ta.news.R
import com.ta.news.activity.BaseActivity
import com.ta.news.adapter.CategoryProductAdapter
import com.ta.news.pojo.ProductCategory
import com.ta.news.pojo.ProductPojo
import com.ta.news.utils.OnLoadMoreListener
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.activity_category_product.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.io.Reader
import java.io.StringReader
import java.lang.reflect.Modifier

class CategoryProductActivity : BaseActivity() {
    var page = 1
    lateinit var layoutManager: LinearLayoutManager
    var totalPages = 1
    lateinit var adapter: CategoryProductAdapter
    lateinit var category: ProductCategory
    var list = ArrayList<ProductPojo>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category_product)
        activity = this
        storeUserData = StoreUserData(activity)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val upArrow = ContextCompat.getDrawable(activity, R.drawable.left_arrow)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            upArrow?.colorFilter = BlendModeColorFilter(ContextCompat.getColor(activity, R.color.white), BlendMode.SRC_ATOP)
        } else {
            upArrow?.setColorFilter(ContextCompat.getColor(activity, R.color.white), PorterDuff.Mode.SRC_ATOP)
        }
        supportActionBar?.setHomeAsUpIndicator(upArrow)
        category = intent.getSerializableExtra("category") as ProductCategory
        tvTitle.text = category.name
        adapter = CategoryProductAdapter(activity, rvProductList, list)
        adapter.setOnLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                if (page < totalPages) {
                    page += 1
                    getProducts()
                    Log.i("TAG", "onCreateView: 3")
                }
            }
        })
        rvProductList.adapter = adapter
        data
    }


    val data: Unit
        get() {
            if (Utils.isOnline(activity)) {
                getProducts()
            } else {
                rvProductList.visibility = View.GONE
                ivNoInternet.visibility = View.VISIBLE
            }
        }

    //adapter.notifyItemRangeChanged(list.size(), list.size() - news.lstBooks.size());
    fun getProducts() {
        Log.i("TAG", "getNewsFromServer: called")
        if (page == 1) progressDialog.show()
        val result = Utils.CallApi(activity).getCategoryWiseProduct(category.id, page)
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressDialog.dismiss()
                try {
                    Log.i("TAG", "onResponse: " + response.code())
                    if (response.code() != 200) {
                        serverAlert(activity)
                        return
                    }
                    val res = response.body()!!.string()
                    Log.i("response", "onResponse: $res")
                    val reader: Reader = StringReader(res)
                    val gson = GsonBuilder()
                            .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                            .create()
                    val products = gson.fromJson(reader, ProductCategory::class.java)
                    //Constants.BOOKS_BASE_URL = books.base_url
                    totalPages = products.totalPages
                    if (page == 1) {
                        list.clear()
                    }
                    list.addAll(products.data)
                    Log.i("TAG", "onResponse: " + "page = " + page + "-\n total - " + totalPages + "\n" + list.size)
                    adapter.notifyDataSetChanged()
                    adapter.setLoaded()
                } catch (e: IOException) {
                    e.printStackTrace()
                    serverAlert(activity)
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                    serverAlert(activity)
                } catch (e: JsonSyntaxException) {
                    e.printStackTrace()
                    serverAlert(activity)
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }
        })
    }
}