package com.ta.news.activity

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.core.content.ContextCompat
import com.activeandroid.query.Select
import com.ta.news.R
import com.ta.news.controls.CustomDialog
import com.ta.news.pojo.Notification
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.activity_notification_detail.*

class NotificationDetailActivity : BaseActivity() {

    lateinit var pojo: Notification
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification_detail)
        activity = this
        storeUserData = StoreUserData(activity)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val upArrow = ContextCompat.getDrawable(activity, R.drawable.left_arrow)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            upArrow?.colorFilter = BlendModeColorFilter(ContextCompat.getColor(activity, R.color.white), BlendMode.SRC_ATOP)
        } else {
            upArrow?.setColorFilter(ContextCompat.getColor(activity, R.color.white), PorterDuff.Mode.SRC_ATOP)
        }
        supportActionBar?.setHomeAsUpIndicator(upArrow)

        pojo = Select().from(Notification::class.java).where("id=?", intent.getLongExtra("id", -1)).executeSingle()
        tvDescription.text = pojo.message
        tvDescription.setOnLongClickListener {
            var share = ""
            share += pojo.message + "\n\n"
            share += ("વધુ માહિતી માટે અહીં થી એપ્લિકેશન ડાઉનલોડ કરો અને મફત સમાચાર મેળવો...\n"
                    + "Android: http://bit.ly/2GC6LH1\niOS: https://apple.co/2Lv3hVm")
            val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText(getString(R.string.app_name), share)
            clipboard.setPrimaryClip(clip)
            Utils.showToast(activity, R.string.text_copied)
            true
        }
        btnDelete.setOnClickListener {
            val dialog = CustomDialog(activity)
            dialog.show()
            dialog.setMessage(R.string.delete_confirmation)
            dialog.setPositiveButton(R.string.yes, View.OnClickListener {
                dialog.dismiss()
                pojo.delete()
                finish()
            })
            dialog.setNegativeButton(R.string.no, View.OnClickListener { dialog.dismiss() })
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean { // handle arrow click here
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}