package com.ta.news.activity.post

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.View.OnLongClickListener
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.GridLayoutManager
import com.activeandroid.query.Select
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import com.ta.news.R
import com.ta.news.activity.BaseActivity
import com.ta.news.activity.MainActivity
import com.ta.news.activity.MainApplication
import com.ta.news.activity.SplashActivity
import com.ta.news.adapter.MediaAdapter
import com.ta.news.controls.CustomDialog
import com.ta.news.fragment.CallAdvOptionsFragment
import com.ta.news.pojo.*
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.activity_news_detail.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.io.Reader
import java.io.StringReader
import java.lang.reflect.Modifier

class NewsDetailActivity : BaseActivity() {

    var pojo: NewsPojo? = null
    lateinit var layoutManager: GridLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_detail)
        activity = this
        storeUserData = StoreUserData(activity)
        if (intent.getIntExtra("openCmd", -1) == 6) {
            getNewsFromServer()
            ivSave.visibility = View.VISIBLE
            ivDelete.visibility = View.GONE
        } else if (intent.getBooleanExtra("isOffline", false)) {
            getNewsFromServer()
            val news = Select().from(OfflineNews::class.java).where("postId = ?", intent.getIntExtra("postId", -1)).executeSingle<OfflineNews>()
            pojo = NewsPojo()
            if (news != null) {
                pojo!!.newsTitle = news.newsTitle
                pojo!!.newsDetails = news.newsDetails
                pojo!!.postId = news.postId
                pojo!!.firstName = news.firstName
                pojo!!.lastName = news.lastName
                pojo!!.mobileNo = news.mobileNo
                pojo!!.addDate = news.addDate
                pojo!!.address = news.address
                pojo!!.approxPrice = news.approxPrice
                pojo!!.profileImage = news.profileImage
                pojo!!.isVerified = news.isVerified
                pojo!!.userId = news.userId
                pojo!!.mediaArray = ArrayList()
                var offList = Select().from(OfflineMedia::class.java).where("postId = ?", pojo!!.postId).execute<OfflineMedia>()
                if (offList != null) {
                    for (media in offList) {
                        var live = MediaPOJO()
                        live.folderPath = media.folderPath
                        live.mediaName = media.mediaName
                        live.thumbFolderPath = media.thumbFolderPath
                        live.thumbImage = media.thumbImage
                        live.type = media.type
                        pojo!!.mediaArray.add(live)
                    }
                }
                ivSave.visibility = View.GONE
                ivDelete.visibility = View.VISIBLE
                setDetails()
            }
        } else {
            pojo = intent.getSerializableExtra("pojo") as NewsPojo
            setDetails()
            ivSave.visibility = View.VISIBLE
            ivDelete.visibility = View.GONE
        }

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val upArrow = ContextCompat.getDrawable(activity, R.drawable.left_arrow)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            upArrow?.colorFilter = BlendModeColorFilter(ContextCompat.getColor(activity, R.color.white), BlendMode.SRC_ATOP)
        } else {
            upArrow?.setColorFilter(ContextCompat.getColor(activity, R.color.white), PorterDuff.Mode.SRC_ATOP)
        }
        supportActionBar?.setHomeAsUpIndicator(upArrow)

        loadBannerAds(banner_container, googleAdView, R.string.fb_news_detail_banner)
    }

    private fun setDetails() {
        if (StoreUserData(this).getString(Constants.USER_ID).isEmpty()) {
            startActivity(Intent(activity, SplashActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
            return
        }
        MainApplication.instance?.loadFullScreenAds(false)
        if (pojo != null) {
            tvTitle.text = pojo!!.newsTitle
            tvAddDate.text = pojo!!.addDate
            tvDescription.text = pojo!!.newsDetails
            if (TextUtils.isEmpty(pojo!!.approxPrice)) {
                tvApproxPrice.visibility = View.GONE
            } else {
                tvApproxPrice.visibility = View.VISIBLE
                tvApproxPrice.text = pojo!!.approxPrice
            }
            if (pojo!!.address.isNullOrEmpty()) {
                tvAddress.visibility = View.GONE
            } else {
                tvAddress.visibility = View.VISIBLE
            }
            tvAddress.text = if (pojo!!.address.isNullOrEmpty()) "" else "સ્થળ: " + pojo!!.address
            if (intent.getBooleanExtra("uploaded", false)) {
                spam.isVisible = false
                tvAuthor.text = String.format("%s %s", storeUserData.getString(Constants.USER_FIRST_NAME), storeUserData.getString(Constants.USER_LAST_NAME))
            } else {
                spam.isVisible = true
                tvAuthor.text = String.format("%s %s", pojo!!.firstName, pojo!!.lastName)
            }
            if (TextUtils.isEmpty(pojo!!.mobileNo)) {
                tvContactDetail.visibility = View.GONE
                tvWhatsapp.visibility = View.GONE
            } else {
                tvContactDetail.visibility = View.VISIBLE
                tvWhatsapp.visibility = View.VISIBLE
                tvContactDetail.text = (if (pojo!!.countryCode == null) "" else pojo!!.countryCode) + pojo!!.mobileNo
                tvWhatsapp.text = (if (pojo!!.countryCode == null) "" else pojo!!.countryCode) + pojo!!.mobileNo
            }

            if (pojo!!.isVerified) {
                tvAuthor.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(activity, R.drawable.users), null, ContextCompat.getDrawable(activity, R.drawable.verified), null)
            } else {
                tvAuthor.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(activity, R.drawable.users), null, null, null)
            }

            if (pojo!!.mediaArray.isNullOrEmpty()) {
                media.visibility = View.GONE
            } else {
                layoutManager = GridLayoutManager(activity, 3)
                media.layoutManager = layoutManager
                media.setHasFixedSize(true)
                media.isNestedScrollingEnabled = false
                media.visibility = View.VISIBLE
                val adapter = MediaAdapter(activity, pojo!!.mediaArray)
                media.adapter = adapter
            }
            if (Constants.lstAdvs.isEmpty()) {
                llAds.visibility = View.GONE
            } else {
                var pos = storeUserData.getInt("adv_cat_" + intent.getIntExtra("categoryId", 0))
                pos++
                if (pos >= Constants.lstAdvs.size) {
                    pos = 0
                }
                llAds.visibility = View.VISIBLE
                var ad = Constants.lstAdvs[pos]
                Glide.with(activity).load(storeUserData.getString(Constants.URL) + ad.folderPath + ad.adImage.replace(" ".toRegex(), "%20")).into(advImage)
                advImage.setOnClickListener {
                    /*val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(Constants.lstAdvs[pos].androidUrl))
                    startActivity(browserIntent)*/

                    val callAdvOptionsFragment = CallAdvOptionsFragment(ad)
                    var bundle = Bundle()
                    callAdvOptionsFragment.arguments = bundle
                    callAdvOptionsFragment.show(
                            supportFragmentManager,
                            CallAdvOptionsFragment::class.java.simpleName
                    )
                }

                storeUserData.setInt("adv_cat_" + intent.getIntExtra("categoryId", 0), pos)
            }

            if (intent.getBooleanExtra("uploaded", false) && pojo?.statusCode != 0) {
                btnDeleteFromServer.visibility = View.VISIBLE
            }

            tvDescription.setOnClickListener {
                llNews.performClick()
            }
            tvDescription.setOnLongClickListener(OnLongClickListener {
                if (pojo != null) {
                    var share: String = pojo!!.newsTitle + "\n\n"
                    var description = pojo!!.newsDetails
                    if (description.length > 60) {
                        description = description.substring(0, 55)
                        description += "..."
                    }
                    share += description + "\n\n"
                    share += "http://piplanapane.in/news/${pojo!!.postId}"
                    share += "\n\n"
                    share += ("વધુ માહિતી માટે અહીં થી એપ્લિકેશન ડાઉનલોડ કરો અને મફત સમાચાર મેળવો...\n"
                            + "Android: http://bit.ly/2GC6LH1\niOS: https://apple.co/2Lv3hVm")
                    val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                    val clip = ClipData.newPlainText(getString(R.string.app_name), share)
                    clipboard.setPrimaryClip(clip)
                    Utils.showToast(activity, R.string.text_copied)
                }
                true
            })
            btnShare.setOnClickListener {
                if (pojo != null) {
                    Utils.openShare(activity, pojo!!)
                }
            }
            ivSave.setOnClickListener {
                if (pojo != null) {
                    val news = Select().from(OfflineNews::class.java).where("postId = ?", pojo!!.postId).executeSingle<OfflineNews>()
                    if (news != null) {
                        showAlert(activity, getString(R.string.news_already_saved))
                    } else {
                        val offlineNews = OfflineNews()
                        offlineNews.newsDetails = pojo!!.newsDetails
                        offlineNews.newsTitle = pojo!!.newsTitle
                        offlineNews.postId = pojo!!.postId
                        offlineNews.addDate = pojo!!.addDate
                        offlineNews.firstName = pojo!!.firstName
                        offlineNews.lastName = pojo!!.lastName
                        offlineNews.mobileNo = pojo!!.mobileNo
                        offlineNews.address = pojo!!.address
                        offlineNews.approxPrice = pojo!!.approxPrice
                        offlineNews.profileImage = pojo!!.profileImage
                        offlineNews.isVerified = pojo!!.isVerified
                        offlineNews.userId = pojo!!.userId
                        Log.i("TAG", "onClick: " + pojo!!.firstName + "\n" + pojo!!.lastName + "\n" + pojo!!.mobileNo)
                        offlineNews.save()
                        for (media in pojo!!.mediaArray) {
                            var offlineMedia = OfflineMedia()
                            offlineMedia.postId = pojo!!.postId
                            offlineMedia.folderPath = media.folderPath
                            offlineMedia.mediaName = media.mediaName
                            offlineMedia.thumbFolderPath = media.thumbFolderPath
                            offlineMedia.thumbImage = media.thumbImage
                            offlineMedia.type = media.type
                            offlineMedia.save()
                        }
                        showAlert(activity, getString(R.string.news_saved_successfully))
                    }
                }
            }
            ivComment.setOnClickListener {
                if (pojo != null) {
                    startActivity(Intent(activity, CommentActivity::class.java)
                            .putExtra("position", intent.getIntExtra("position", -1))
                            .putExtra("pojo", pojo))
                }
            }
            ivDelete.setOnClickListener {
                if (pojo != null) {
                    val dialog = CustomDialog(activity)
                    dialog.show()
                    dialog.setMessage(R.string.delete_confirmation)
                    dialog.setPositiveButton(R.string.yes, View.OnClickListener {
                        dialog.dismiss()
                        val news = Select().from(OfflineNews::class.java).where("postId = ?", pojo!!.postId).executeSingle<OfflineNews>()
                        news?.delete()!!
                        val media = Select().from(OfflineMedia::class.java).where("postId = ?", pojo!!.postId).execute<OfflineMedia>()
                        if (media != null) {
                            for (m in media) {
                                m.delete()
                            }
                        }
                        finish()
                    })
                    dialog.setNegativeButton(R.string.no, View.OnClickListener { dialog.dismiss() })
                }
            }

            btnDeleteFromServer.setOnClickListener(View.OnClickListener {
                if (pojo != null) {
                    if (pojo!!.statusCode == 1 || pojo!!.statusCode == 3) {
                        val dialog = CustomDialog(activity)
                        dialog.show()
                        dialog.setMessage(R.string.delete_news_confirmation)
                        dialog.setPositiveButton(R.string.yes, View.OnClickListener {
                            dialog.dismiss()
                            deleteNews()
                        })
                        dialog.setNegativeButton(R.string.no, View.OnClickListener { dialog.dismiss() })
                    } else if (pojo!!.statusCode == 2) {
                        showAlert(activity, getString(R.string.post_already_declined))
                    }
                }
            })

            tvWhatsapp.setOnClickListener {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://api.whatsapp.com/send?phone=" + tvWhatsapp.text.toString().replace("+", ""))))
            }
            llNews.setOnClickListener {
                if (!pojo!!.mobileNo.isNullOrEmpty()) {
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + pojo!!.mobileNo)))
                }
            }

            spam.setOnClickListener {
                startActivity(Intent(activity, SpamActivity::class.java).putExtra("pojo", pojo))
            }
        }
    }

    fun deleteNews() {
        progressDialog.show()
        val result = Utils.CallApi(activity).deleteNews(
                storeUserData.getString(Constants.USER_ID),
                pojo!!.postId
        )
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (progressDialog.isShowing) progressDialog.dismiss()

                if (response.code() != 200) {
                    serverAlert(activity)
                    return
                }
                val responseData = response.body()!!.string()
                Log.i("TAG", "onResponse: $responseData")
                if (!TextUtils.isEmpty(responseData)) {
                    val jsonObject = JSONObject(responseData)
                    if (jsonObject.getBoolean("success")) {
                        finish()
                    } else {
                        Snackbar.make(btnDeleteFromServer, jsonObject.getString("message"), Snackbar.LENGTH_LONG).show()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                if (progressDialog.isShowing) progressDialog.dismiss()
                t.printStackTrace()
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (intent.getIntExtra("openCmd", -1) == 6) {
            startActivity(Intent(activity, MainActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
        } else {
            finish()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean { // handle arrow click here
        if (item.itemId == android.R.id.home) {
            if (intent.getIntExtra("openCmd", -1) == 6) {
                startActivity(Intent(activity, MainActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
            } else {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }


    fun getNewsFromServer() {
        progressDialog.show()
        val result = Utils.CallApi(activity).news(intent.getIntExtra("postId", -1), "android")
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (progressDialog.isShowing) progressDialog.dismiss()
                try {
                    if (response.code() != 200) {
                        serverAlert(activity)
                        return
                    }
                    val res = response.body()!!.string()
                    Log.i("response", "onResponse: $res")
                    val reader: Reader = StringReader(res)
                    val gson = GsonBuilder()
                            .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                            .serializeNulls()
                            .create()
                    val news = gson.fromJson(reader, News::class.java)
                    if (news.success) {
                        if (news.advertisement != null && news.advertisement!!.size > 0) {
                            Constants.lstAdvs.clear()
                            Constants.lstAdvs.addAll(news.advertisement!!)
                        }
                        if (news.data != null && news.data!!.size > 0) {
                            pojo = news.data!![0]
                        }
                        setDetails()
                    } else if (intent.getBooleanExtra("isOffline", false)) {
                        showAlert(activity, getString(R.string.post_removed_by_author), true)
                    } else {
                        startActivity(Intent(activity, MainActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP))
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                    serverAlert(activity)
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                    serverAlert(activity)
                } catch (e: JsonSyntaxException) {
                    e.printStackTrace()
                    serverAlert(activity)
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                if (progressDialog.isShowing) progressDialog.dismiss()
                t.printStackTrace()
            }
        })
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            99 -> {
                var required = false
                var i = 0
                while (i < permissions.size) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Permissions", "Permission Granted: " + permissions[i])
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i])
                        required = true
                    }
                    i++
                }
                if (required) {
                    showCustomDialog()
                }
            }
            else -> {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            }
        }
    }

    private fun showCustomDialog() {
        val dialog = CustomDialog(activity)
        dialog.show()
        dialog.setMessage(R.string.permission_message)
        dialog.setPositiveButton(R.string.ok, View.OnClickListener {
            dialog.dismiss()
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.fromParts("package", packageName, null))
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        })
    }
}