package com.ta.news.activity.content

import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.MenuItem
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.ta.news.R
import com.ta.news.activity.BaseActivity
import com.ta.news.activity.MainApplication
import com.ta.news.adapter.ContentSubCategoryMediaImageAdapter
import com.ta.news.pojo.ContentPost
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.activity_content_detail.*


class ContentDetailActivity : BaseActivity() {
    lateinit var content: ContentPost
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = this
        storeUserData = StoreUserData(activity)
        setContentView(R.layout.activity_content_detail)
        content = intent.getSerializableExtra("content") as ContentPost
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val upArrow = ContextCompat.getDrawable(activity, R.drawable.left_arrow)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            upArrow?.colorFilter = BlendModeColorFilter(ContextCompat.getColor(activity, R.color.white), BlendMode.SRC_ATOP)
        } else {
            upArrow?.setColorFilter(ContextCompat.getColor(activity, R.color.white), PorterDuff.Mode.SRC_ATOP)
        }
        supportActionBar?.setHomeAsUpIndicator(upArrow)
        tv_title.text = content.title
        tv_description.text = content.details
        Glide.with(activity).load(StoreUserData(activity).getString(Constants.URL) + "" + content.thumbImage).into(titleImage)
        recyclerView.adapter = ContentSubCategoryMediaImageAdapter(activity, content.mediaArray)
        MainApplication.instance?.loadFullScreenAds(false)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}