package com.ta.news.activity

//import com.ta.news.database.AppDatabase

import android.os.Build
import android.os.StrictMode
import android.os.StrictMode.VmPolicy
import android.util.Log
import androidx.multidex.MultiDex
import com.activeandroid.ActiveAndroid
import com.activeandroid.Configuration
import com.activeandroid.app.Application
import com.facebook.ads.Ad
import com.facebook.ads.AdError
import com.facebook.ads.InterstitialAd
import com.facebook.ads.InterstitialAdListener
import com.google.android.gms.ads.AdRequest
import com.google.firebase.iid.FirebaseInstanceId
import com.onesignal.OneSignal
import com.ta.news.BuildConfig
import com.ta.news.R
import com.ta.news.fcm.OneSignalNotificationHandler
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import net.gotev.uploadservice.UploadService


/**
 * Created by arthtilva on 25-Feb-17.
 */
class MainApplication : Application() {
    companion object {
        @get:Synchronized
        var instance: MainApplication? = null
    }

    lateinit var fbInterstitial: InterstitialAd
    lateinit var googleInterstitial: com.google.android.gms.ads.InterstitialAd
    lateinit var fbInterstitialAdListener: InterstitialAdListener
    var adsCounter = 0
    lateinit var storeUserData: StoreUserData
    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)
        instance = this
        storeUserData = StoreUserData(this)
        val builder = VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            builder.detectFileUriExposure()
        }
        /**
         * Upload Service
         */
        UploadService.NAMESPACE = BuildConfig.APPLICATION_ID
        /**
         * Active Android
         */
        val dbConfiguration = Configuration.Builder(this).setDatabaseName("tasNews.db").create()
        ActiveAndroid.initialize(dbConfiguration)
        /**
         * OneSignal
         */
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.None)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .setNotificationOpenedHandler(OneSignalNotificationHandler())
                .init()

        // noteDatabase = Room.databaseBuilder(this, AppDatabase::class.java, DB_NAME).build()

        OneSignal.idsAvailable { userId: String, registrationId: String? ->
            Log.d("debug", "User:$userId")
            if (registrationId != null) Log.d("debug", "registrationId:$registrationId")
        }

        if (StoreUserData(this).getString(Constants.USER_FCM).isEmpty()) {
            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceIdResult ->
                val deviceToken = instanceIdResult.token
                Log.i("token", deviceToken)
                StoreUserData(this).setString(Constants.USER_FCM, deviceToken)
            }
        }

        fbInterstitial = InterstitialAd(this, getString(R.string.fb_interstitial))
        fbInterstitialAdListener = object : InterstitialAdListener {
            override fun onInterstitialDisplayed(ad: Ad) {
                // Interstitial ad displayed callback
                Log.e("TAG", "Interstitial ad displayed.")
            }

            override fun onInterstitialDismissed(ad: Ad) {
                // Interstitial dismissed callback
                Log.e("TAG", "Interstitial ad dismissed.")
            }

            override fun onError(ad: Ad?, adError: AdError) {
                // Ad error callback
                Log.e("TAG", "Interstitial ad failed to load: " + adError.getErrorMessage())
            }

            override fun onAdLoaded(ad: Ad) {
                // Interstitial ad is loaded and ready to be displayed
                Log.d("TAG", "Interstitial ad is loaded and ready to be displayed!")
                // Show the ad
            }

            override fun onAdClicked(ad: Ad) {
                // Ad clicked callback
                Log.d("TAG", "Interstitial ad clicked!")
            }

            override fun onLoggingImpression(ad: Ad) {
                // Ad impression logged callback
                Log.d("TAG", "Interstitial ad impression logged!")
            }
        }

        fbInterstitial.loadAd(
                fbInterstitial.buildLoadAdConfig()
                        .withAdListener(fbInterstitialAdListener)
                        .build())
        googleInterstitial = com.google.android.gms.ads.InterstitialAd(this)
        googleInterstitial.adUnitId = getString(R.string.google_interstitial)
        googleInterstitial.loadAd(AdRequest.Builder().build())

    }

    fun loadFullScreenAds(isForce: Boolean) {
        if (storeUserData.getBoolean(Constants.IS_PREMIUM)) {
            return
        }
        adsCounter++
        if (isForce) {
            if (fbInterstitial.isAdLoaded && !fbInterstitial.isAdInvalidated) {
                fbInterstitial.show()
            } else if (googleInterstitial.isLoaded) {
                googleInterstitial.show()
            }
        } else if (adsCounter % Constants.interstitialCount == 0) {
            if (fbInterstitial.isAdLoaded && !fbInterstitial.isAdInvalidated) {
                fbInterstitial.show()
            } else if (googleInterstitial.isLoaded) {
                googleInterstitial.show()
            }
            adsCounter = 0
        }

        if (!fbInterstitial.isAdLoaded || fbInterstitial.isAdInvalidated)
            fbInterstitial.loadAd(fbInterstitial.buildLoadAdConfig()
                    .withAdListener(fbInterstitialAdListener)
                    .build())

        if (!googleInterstitial.isLoaded) {
            googleInterstitial.loadAd(AdRequest.Builder().build())
        }
    }
}