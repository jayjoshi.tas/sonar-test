package com.ta.news.activity
import android.os.Bundle
import com.ta.news.R
import com.ta.news.adapter.LangAdapter
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.activity_language.*


class LanguageActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = this
        storeUserData = StoreUserData(activity)
        setContentView(R.layout.activity_language)

        val langNameList = resources.getStringArray(R.array.lang_name)
        val codeList = resources.getStringArray(R.array.lang_code)
        rvLangList.adapter = LangAdapter(activity, langNameList,codeList)
    }
}