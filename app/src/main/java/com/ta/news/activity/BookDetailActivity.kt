package com.ta.news.activity

import android.content.Intent
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.MenuItem
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.ta.news.R
import com.ta.news.adapter.BookImagesAdapter
import com.ta.news.pojo.BooksPojo
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.activity_book_detail.*

class BookDetailActivity : BaseActivity() {

    lateinit var book: BooksPojo
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_detail)
        activity = this
        storeUserData = StoreUserData(activity)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val upArrow = ContextCompat.getDrawable(activity, R.drawable.left_arrow)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            upArrow?.colorFilter = BlendModeColorFilter(ContextCompat.getColor(activity, R.color.white), BlendMode.SRC_ATOP)
        } else {
            upArrow?.setColorFilter(ContextCompat.getColor(activity, R.color.white), PorterDuff.Mode.SRC_ATOP)
        }
        supportActionBar?.setHomeAsUpIndicator(upArrow)
        book = intent.getSerializableExtra("pojo") as BooksPojo

        Glide.with(activity).load(StoreUserData(activity).getString(Constants.URL) + book.folderPath + book.titleImage.replace(" ".toRegex(), "%20")).into(titleImage)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = BookImagesAdapter(activity, book.bookImages)

        bookId.text = "#${book.id}"
        bookPrice.text = "₹ ${book.price}"
        tv_title.text = book.title
        tv_description.text = book.description
        titleImage.setOnClickListener {
            startActivity(Intent(activity, ImageActivity::class.java)
                    .putExtra("cmd", Constants.SINGLE_IMAGE)
                    .putExtra("url", StoreUserData(activity).getString(Constants.URL) + book.folderPath + book.titleImage.replace(" ".toRegex(), "%20")))
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean { // handle arrow click here
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}