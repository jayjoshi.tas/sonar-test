package com.ta.news.activity

import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import com.ta.news.R
import com.ta.news.adapter.CommodityPriceAdapter
import com.ta.news.pojo.CommodityPrice
import com.ta.news.pojo.CommodityPricePojo
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.activity_commodity_price.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.io.Reader
import java.io.StringReader
import java.lang.reflect.Modifier
import java.util.*

class CommodityPriceActivity : BaseActivity() {
    lateinit var commodityPrice: CommodityPrice
    var lstCommodity: ArrayList<CommodityPricePojo> = ArrayList()
    lateinit var adapter: CommodityPriceAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_commodity_price)
        activity = this
        storeUserData = StoreUserData(activity)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val upArrow = ContextCompat.getDrawable(activity, R.drawable.left_arrow)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            upArrow?.colorFilter = BlendModeColorFilter(ContextCompat.getColor(activity, R.color.white), BlendMode.SRC_ATOP)
        } else {
            upArrow?.setColorFilter(ContextCompat.getColor(activity, R.color.white), PorterDuff.Mode.SRC_ATOP)
        }
        supportActionBar?.setHomeAsUpIndicator(upArrow)
        tv_title.text = intent.getStringExtra("commodityName")
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        textView.isSelected = true
        textView.ellipsize = TextUtils.TruncateAt.MARQUEE
        adapter = CommodityPriceAdapter(activity, lstCommodity)
        getCommodityPrice()
        edSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                lstCommodity.clear()
                if (s.isNotEmpty()) {
                    for (c in commodityPrice.data) {
                        if (c.districtEn.toLowerCase().contains(s.toString().toLowerCase()) ||
                                c.districtGu.toLowerCase().contains(s.toString().toLowerCase()) ||
                                c.marketGuName.toLowerCase().contains(s.toString().toLowerCase()) ||
                                c.marketEnName.toLowerCase().contains(s.toString().toLowerCase())) {
                            lstCommodity.add(c)
                        }
                    }
                    if (s.toString().isNotEmpty()) {
                        btnCancel.visibility = View.VISIBLE
                    } else {
                        btnCancel.visibility = View.GONE
                    }
                } else {
                    lstCommodity.addAll(commodityPrice.data)
                }
                if (adapter != null) adapter.notifyDataSetChanged()
            }

            override fun afterTextChanged(s: Editable) {}
        })

        btnCancel.setOnClickListener {
            edSearch.setText("")
            Utils.hideKB(activity, edSearch)
        }
        loadBannerAds(banner_container, googleAdView, R.string.fb_whats_app_banner)
    }


    fun getCommodityPrice() {
        progressDialog.show()
        val result = Utils.CallApi(activity).commodityPrice(intent.getIntExtra("commodityId", -1))
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (progressDialog.isShowing) progressDialog.dismiss()
                try {
                    if (response.code() != 200) {
                        serverAlert(activity)
                        return
                    }
                    val res = response.body()!!.string()
                    Log.i("response", "onResponse: $res")
                    val reader: Reader = StringReader(res)
                    val gson = GsonBuilder()
                            .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                            .create()
                    commodityPrice = gson.fromJson(reader, CommodityPrice::class.java)
                    priceNotice.text = commodityPrice.priceNotice

                    lstCommodity.clear()
                    lstCommodity.addAll(commodityPrice.data)
                    adapter = CommodityPriceAdapter(activity, lstCommodity)
                    recyclerView.adapter = adapter
                } catch (e: IOException) {
                    e.printStackTrace()
                    serverAlert(activity)
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                    serverAlert(activity)
                } catch (e: JsonSyntaxException) {
                    e.printStackTrace()
                    serverAlert(activity)
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                if (progressDialog.isShowing) progressDialog.dismiss()
                t.printStackTrace()
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean { // handle arrow click here
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}