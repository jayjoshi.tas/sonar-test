package com.ta.news.activity

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.ta.news.R
import com.ta.news.adapter.FullScreenImageAdapter
import com.ta.news.pojo.MediaPOJO
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.activity_image.*
import java.io.*

class ImageActivity : BaseActivity() {

    var bitmap: Bitmap? = null
    var hasStoragePermission = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)
        activity = this
        storeUserData = StoreUserData(activity)
        back.setOnClickListener { view: View? -> finish() }
        var list = ArrayList<MediaPOJO>()
        if (intent.getIntExtra("cmd", -1) == Constants.LOCAL_IMAGE) {
            image.visibility = View.VISIBLE
            viewPager.visibility = View.GONE
            Glide.with(activity).asBitmap().load(File(intent.getStringExtra("url"))).listener(object : RequestListener<Bitmap?> {
                override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Bitmap?>, isFirstResource: Boolean): Boolean {
                    return false
                }

                override fun onResourceReady(resource: Bitmap?, model: Any, target: Target<Bitmap?>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                    bitmap = resource!!
                    return false
                }
            }).into(image)
        } else if (intent.getIntExtra("cmd", -1) == Constants.SINGLE_IMAGE) {
            image.visibility = View.VISIBLE
            viewPager.visibility = View.GONE
            Glide.with(activity).asBitmap().load(intent.getStringExtra("url")?.replace(" ".toRegex(), "%20")).listener(object : RequestListener<Bitmap?> {
                override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Bitmap?>, isFirstResource: Boolean): Boolean {
                    return false
                }

                override fun onResourceReady(resource: Bitmap?, model: Any, target: Target<Bitmap?>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                    bitmap = resource!!
                    return false
                }
            }).into(image)
        } else {
            viewPager.visibility = View.VISIBLE
            image.visibility = View.GONE
            list.addAll((intent.getSerializableExtra("list") as? ArrayList<MediaPOJO>)!!)
            viewPager.adapter = FullScreenImageAdapter(activity, list, itemClickListener)
            if (intent.getIntExtra("position", -1) != -1) {
                viewPager.currentItem = intent.getIntExtra("position", -1)
            }
        }
        download.setOnClickListener { view: View? ->
            hasStoragePermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            val list = ArrayList<String>()
            if (hasStoragePermission != PackageManager.PERMISSION_GRANTED) {
                list.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
            if (list.size != 0) {
                ActivityCompat.requestPermissions(activity, list.toTypedArray(), 99)
            } else if (bitmap != null) {
                try {
                    val path = Environment.getExternalStorageDirectory().toString()
                    var fOut: OutputStream? = null
                    val file = File(path, System.currentTimeMillis().toString() + ".jpg") // the File to save , append increasing numeric counter to prevent files from getting overwritten.
                    fOut = FileOutputStream(file)
                    bitmap!!.compress(Bitmap.CompressFormat.JPEG, 85, fOut) // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
                    fOut.flush() // Not really required
                    fOut.close() // do not forget to close the stream
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                Toast.makeText(activity, getString(R.string.image_saved), Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(activity, getString(R.string.please_wait), Toast.LENGTH_SHORT).show()
            }
        }
    }

    interface ItemClickListener {
        fun onClick(view: Bitmap)
    }

    var itemClickListener: ItemClickListener = object : ItemClickListener {
        override fun onClick(view: Bitmap) {
            bitmap = view
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            99 -> {
                var required = false
                var i = 0
                while (i < permissions.size) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Permissions", "Permission Granted: " + permissions[i])
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "Permission Denied: " + permissions[i])
                        required = true
                    }
                    i++
                }
                if (!required) {
                    download.performClick()
                }
            }
            else -> {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            }
        }
    }
}