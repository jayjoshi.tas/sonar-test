package com.ta.news.activity.post

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.*
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.ParcelFileDescriptor
import android.speech.RecognizerIntent
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.webkit.MimeTypeMap
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.JsonSyntaxException
import com.ta.news.R
import com.ta.news.activity.BaseActivity
import com.ta.news.activity.MainActivity
import com.ta.news.activity.MainApplication
import com.ta.news.adapter.AddMediaAdapter
import com.ta.news.controls.CustomDialog
import com.ta.news.fragment.DistrictFragment
import com.ta.news.media.image.ImagePicker
import com.ta.news.pojo.CategoryPojo
import com.ta.news.pojo.DistrictPojo
import com.ta.news.utils.API
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.activity_new_post_add.*
import kotlinx.android.synthetic.main.dialog_confirm_upload.*
import net.gotev.uploadservice.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.text.SimpleDateFormat
import java.util.*

class NewPostAddActivity : BaseActivity() {

    var lstMedia: ArrayList<String> = ArrayList()

    //var lstMediaCompressed = ArrayList<String>()
    var lstMediaCompressedThumb = ArrayList<String>()
    lateinit var layoutManager: GridLayoutManager
    lateinit var adapter: AddMediaAdapter
    lateinit var confirmDialog: ConfirmDialog

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val mPaths: List<String> = data!!.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH)!!
            if (mPaths.size > Constants.MAX_MEDIA_UPLOAD) {
                showAlert(activity, getString(R.string.you_can) + " " + Constants.MAX_MEDIA_UPLOAD + " " + getString(R.string.upload_video_or_image))
                return
            }
            lstMedia.addAll(mPaths)
            adapter.notifyDataSetChanged()
        } else if (requestCode == 456 && resultCode == Activity.RESULT_OK) {
            val result: ArrayList<String> = data!!.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
            ed_title.append(" " + result[0])
        } else if (requestCode == 789 && resultCode == Activity.RESULT_OK) {
            val result: ArrayList<String> = data!!.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
            ed_description.append(" " + result[0])
        }

        /*if (requestCode == 159 && resultCode == Activity.RESULT_OK) {
            if (intent.hasExtra("paths")) {
                val pathsList = intent.extras!!.getStringArrayList("paths")
                lstMedia.addAll(pathsList!!)
            } else if (intent.hasExtra("path")) {
                val filePath = intent.extras!!.getString("path")!!
                lstMedia.add(filePath)
            }
            adapter.notifyDataSetChanged()
        }*/
    }

    var itemClickListener: ItemClickListener = object : ItemClickListener {
        override fun onClick(view: View?, position: Int) {
            if (position == 0) {
                if (lstMedia.size > Constants.MAX_MEDIA_UPLOAD) {
                    showAlert(activity, getString(R.string.you_can) + " " + Constants.MAX_MEDIA_UPLOAD + " " + getString(R.string.upload_video_or_image))
                    return
                }

                 /*val intent = Intent(activity, PickerActivity::class.java)
                 intent.putExtra("IMAGES_LIMIT", Constants.MAX_MEDIA_UPLOAD)
                 startActivityForResult(intent, 159)*/

                ImagePicker.Builder(activity)
                        .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                        .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.JPG)
                        .scale(800, 800)
                        .allowMultipleImages(false)
                        .enableDebuggingMode(false)
                        .build()


            } else {
                recyclerView.post {
                    if (position < lstMedia.size) {
                        lstMedia.removeAt(position)
                        adapter.notifyItemRemoved(position)
                        if (position <= 0) {
                            adapter.notifyDataSetChanged()
                        } else {
                            adapter.notifyItemRangeChanged(position - 1, lstMedia!!.size)
                        }
                    }
                }
            }
        }

        override fun onDistrictSelect(pojo: DistrictPojo) {
            ed_district.text = pojo.districtGu
            districtId = pojo.id
        }
    }
    var uploadIds = ArrayList<String>()
    var postId = ""
    var districtId = -1
    var successMessage = ""
    lateinit var category: CategoryPojo
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_post_add)
        activity = this
        storeUserData = StoreUserData(activity)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val upArrow = ContextCompat.getDrawable(activity, R.drawable.left_arrow)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            upArrow?.colorFilter = BlendModeColorFilter(ContextCompat.getColor(activity, R.color.white), BlendMode.SRC_ATOP)
        } else {
            upArrow?.setColorFilter(ContextCompat.getColor(activity, R.color.white), PorterDuff.Mode.SRC_ATOP)
        }
        supportActionBar?.setHomeAsUpIndicator(upArrow)
        category = intent.getSerializableExtra("category") as CategoryPojo
        if (category.hintTitle != null) {
            ed_title.hint = category.hintTitle
            ed_description.hint = category.hintDescription
        }
        tv_category.text = category.categoryName
        lstMedia.add("Add")
        adapter = AddMediaAdapter(activity, lstMedia, itemClickListener)
        layoutManager = GridLayoutManager(activity, 3)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
        recyclerView.isNestedScrollingEnabled = false
        btn_next.setOnClickListener { v: View? ->
            if (Utils.isEmpty(ed_title)) {
                showAlert(activity, getString(R.string.enter_title))
            } else if (Utils.isEmpty(ed_description)) {
                showAlert(activity, getString(R.string.enter_description))
            } else if (Utils.isEmpty(ed_location)) {
                showAlert(activity, getString(R.string.enter_location))
            } else if (Utils.isEmpty(ed_district)) {
                showAlert(activity, getString(R.string.enter_taluka))
            } else if (Utils.isEmpty(ed_approx_price)) {
                showAlert(activity, getString(R.string.enter_price))
            } else if (!Utils.isOnline(activity)) {
                internetAlert(activity)
            } else if (lstMedia != null && lstMedia.size - 1 > Constants.MAX_MEDIA_UPLOAD) {
                showAlert(activity, getString(R.string.you_can) + " " + Constants.MAX_MEDIA_UPLOAD + " " + getString(R.string.upload_video_or_image))
            } else {
                confirmDialog = ConfirmDialog(activity)
                confirmDialog.show()
            }
        }
        ed_title.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.toString().length > 30) {
                    showAlert(activity, getString(R.string.more_title))
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        ed_district.setOnClickListener {
            val districtFragment = DistrictFragment(itemClickListener)
            var bundle = Bundle()
            districtFragment.arguments = bundle
            districtFragment.show(
                    supportFragmentManager,
                    DistrictFragment::class.java.simpleName
            )
        }
        getDistrictList()

        recordTitle.setOnClickListener {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                checkPermission(456)
            } else {
                var speechRecognizerIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
                speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
                speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "gu")
                startActivityForResult(speechRecognizerIntent, 456)
            }
        }
        recordDescription.setOnClickListener {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                checkPermission(789)
            } else {
                var speechRecognizerIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
                speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
                speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "gu")
                startActivityForResult(speechRecognizerIntent, 789)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        var required = false
        var i = 0
        while (i < permissions.size) {
            if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                Log.d("Permissions", "Permission Granted: " + permissions[i])
            } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                Log.d("Permissions", "Permission Denied: " + permissions[i])
                required = true
            }
            i++
        }
        if (required) {
            /*showCustomDialog("You need to allow access to some permissions.",
                    object : OnClickListener() {
                        fun onClick(dialog: DialogInterface?, which: Int) {
                            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                    Uri.fromParts("package", packageName, null))
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(intent)
                            askOnceAgain = true
                        }
                    })*/
        } else {
            when (requestCode) {
                456 -> {
                    recordTitle.performClick()
                }
                789 -> {
                    recordDescription.performClick()
                }
                else -> {
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults)
                }
            }

        }

    }

    private fun checkPermission(code: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.RECORD_AUDIO), code)
        }
    }

    fun compressFromArray(position: Int) {

        val parcelFileDescriptor: ParcelFileDescriptor =
                contentResolver.openFileDescriptor(Uri.fromFile(File(lstMedia[position])), "r")!!
        val fileDescriptor: FileDescriptor = parcelFileDescriptor.fileDescriptor
        val image: Bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor)
        parcelFileDescriptor.close()
        //return image


        //val options = BitmapFactory.Options()
        //options.inPreferredConfig = Bitmap.Config.ARGB_8888
        //var bitmap = BitmapFactory.decodeFile(File(lstMedia[position]).absolutePath, options)
        val thumb: Bitmap
        thumb = scaleDown(image, 450f, false)
        var fOutThumb: OutputStream? = null
        var fileThumb: File? = null
        try {
            val dir = File(lstMedia[position]).parent
            fileThumb = File(dir + "/thumb_" + File(lstMedia[position]).name)
            fOutThumb = FileOutputStream(fileThumb)
            thumb.compress(Bitmap.CompressFormat.JPEG, 85, fOutThumb) // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
            try {
                fOutThumb.flush() // Not really required
                fOutThumb.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            lstMediaCompressedThumb.add(fileThumb.absolutePath)

            if (position == lstMedia.size - 1) {
                for (k in lstMedia.indices) { //for (String file : lstMediaCompressed) {
                    try {
                        val uploadNotificationConfig = UploadNotificationConfig()
                        uploadNotificationConfig.setClearOnActionForAllStatuses(true)
                        uploadNotificationConfig.setTitleForAllStatuses(getString(R.string.app_name))
                        val uploadId = MultipartUploadRequest(activity, storeUserData.getString(Constants.URL) + "${API.version}/news/addNewsMedia")
                                .addFileToUpload(lstMedia[k], "file")
                                .addFileToUpload(lstMediaCompressedThumb[k], "thumb")
                                .addParameter("type", if (getMimeType(lstMedia[k])!!.startsWith("image")) "1" else "3")
                                .addParameter("postId", postId)
                                .setDelegate(object : UploadStatusDelegate {
                                    override fun onProgress(context: Context, uploadInfo: UploadInfo) {}
                                    override fun onError(context: Context, uploadInfo: UploadInfo, serverResponse: ServerResponse?, exception: Exception) {
                                        uploadIds.remove(uploadInfo.uploadId)
                                        if (uploadIds.isEmpty()) {
                                            dismissProgress()
                                            if (
                                                    !activity.isDestroyed || !activity.isFinishing) {
                                                val dialog = CustomDialog(activity)
                                                dialog.show()
                                                dialog.setMessage(successMessage)
                                                dialog.setCancelable(false)
                                                dialog.setPositiveButton(R.string.ok, View.OnClickListener {
                                                    dialog.dismiss()
                                                    startActivity(Intent(activity, MainActivity::class.java)
                                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP))
                                                })
                                            }
                                        }
                                    }

                                    override fun onCompleted(context: Context, uploadInfo: UploadInfo, serverResponse: ServerResponse?) {
                                        uploadIds.remove(uploadInfo.uploadId)
                                        if (uploadIds.isEmpty()) {
                                            dismissProgress()
                                            val dialog = CustomDialog(activity)
                                            dialog.show()
                                            dialog.setMessage(successMessage)
                                            dialog.setCancelable(false)
                                            dialog.setPositiveButton(R.string.ok, View.OnClickListener {
                                                dialog.dismiss()
                                                startActivity(Intent(activity, MainActivity::class.java)
                                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP))
                                            })
                                        }
                                    }

                                    override fun onCancelled(context: Context, uploadInfo: UploadInfo) {

                                    }
                                })
                                .startUpload()
                        uploadIds.add(uploadId)
                    } catch (exc: Exception) {
                        Log.e("AndroidUploadService", exc.message, exc)
                    }
                }
            } else {
                val mPosition = position + 1
                compressFromArray(mPosition)
            }
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
    }

    /*@SuppressLint("CheckResult")
    fun compressFromArray(position: Int) {
        Compressor(this)
                .setQuality(75)
                .compressToFileAsFlowable(File(lstMedia[position]))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ mFile: File ->
                    val options = BitmapFactory.Options()
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888
                    var bitmap = BitmapFactory.decodeFile(mFile.absolutePath, options)
                    val thumb: Bitmap
                    bitmap = if (category.id == 5) { //pashu palan mate vadhare
                        scaleDown(bitmap, 1200f, false)
                    } else {
                        scaleDown(bitmap, 900f, false)
                    }
                    thumb = scaleDown(bitmap, 350f, false)
                    var fOut: OutputStream? = null
                    var fOutThumb: OutputStream? = null
                    var fileThumb: File? = null
                    try {
                        fOut = FileOutputStream(mFile)
                        val dir = mFile.parent
                        fileThumb = File(dir + "/thumb_" + mFile.name)
                        fOut = FileOutputStream(mFile)
                        fOutThumb = FileOutputStream(fileThumb)
                    } catch (e: FileNotFoundException) {
                        e.printStackTrace()
                    }
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut) // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
                    thumb.compress(Bitmap.CompressFormat.JPEG, 85, fOutThumb) // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
                    try {
                        fOut!!.flush() // Not really required
                        fOut.close()
                        fOutThumb!!.flush() // Not really required
                        fOutThumb.close()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                    lstMediaCompressedThumb.add(fileThumb!!.absolutePath)
                    Log.i("TAG", "onPostExecute: " + position + ": " + mFile.absolutePath)
                    lstMediaCompressed.add(position, mFile.absolutePath)
                    if (position == lstMedia!!.size - 1) {
                        for (k in lstMediaCompressed.indices) { //for (String file : lstMediaCompressed) {
                            try {
                                val uploadNotificationConfig = UploadNotificationConfig()
                                uploadNotificationConfig.setClearOnActionForAllStatuses(true)
                                uploadNotificationConfig.setTitleForAllStatuses(getString(R.string.app_name))
                                val uploadId = MultipartUploadRequest(activity, storeUserData.getString(Constants.URL) + "${API.version}/news/addNewsMedia")
                                        .addFileToUpload(lstMediaCompressed[k], "file")
                                        .addFileToUpload(lstMediaCompressedThumb[k], "thumb")
                                        .addParameter("type", if (getMimeType(lstMediaCompressed[k])!!.startsWith("image")) "1" else "3")
                                        .addParameter("postId", postId)
                                        .setDelegate(object : UploadStatusDelegate {
                                            override fun onProgress(context: Context, uploadInfo: UploadInfo) {}
                                            override fun onError(context: Context, uploadInfo: UploadInfo, serverResponse: ServerResponse?, exception: Exception) {
                                                uploadIds.remove(uploadInfo.uploadId)
                                                if (uploadIds.isEmpty()) {
                                                    if (progressDialog.isShowing)
                                                        progressDialog.dismiss()
                                                    if (!activity.isDestroyed || !activity.isFinishing) {
                                                        val dialog = CustomDialog(activity)
                                                        dialog.show()
                                                        dialog.setMessage(successMessage)
                                                        dialog.setCancelable(false)
                                                        dialog.setPositiveButton(R.string.ok, View.OnClickListener {
                                                            dialog.dismiss()
                                                            startActivity(Intent(activity, MainActivity::class.java)
                                                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP))
                                                        })
                                                    }
                                                }
                                            }

                                            override fun onCompleted(context: Context, uploadInfo: UploadInfo, serverResponse: ServerResponse?) {
                                                uploadIds.remove(uploadInfo.uploadId)
                                                if (uploadIds.isEmpty()) {
                                                    progressDialog.dismiss()
                                                    val dialog = CustomDialog(activity)
                                                    dialog.show()
                                                    dialog.setMessage(successMessage)
                                                    dialog.setCancelable(false)
                                                    dialog.setPositiveButton(R.string.ok, View.OnClickListener {
                                                        dialog.dismiss()
                                                        startActivity(Intent(activity, MainActivity::class.java)
                                                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP))
                                                    })
                                                }
                                            }

                                            override fun onCancelled(context: Context, uploadInfo: UploadInfo) {}
                                        })
                                        .startUpload()
                                uploadIds.add(uploadId)
                            } catch (exc: Exception) {
                                Log.e("AndroidUploadService", exc.message, exc)
                            }
                        }
                    } else {
                        val mPosition = position + 1
                        compressFromArray(mPosition)
                    }
                }) { throwable -> throwable.printStackTrace() }
    }*/

    /*@SuppressLint("CheckResult")
    fun compressFromArray(position: Int) {
        Compressor(this)
                .compressToFileAsFlowable(File(lstMedia[position]))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ mFile: File ->
                    val options = BitmapFactory.Options()
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888
                    var bitmap = BitmapFactory.decodeFile(mFile.absolutePath, options)
                    val thumb: Bitmap
                    bitmap = if (category.id == 5) { //pashu palan mate vadhare
                        scaleDown(bitmap, 1200f, false)
                    } else {
                        scaleDown(bitmap, 900f, false)
                    }
                    thumb = scaleDown(bitmap, 350f, false)
                    var fOut: OutputStream? = null
                    var fOutThumb: OutputStream? = null
                    var fileThumb: File? = null
                    try {
                        fOut = FileOutputStream(mFile)
                        val dir = mFile.parent
                        fileThumb = File(dir + "/thumb_" + mFile.name)
                        fOut = FileOutputStream(mFile)
                        fOutThumb = FileOutputStream(fileThumb)
                    } catch (e: FileNotFoundException) {
                        e.printStackTrace()
                    }
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut) // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
                    thumb.compress(Bitmap.CompressFormat.JPEG, 85, fOutThumb) // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
                    try {
                        fOut!!.flush() // Not really required
                        fOut.close()
                        fOutThumb!!.flush() // Not really required
                        fOutThumb.close()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                    lstMediaCompressedThumb.add(fileThumb!!.absolutePath)
                    Log.i("TAG", "onPostExecute: " + position + ": " + mFile.absolutePath)
                    lstMediaCompressed.add(position, mFile.absolutePath)
                    if (position == lstMedia!!.size - 1) {
                        for (k in lstMediaCompressed.indices) { //for (String file : lstMediaCompressed) {
                            try {
                                val uploadNotificationConfig = UploadNotificationConfig()
                                uploadNotificationConfig.setClearOnActionForAllStatuses(true)
                                uploadNotificationConfig.setTitleForAllStatuses(getString(R.string.app_name))
                                val uploadId = MultipartUploadRequest(activity, storeUserData.getString(Constants.URL) + "${API.version}/news/addNewsMedia")
                                        .addFileToUpload(lstMediaCompressed[k], "file")
                                        .addFileToUpload(lstMediaCompressedThumb[k], "thumb")
                                        .addParameter("type", if (getMimeType(lstMediaCompressed[k])!!.startsWith("image")) "1" else "3")
                                        .addParameter("postId", postId)
                                        .setDelegate(object : UploadStatusDelegate {
                                            override fun onProgress(context: Context, uploadInfo: UploadInfo) {}
                                            override fun onError(context: Context, uploadInfo: UploadInfo, serverResponse: ServerResponse?, exception: Exception) {
                                                uploadIds.remove(uploadInfo.uploadId)
                                                if (uploadIds.isEmpty()) {
                                                    if (progressDialog.isShowing)
                                                        progressDialog.dismiss()
                                                    if (!activity.isDestroyed || !activity.isFinishing) {
                                                        val dialog = CustomDialog(activity)
                                                        dialog.show()
                                                        dialog.setMessage(successMessage)
                                                        dialog.setCancelable(false)
                                                        dialog.setPositiveButton(R.string.ok, View.OnClickListener {
                                                            dialog.dismiss()
                                                            startActivity(Intent(activity, MainActivity::class.java)
                                                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP))
                                                        })
                                                    }
                                                }
                                            }

                                            override fun onCompleted(context: Context, uploadInfo: UploadInfo, serverResponse: ServerResponse?) {
                                                uploadIds.remove(uploadInfo.uploadId)
                                                if (uploadIds.isEmpty()) {
                                                    progressDialog.dismiss()
                                                    val dialog = CustomDialog(activity)
                                                    dialog.show()
                                                    dialog.setMessage(successMessage)
                                                    dialog.setCancelable(false)
                                                    dialog.setPositiveButton(R.string.ok, View.OnClickListener {
                                                        dialog.dismiss()
                                                        startActivity(Intent(activity, MainActivity::class.java)
                                                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP))
                                                    })
                                                }
                                            }

                                            override fun onCancelled(context: Context, uploadInfo: UploadInfo) {}
                                        })
                                        .startUpload()
                                uploadIds.add(uploadId)
                            } catch (exc: Exception) {
                                Log.e("AndroidUploadService", exc.message, exc)
                            }
                        }
                    } else {
                        val mPosition = position + 1
                        compressFromArray(mPosition)
                    }
                }) { throwable -> throwable.printStackTrace() }
    }*/

    fun scaleDown(realImage: Bitmap, maxImageSize: Float,
                  filter: Boolean): Bitmap {
        val ratio = Math.min(
                maxImageSize / realImage.width,
                maxImageSize / realImage.height)
        val width = Math.round(ratio * realImage.width)
        val height = Math.round(ratio * realImage.height)
        return Bitmap.createScaledBitmap(realImage, width,
                height, filter)
    }

    fun addNews() {
        var pInfo: PackageInfo? = null
        try {
            pInfo = this.packageManager.getPackageInfo(packageName, 0)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        val version = pInfo!!.versionCode
        progressDialog.show()
        val result = Utils.CallApi(activity).addNews(
                storeUserData.getString(Constants.USER_ID),
                category.id,
                ed_title.text.toString().trim(),
                ed_description.text.toString().trim(),
                ed_location.text.toString().trim(),
                ed_approx_price.text.toString().trim(),
                districtId,
                version
        )
        //lstMediaCompressed.clear()
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.code() != 200) {
                    serverAlert(activity)
                    return
                }
                val responseData = response.body()!!.string()
                Log.i("response", "onResponse: $responseData")
                if (!TextUtils.isEmpty(responseData)) {
                    val jsonObject = JSONObject(responseData)
                    if (jsonObject.getBoolean("success")) {
                        postId = jsonObject.getString("postId")
                        successMessage = jsonObject.getString("message")
                        confirmDialog.dismiss()
                        lstMedia.removeAt(0) //Add Parameter
                        if (lstMedia.size > 0) {
                            progressDialog.setTitle(getString(R.string.uploading_photo))
                            compressFromArray(0)
                            //compressToWebP(0);
                        } else {
                            dismissProgress()
                            val dialog = CustomDialog(activity)
                            dialog.show()
                            dialog.setMessage(successMessage)
                            dialog.setCancelable(false)
                            dialog.setPositiveButton(R.string.ok, View.OnClickListener {
                                dialog.dismiss()
                                startActivity(Intent(activity, MainActivity::class.java)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP))
                            })
                        }
                    } else {
                        dismissProgress()
                        showAlert(activity, jsonObject.getString("message"))
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                dismissProgress()
                t.printStackTrace()
            }
        })
    }

    fun getMimeType(url: String?): String? {
        var type: String = ""
        val extension = MimeTypeMap.getFileExtensionFromUrl(url)
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)!!
        }
        Log.i("TAG", "getMimeType: $type")
        return type
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    interface ItemClickListener {
        fun onClick(view: View?, position: Int)
        fun onDistrictSelect(pojo: DistrictPojo)
    }

    inner class ConfirmDialog(context: Context) : Dialog(context), View.OnClickListener {

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(R.layout.dialog_confirm_upload)

            tv_title.text = activity.ed_title.text.toString()
            tv_description.text = activity.ed_description.text.toString()
            btn_cancel.setOnClickListener(this)
            btn_agree.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            if (v === btn_agree) {
                MainApplication.instance?.loadFullScreenAds(true)
                dismiss()
                addNews()
            } else if (v === btn_cancel) {
                dismiss()
            }
        }
    }

    private fun getDistrictList() {
        var dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        if (dateFormat.format(Date()) != storeUserData.getString(Constants.NEWS_CAT_SYNC_DATE) || TextUtils.isEmpty(storeUserData.getString(Constants.DISTRICT_LIST))) {
            val result = Utils.CallApi(activity).getDistrict()
            result.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    if (response.code() != 200) {

                        return
                    }
                    try {
                        val res = response.body()!!.string()
                        Log.i("response", "onResponse: $res")
                        storeUserData.setString(Constants.DISTRICT_LIST, res)
                    } catch (e: IOException) {
                        e.printStackTrace()
                    } catch (e: IllegalStateException) {
                        e.printStackTrace()
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    t.printStackTrace()
                }
            })
        }
    }
}