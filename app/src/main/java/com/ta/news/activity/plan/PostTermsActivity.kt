package com.ta.news.activity.plan

import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.core.content.ContextCompat
import com.ta.news.R
import com.ta.news.activity.BaseActivity
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.activity_post_terms.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PostTermsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_terms)
        activity = this
        storeUserData = StoreUserData(activity)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val upArrow = ContextCompat.getDrawable(activity, R.drawable.left_arrow)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            upArrow?.colorFilter = BlendModeColorFilter(ContextCompat.getColor(activity, R.color.white), BlendMode.SRC_ATOP)
        } else {
            upArrow?.setColorFilter(ContextCompat.getColor(activity, R.color.white), PorterDuff.Mode.SRC_ATOP)
        }
        supportActionBar?.setHomeAsUpIndicator(upArrow)
        if (Utils.isOnline(activity)) {
            postTerms()
        } else {
            webView.visibility = View.GONE
            ivNoInternet.visibility = View.VISIBLE
        }
    }

    fun postTerms() {
        progressDialog.show()
        val result = Utils.CallApi(activity).getPage("adTerms")
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (progressDialog.isShowing) progressDialog.dismiss()
                val responseData = response.body()!!.string()
                Log.i("response", "onResponse: $responseData")
                if (!TextUtils.isEmpty(responseData)) {
                    val jsonObject = JSONObject(responseData)
                    if (jsonObject.getBoolean("success")) {
                        webView.loadData(jsonObject.getString("data"), "text/html; charset=UTF-8", null)
                    } else {
                        showAlert(activity, jsonObject.getString("message"))
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                if (progressDialog.isShowing) progressDialog.dismiss()
                t.printStackTrace()
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean { // handle arrow click here
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}