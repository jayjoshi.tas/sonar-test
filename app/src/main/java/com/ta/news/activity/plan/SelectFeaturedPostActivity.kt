package com.ta.news.activity.plan

import android.app.Activity
import android.content.Intent
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import com.ta.news.R
import com.ta.news.activity.BaseActivity
import com.ta.news.activity.MainActivity
import com.ta.news.adapter.SelectFeaturedAdapter
import com.ta.news.controls.CustomDialog
import com.ta.news.pojo.News
import com.ta.news.pojo.NewsPojo
import com.ta.news.pojo.PlansPojo
import com.ta.news.utils.Constants
import com.ta.news.utils.OnLoadMoreListener
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.activity_select_featured_post.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.io.Reader
import java.io.StringReader
import java.lang.reflect.Modifier

class SelectFeaturedPostActivity : BaseActivity(), PaymentResultListener {
    lateinit var planPojo: PlansPojo
    var page = 1
    lateinit var layoutManager: LinearLayoutManager
    lateinit var news: NewsPojo
    var selectedCategories = ""
    var totalPages = 1
    lateinit var adapter: SelectFeaturedAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = this
        storeUserData = StoreUserData(activity)
        setContentView(R.layout.activity_select_featured_post)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val upArrow = ContextCompat.getDrawable(activity, R.drawable.left_arrow)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            upArrow?.colorFilter = BlendModeColorFilter(ContextCompat.getColor(activity, R.color.white), BlendMode.SRC_ATOP)
        } else {
            upArrow?.setColorFilter(ContextCompat.getColor(activity, R.color.white), PorterDuff.Mode.SRC_ATOP)
        }
        supportActionBar?.setHomeAsUpIndicator(upArrow)

        planPojo = intent.getSerializableExtra("featuredPlan") as PlansPojo

        layoutManager = LinearLayoutManager(activity)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = layoutManager
        adapter = SelectFeaturedAdapter(activity, recyclerView, Constants.lstNews, onAdSelect)
        recyclerView.adapter = adapter

        adapter.setOnLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                if (page < totalPages) {
                    page += 1
                    data
                }
            }
        })
        data
    }

    interface OnAdSelect {
        fun onSelect(post: NewsPojo)
    }

    var onAdSelect = object : OnAdSelect {
        override fun onSelect(post: NewsPojo) {
            selectedCategories = ""
            news = post
            startActivityForResult(Intent(activity, SelectFeaturedCategoryActivity::class.java), 123)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 123 && resultCode == Activity.RESULT_OK) {
            selectedCategories = data!!.getStringExtra("selectedCategories")
            val dialog = CustomDialog(activity)
            dialog.show()
            dialog.setCancelable(false)
            dialog.setMessage(getString(R.string.continue_for_payment))
            dialog.setPositiveButton(R.string.ok, View.OnClickListener {
                dialog.dismiss()
                startPayment()
            })
        }
    }

    private fun startPayment() {
        val activity: Activity = this
        val co = Checkout()

        try {
            val options = JSONObject()
            options.put("name", "#" + planPojo.id)
            options.put("description", planPojo.planName)
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png")
            options.put("theme.color", "#37a000")
            options.put("currency", "INR")
            //options.put("order_id", storeUserData.getString(Constants.USER_ID) + "_" + System.currentTimeMillis())
            options.put("amount", planPojo.price.toDouble() * 100)//pass amount in currency subunits

            val prefill = JSONObject()
            prefill.put("email", storeUserData.getString(Constants.USER_EMAIL))
            prefill.put("contact", storeUserData.getString(Constants.USER_COUNTRY_CODE) + storeUserData.getString(Constants.USER_PHONE_NUMBER))
            options.put("prefill", prefill)
            co.open(activity, options)
        } catch (e: Exception) {
            Toast.makeText(activity, "Error in payment: " + e.message, Toast.LENGTH_LONG).show()
            e.printStackTrace()
        }
    }

    private fun addOrder(transactionId: String) {
        showProgress()
        val result = Utils.CallApi(activity).setFeaturedPost(
                storeUserData.getString(Constants.USER_ID),
                planPojo.id,
                planPojo.price,
                news.postId,
                selectedCategories,
                planPojo.planDay,
                transactionId
        )
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                dismissProgress()
                if (response.code() != 200) {
                    serverAlert(activity)
                    return
                }
                try {
                    val res = response.body()!!.string()
                    Log.i("response", "onResponse: $res")
                    var json = JSONObject(res)
                    val dialog = CustomDialog(activity)
                    dialog.show()
                    dialog.setCancelable(false)
                    dialog.setMessage(json.getString("message"))
                    dialog.setPositiveButton(R.string.ok, View.OnClickListener {
                        if (json.getBoolean("success")) {
                            startActivity(Intent(activity, MainActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
                        } else {
                            dialog.dismiss()
                        }
                    })

                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                } catch (e: JsonSyntaxException) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                dismissProgress()
            }
        })
    }

    override fun onPaymentSuccess(transactionId: String?) {
        addOrder(transactionId!!)
    }

    override fun onPaymentError(p0: Int, p1: String?) {
        showAlert(activity, p1!!)
    }

    val data: Unit
        get() {
            if (Utils.isOnline(activity)) {
                userNews
            }
        }

    val userNews: Unit
        get() {
            if (page == 1) showProgress()
            val result = Utils.CallApi(activity).newsHistory(storeUserData.getString(Constants.USER_ID), page)
            result.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    dismissProgress()
                    try {
                        if (response.code() != 200) {
                            serverAlert(activity)
                            return
                        }
                        val res = response.body()!!.string()
                        Log.i("response", "onResponse: $res")
                        val reader: Reader = StringReader(res)
                        val gson = GsonBuilder()
                                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                                .serializeNulls()
                                .create()
                        val news = gson.fromJson(reader, News::class.java)
                        totalPages = news.totalPages
                        if (page == 1) {
                            Constants.lstNews.clear()
                        } else {
                            //hide progress
                        }
                        if (news.data != null)
                            Constants.lstNews.addAll(news.data!!)
                        Log.i("TAG", "onResponse: " + "page = " + page + "-\n total - " + totalPages + "\n" + Constants.lstNews.size)
                        /*if (page == 1 && Constants.lstNews.isEmpty()) {
                            swipeRefresh.visibility = View.GONE
                            ivNoInternet.visibility = View.VISIBLE
                            tvMessage.setText(R.string.not_posted)
                            ivWarning.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.empty_icon))
                        }*/
                        adapter.notifyDataSetChanged()
                        adapter.setLoaded()
                    } catch (e: IOException) {
                        e.printStackTrace()
                        serverAlert(activity)
                    } catch (e: IllegalStateException) {
                        e.printStackTrace()
                        serverAlert(activity)
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        serverAlert(activity)
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    t.printStackTrace()
                }
            })
        }

    override fun onOptionsItemSelected(item: MenuItem): Boolean { // handle arrow click here
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}