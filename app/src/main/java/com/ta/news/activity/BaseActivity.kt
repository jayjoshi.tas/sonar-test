package com.ta.news.activity

import android.app.Activity
import android.content.res.Configuration
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.facebook.ads.*
import com.google.android.gms.ads.AdRequest
import com.ta.news.R
import com.ta.news.controls.CustomDialog
import com.ta.news.controls.CustomProgressDialog
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import java.util.*


open class BaseActivity : AppCompatActivity() {

    lateinit var progressDialog: CustomProgressDialog
    lateinit var activity: AppCompatActivity
    lateinit var storeUserData: StoreUserData
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var language = StoreUserData(this).getString(Constants.USER_LANGUAGE)
       /* if (language.isEmpty()) {
            language = "en"
        }*/
        val locale = Locale(language)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        baseContext.resources.updateConfiguration(config,
                baseContext.resources.displayMetrics)
        window.decorView.layoutDirection = View.LAYOUT_DIRECTION_LTR
        progressDialog = CustomProgressDialog(this)
        progressDialog.setCancelable(false)
    }

    fun setBackArrowColor(toolbar:Toolbar,supportActionBar: ActionBar?) {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val upArrow = ContextCompat.getDrawable(activity, R.drawable.left_arrow)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            upArrow?.colorFilter = BlendModeColorFilter(ContextCompat.getColor(activity, R.color.white), BlendMode.SRC_ATOP)
        } else {
            upArrow?.setColorFilter(ContextCompat.getColor(activity, R.color.white), PorterDuff.Mode.SRC_ATOP)
        }
        supportActionBar?.setHomeAsUpIndicator(upArrow)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    fun showProgress() {
        progressDialog.show()
    }

    fun dismissProgress() {
        if (progressDialog.isShowing)
            progressDialog.dismiss()
    }

    override fun onDestroy() {
        if (progressDialog.isShowing) {
            progressDialog.dismiss()
        }
        super.onDestroy()
    }

    fun showAlert(activity: Activity, message: String) {
        if (!activity.isFinishing) {
            val dialog = CustomDialog(activity)
            dialog.show()
            dialog.setCancelable(false)
            dialog.setMessage(message)
            dialog.setPositiveButton(R.string.ok)
        }
    }

    fun showAlert(activity: Activity, message: String, isFinish: Boolean) {
        if (!activity.isFinishing) {
            val dialog = CustomDialog(activity)
            dialog.show()
            dialog.setCancelable(false)
            dialog.setMessage(message)
            dialog.setPositiveButton(R.string.ok)
            if (isFinish) {
                dialog.setPositiveButton(R.string.ok, View.OnClickListener { finish() })
            }
        }
    }

    fun internetAlert(activity: Activity) {
        if (!activity.isFinishing) {
            val dialog = CustomDialog(activity)
            dialog.show()
            dialog.setMessage(R.string.internet_error_message)
            dialog.setPositiveButton(R.string.ok)
        }
    }

    fun serverAlert(activity: Activity) {
        if (!activity.isFinishing) {
            val dialog = CustomDialog(activity)
            dialog.show()
            dialog.setTitle(R.string.server_error_title)
            dialog.setMessage(R.string.server_error_message)
            dialog.setPositiveButton(R.string.ok)
        }
    }

    fun loadBannerAds(banner_container: LinearLayout, googleAdView: com.google.android.gms.ads.AdView, id: Int) {
        if (!storeUserData.getBoolean(Constants.IS_PREMIUM)) {
            var adView = AdView(activity, getString(id), AdSize.BANNER_HEIGHT_50)
            banner_container.addView(adView)
            var adListener = object : AdListener {
                override fun onAdClicked(p0: Ad?) {

                }

                override fun onError(p0: Ad?, p1: AdError?) {
                    googleAdView.visibility = View.VISIBLE
                    googleAdView.loadAd(AdRequest.Builder().build())
                }

                override fun onAdLoaded(p0: Ad?) {

                }

                override fun onLoggingImpression(p0: Ad?) {

                }
            }
            var buildConfig = adView.buildLoadAdConfig().withAdListener(adListener).build()
            adView.loadAd(buildConfig)
        } /*else {
            googleAdView.visibility = View.VISIBLE
            googleAdView.loadAd(AdRequest.Builder().build())
        }*/
    }
}