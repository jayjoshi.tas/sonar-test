package com.ta.news.activity.plan

import android.content.Intent
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.core.content.ContextCompat
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import com.ta.news.R
import com.ta.news.activity.BaseActivity
import com.ta.news.adapter.SelectFeaturedCategoryAdapter
import com.ta.news.pojo.NewsCategory
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.activity_select_featured_category.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.io.Reader
import java.io.StringReader
import java.lang.reflect.Modifier


class SelectFeaturedCategoryActivity : BaseActivity() {
    lateinit var adapter: SelectFeaturedCategoryAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = this
        storeUserData = StoreUserData(activity)
        setContentView(R.layout.activity_select_featured_category)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val upArrow = ContextCompat.getDrawable(activity, R.drawable.left_arrow)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            upArrow?.colorFilter = BlendModeColorFilter(ContextCompat.getColor(activity, R.color.white), BlendMode.SRC_ATOP)
        } else {
            upArrow?.setColorFilter(ContextCompat.getColor(activity, R.color.white), PorterDuff.Mode.SRC_ATOP)
        }
        supportActionBar?.setHomeAsUpIndicator(upArrow)
        if (Utils.isOnline(activity)) {
            categoryFromServer
        } else {
            internetAlert(activity)
        }
        showAlert(activity, getString(R.string.featured_category_select_title))

        btnSubmit.setOnClickListener {
            val returnIntent = Intent()
            returnIntent.putExtra("selectedCategories", adapter.getSelectedCategories())
            setResult(RESULT_OK, returnIntent)
            finish()
        }
    }

    val categoryFromServer: Unit
        get() {
            val result = Utils.CallApi(activity).newsCategory("add")
            result.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    try {
                        if (response.code() != 200) {
                            serverAlert(activity)
                            return
                        }
                        val res = response.body()!!.string()
                        Log.i("response", "onResponse: $res")
                        val reader: Reader = StringReader(res)
                        val gson = GsonBuilder()
                                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                                .serializeNulls()
                                .create()
                        val newsCategory = gson.fromJson(reader, NewsCategory::class.java)
                        adapter = SelectFeaturedCategoryAdapter(activity, newsCategory.data)
                        recyclerView.adapter = adapter
                    } catch (e: IOException) {
                        e.printStackTrace()
                        serverAlert(activity)
                    } catch (e: IllegalStateException) {
                        e.printStackTrace()
                        serverAlert(activity)
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        serverAlert(activity)
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    t.printStackTrace()
                }
            })
        }

    override fun onOptionsItemSelected(item: MenuItem): Boolean { // handle arrow click here
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    interface ItemClickListener {
        fun onClick(view: View?, position: Int)
    }
}