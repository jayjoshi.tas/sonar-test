package com.ta.news.activity

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.PersistableBundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.activeandroid.query.Select
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.gson.GsonBuilder
import com.ta.news.BuildConfig
import com.ta.news.R
import com.ta.news.activity.plan.PlanTypeActivity
import com.ta.news.controls.CustomDialog
import com.ta.news.fragment.*
import com.ta.news.media.image.ImagePicker
import com.ta.news.pojo.Notification
import com.ta.news.pojo.SplashScreens
import com.ta.news.services.DownloadService
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.Reader
import java.io.StringReader
import java.lang.reflect.Modifier

class MainActivity : BaseActivity() {

    var categoryFragment: CategoryFragment? = null
    lateinit var profileFragment: ProfileFragment
    var rate = ""
    var doubleBackToExitPressedOnce = false
    lateinit var appUpdateManager: AppUpdateManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = this
        storeUserData = StoreUserData(activity)
        setContentView(R.layout.activity_main)
        initViews()
        if (StoreUserData(this).getString(Constants.USER_ID).isEmpty()) {
            startActivity(Intent(activity, SplashActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
        }
        //FirebaseAnalytics.getInstance(this).logEvent("main_activity_ready",null)

        Utils.loadUserImage(activity, storeUserData.getString(Constants.USER_IMAGE), logo)
        version.text = getString(R.string.version) + BuildConfig.VERSION_CODE + " - " + BuildConfig.VERSION_NAME
        profileFragment = ProfileFragment()
        profileLayout.setOnClickListener {
            changeFragment(profileFragment, R.string.profile, null, false)
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        btn_home.setOnClickListener {
            categoryFragment = CategoryFragment()
            changeFragment(categoryFragment!!, R.string.sell_buy, null, false)
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        btn_content.setOnClickListener {
            changeFragment(ContentFragment(), R.string.content, null, false)
            drawer_layout.closeDrawer(GravityCompat.START)
        }

        btn_mandi_price.setOnClickListener {
            changeFragment(MandiPriceFragment(), R.string.mandi_price, null, false)
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        btn_saved.setOnClickListener {
            changeFragment(SavedNewsFragment(), R.string.saved, null, false)
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        btn_notifications.setOnClickListener {
            changeFragment(NotificationFragment(), R.string.notifications, null, false)
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        btn_weather.setOnClickListener {
            changeFragment(WeatherFragment(), R.string.weather, null, false)
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        btn_paid_ads.setOnClickListener {
            changeFragment(PaidAdsInfoFragment(), R.string.business_ad, null, false)
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        btn_remove_ads.setOnClickListener {
            drawer_layout.closeDrawer(GravityCompat.START)
            startActivity(Intent(activity, PlanTypeActivity::class.java))
        }
        btn_books.setOnClickListener {
            changeFragment(BooksFragment(), R.string.books, null, false)
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        btn_info.setOnClickListener {
            changeFragment(AboutUsFragment(), R.string.about_us, null, false)
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        btn_contact_us.setOnClickListener {
            //startActivity(Intent(activity, JivoActivity::class.java))
            changeFragment(ContactUsFragment(), R.string.contact_us, null, false)
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        btn_terms_and_conditions.setOnClickListener {
            changeFragment(TermsAndConditionsFragment(), R.string.terms_and_conditions, null, false)
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        btn_uploaded.setOnClickListener {
            changeFragment(UploadedFragment(), R.string.sent, null, false)
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        btn_whatsapp.setOnClickListener {
            changeFragment(WhatsAppFragment(), R.string.do_whatsapp, null, false)
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        btn_videos.setOnClickListener {
            drawer_layout.closeDrawer(GravityCompat.START)
            val Urlintent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://www.youtube.com/channel/UCUD41fawz9ARbMHDpzBKw4Q"))
            startActivity(Urlintent)
            // startActivity(new Intent(activity, VideosActivity.class));
        }
        btn_facebook.setOnClickListener {
            drawer_layout.closeDrawer(GravityCompat.START)
            try {
                val facebookIntent = Intent(Intent.ACTION_VIEW)
                val facebookUrl = getFacebookPageURL()
                facebookIntent.data = Uri.parse(facebookUrl)
                startActivity(facebookIntent)
            } catch (e: ActivityNotFoundException) {
                e.printStackTrace()
            }
        }
        btn_share.setOnClickListener {
            val sharingIntent = Intent(Intent.ACTION_SEND)
            sharingIntent.type = "text/plain"
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_app_title))
            sharingIntent.putExtra(Intent.EXTRA_TEXT, storeUserData.getString(Constants.SHARE_TEXT))
            startActivity(Intent.createChooser(sharingIntent, "Share using"))
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        /*if (TextUtils.isEmpty(Constants.HOW_TO_USE)) {
            btnHowToUse.setVisibility(View.GONE);
        } else {
            btnHowToUse.setVisibility(View.VISIBLE);
        }
        btnHowToUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube://" + Constants.HOW_TO_USE));
                startActivity(intent);
            }
        });*/

        btn_products.setOnClickListener {
            changeFragment(ProductListFragment(), R.string.store, null, false)
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        btn_rate.setOnClickListener { v: View? ->
            try {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName")))
            } catch (anfe: ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$packageName")))
            }
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        when (intent.getIntExtra("openCmd", -1)) {
            1 -> btn_notifications.performClick()
            2 -> {
            }
            3 -> btn_home.performClick()
            4 -> btn_uploaded.performClick()
            5 -> btn_mandi_price.performClick()
            else -> btn_home.performClick()
        }
        if (TextUtils.isEmpty(storeUserData.getString(Constants.USER_CITY))) {
            profileLayout.performClick()
            showAlert(activity, getString(R.string.pls_complete_your_profile))
        }
        // ATTENTION: This was auto-generated to handle app links.
        val appLinkIntent = intent
        val appLinkAction = appLinkIntent.action
        val appLinkData = appLinkIntent.data
        Log.i("TAG", "onCreate: $appLinkAction")
        Log.i("TAG", "onCreate: $appLinkData")
        try {
            val pInfo = this.packageManager.getPackageInfo(packageName, 0)
            val version = pInfo.versionCode
            if (storeUserData.getInt(Constants.LIVE_VERSION) > version) {
                /*val dialog = CustomDialog(activity)
                dialog.show()
                dialog.setCancelable(!storeUserData.getBoolean(Constants.FORCE_UPDATE))
                dialog.setTitle(getString(R.string.app_update_title))
                dialog.setMessage(getString(R.string.app_update_message))
                dialog.setPositiveButton(getString(R.string.app_update_yes), View.OnClickListener {
                    try {
                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName")))
                    } catch (anfe: ActivityNotFoundException) {
                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$packageName")))
                    }
                    dialog.dismiss()
                })*/
            } else {
                rate = Constants.RATE_V + pInfo.versionCode
                Log.i("TAG", "rate: " + storeUserData.getInt(rate))
                if (storeUserData.getInt(rate) != -2 && storeUserData.getInt(rate) % 7 == 0) { // if -2 then no thanks or done so no need to ask
                    val dialog = CustomDialog(activity)
                    dialog.show()
                    dialog.setCancelable(!storeUserData.getBoolean(Constants.FORCE_UPDATE))
                    dialog.setTitle(getString(R.string.app_rate_title))
                    dialog.setMessage(getString(R.string.app_rate_msg))
                    dialog.setPositiveButton(getString(R.string.app_rate_yes), View.OnClickListener {
                        val appPackageName = packageName // getPackageName() from Context or Activity object
                        storeUserData.setInt(rate, -2)
                        try {
                            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
                        } catch (anfe: ActivityNotFoundException) {
                            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
                        }
                        dialog.dismiss()
                    })
                    dialog.setNegativeButton(getString(R.string.app_rate_no), View.OnClickListener { dialog.dismiss() })
                }
                Log.i("TAG", "rate:-- " + storeUserData.getInt(rate))
                if (storeUserData.getInt(rate) != -2) {
                    storeUserData.setInt(rate, storeUserData.getInt(rate) + 2)
                }
                Log.i("TAG", "rate:++ " + storeUserData.getInt(rate))
            }
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        checkForUpdate()
        /*val manager = ReviewManagerFactory.create(activity)
        val request = manager.requestReviewFlow()
        var reviewInfo: ReviewInfo? = null
        request.addOnCompleteListener { request ->
            if (request.isSuccessful) {
                //Received ReviewInfo object
                reviewInfo = request.result
                reviewInfo?.let {
                    val flow = manager.launchReviewFlow(this@MainActivity, it)
                    flow.addOnCompleteListener {
                        Log.i("TAG", "onCreate: ")
                        //Irrespective of the result, the app flow should continue
                    }
                }
            } else {
                Log.i("TAG", "onCreate: ${request.exception}")
                //Problem in receiving object
                reviewInfo = null
            }
        }*/
    }


    fun getFacebookPageURL(): String {
        return try {
            if (Utils.isAppInstalled(activity, "com.facebook.katana")) {
                "fb://page/$FACEBOOK_PAGE_ID"
            } else {
                FACEBOOK_URL //normal web url
            }
        } catch (e: Exception) {
            FACEBOOK_URL //normal web url
        }
    }

    override fun onResume() {
        super.onResume()
        displayNotification()
    }

    private fun displayNotification() {
        val list = Select().from(Notification::class.java).where("isRead = 0").execute<Notification>()
        Log.i("TAG", "onResume: " + list!!.size + "")
        if (list != null && list.size > 0) {
            tv_notification.visibility = View.VISIBLE
            tv_notification.text = String.format("%d", list.size)
        } else {
            tv_notification.visibility = View.GONE
        }
    }

    fun changeFragment(fragment: Fragment, title: Int, args: Bundle?, add: Boolean) {
        Log.i("TAG", "changeFragment: called")
        val ft: FragmentTransaction
        ft = if (add) {
            supportFragmentManager.beginTransaction().addToBackStack(resources.getString(title))
        } else supportFragmentManager.beginTransaction()
        if (args != null) fragment.arguments = args
        ft.replace(R.id.content_main, fragment).commitAllowingStateLoss()
        tv_title.text = resources.getString(title)
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) { //super.onSaveInstanceState(outState, outPersistentState);
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else if (categoryFragment != null) {
            if (categoryFragment!!.isAdded && categoryFragment!!.isVisible) {
                Log.i("TAG", "onBackPressed: isadded")
                exitApp()
            } else {
                changeFragment(categoryFragment!!, R.string.home, null, false)
            }
        } else {
            Log.i("TAG", "onBackPressed: isadded2")
            exitApp()
        }
    }

    private fun exitApp() {
        if (doubleBackToExitPressedOnce) {
            finish()
            return
        }
        doubleBackToExitPressedOnce = true
        Utils.showToast(activity, R.string.exit_app)
        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }

    private fun initViews() {

        setSupportActionBar(toolbar)
        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.app_name, R.string.app_name)
        drawer_layout.addDrawerListener(toggle)
        drawer_layout.addDrawerListener(object : DrawerLayout.DrawerListener {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {}
            override fun onDrawerOpened(drawerView: View) {
                displayNotification()
            }

            override fun onDrawerClosed(drawerView: View) {}
            override fun onDrawerStateChanged(newState: Int) {}
        })
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toggle.syncState()
        info.text = (storeUserData.getString(Constants.USER_FIRST_NAME)
                + " " + storeUserData.getString(Constants.USER_LAST_NAME)
                + "\n" + storeUserData.getString(Constants.USER_COUNTRY_CODE) + storeUserData.getString(Constants.USER_PHONE_NUMBER))


        if (storeUserData.getString(Constants.SPLASH_DATA).isNotEmpty()) {
            val reader: Reader = StringReader(storeUserData.getString(Constants.SPLASH_DATA))
            val gson = GsonBuilder()
                    .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                    .serializeNulls()
                    .create()
            val splashScreens = gson.fromJson(reader, SplashScreens::class.java)
            for (k in splashScreens.splashArray.indices) {
                val url = splashScreens.splashArray[k].image
                val fileName = url.substring(url.lastIndexOf('/') + 1)
                val splashImage = File(filesDir.absolutePath + "/" + fileName)
                if (!splashImage.exists()) {
                    val intent = Intent(activity, DownloadService::class.java)
                    intent.putExtra("url", url)
                    startService(intent)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val mPaths: List<String> = data!!.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH)!!
            if (!mPaths.isNullOrEmpty()) {
                profileFragment.setImage(mPaths[0])
            } else {
                Toast.makeText(activity, getString(R.string.image_selected), Toast.LENGTH_SHORT).show()
            }
        } else if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == Activity.RESULT_CANCELED) {
            Toast.makeText(activity, getString(R.string.image_sel_cancelled), Toast.LENGTH_SHORT).show()
        } else if (requestCode == 123 && resultCode == Activity.RESULT_OK) {
            appUpdateManager.completeUpdate()
        } else if (requestCode == 123 && resultCode == Activity.RESULT_CANCELED) {
            checkForUpdate()
        }
    }

    fun checkForUpdate() {
        // Creates instance of the manager.
        appUpdateManager = AppUpdateManagerFactory.create(activity)
        // Returns an intent object that you use to check for an update.
        val appUpdateInfoTask = appUpdateManager.appUpdateInfo
        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener { appUpdateInfo ->
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                // Request the update.
                appUpdateManager.startUpdateFlowForResult(
                        // Pass the intent that is returned by 'getAppUpdateInfo()'.
                        appUpdateInfo,
                        // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                        AppUpdateType.FLEXIBLE,
                        // The current activity making the update request.
                        this,
                        // Include a request code to later monitor this update request.
                        123)
            } else {
                Log.i("TAG", "checkForUpdate: ${appUpdateInfo.availableVersionCode()}")
            }
        }
    }

    companion object {
        const val FACEBOOK_URL = "https://www.facebook.com/piplanapane"
        const val FACEBOOK_PAGE_ID = "250680781763487"
    }
}