package com.ta.news.activity.auth

import android.content.Intent
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.MenuItem
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.facebook.ads.AdError
import com.facebook.ads.NativeAdsManager
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import com.ta.news.R
import com.ta.news.activity.BaseActivity
import com.ta.news.adapter.UserNewsAdapter
import com.ta.news.pojo.News
import com.ta.news.pojo.NewsPojo
import com.ta.news.utils.Constants
import com.ta.news.utils.OnLoadMoreListener
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.activity_profile.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.io.Reader
import java.io.StringReader
import java.lang.reflect.Modifier
import java.util.*

class ProfileActivity : BaseActivity() {
    private var mNativeAdsManager: NativeAdsManager? = null
    private var adLoader: AdLoader? = null
    var page = 1
    lateinit var layoutManager: LinearLayoutManager
    var totalPages = 1
    lateinit var adapter: UserNewsAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        activity = this
        storeUserData = StoreUserData(activity)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val upArrow = ContextCompat.getDrawable(activity, R.drawable.left_arrow)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            upArrow?.colorFilter = BlendModeColorFilter(ContextCompat.getColor(activity, R.color.white), BlendMode.SRC_ATOP)
        } else {
            upArrow?.setColorFilter(ContextCompat.getColor(activity, R.color.white), PorterDuff.Mode.SRC_ATOP)
        }
        supportActionBar?.setHomeAsUpIndicator(upArrow)
        if (Utils.isOnline(activity)) {
            profile
        } else {
            internetAlert(activity)
        }
        tvWhatsapp.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://api.whatsapp.com/send?phone=" + tvWhatsapp.text.toString().replace("+", ""))))
        }

        mNativeAdsManager = NativeAdsManager(activity, getString(R.string.fb_news_list_native), 8)
        mNativeAdsManager!!.setListener(object : NativeAdsManager.Listener {
            override fun onAdsLoaded() {
                Log.d("TestData", "AdError : Loaded")
            }

            override fun onAdError(adError: AdError?) {
                if (adError != null) {
                    Log.d("TestData", "AdError : ${adError.errorCode} :  ${adError.errorMessage}")
                }
                FirebaseAnalytics.getInstance(activity).logEvent("facebookAdFailed", Utils.getUserDetails(activity))
            }
        })


        val builder: AdLoader.Builder = AdLoader.Builder(activity, getString(R.string.google_news_list_native))
        adLoader = builder.forUnifiedNativeAd { unifiedNativeAd ->
            Log.i("MainActivity", "onUnifiedNativeAdLoaded:ad loaded ")
            adapter.unifiedNativeAd.add(unifiedNativeAd)
        }.withAdListener(
                object : AdListener() {
                    override fun onAdFailedToLoad(errorCode: Int) {
                        Log.e("MainActivity", "The previous native ad failed to load. Attempting to"
                                + " load another.${adLoader?.isLoading} $errorCode")
                        FirebaseAnalytics.getInstance(activity).logEvent("googleAdFailed", Utils.getUserDetails(activity))
                    }
                }).build()

        if (storeUserData.getBoolean(Constants.IS_FACEBOOK)) {
            FirebaseAnalytics.getInstance(activity).logEvent("facebookAdRequested", Utils.getUserDetails(activity))
            mNativeAdsManager?.loadAds()
        }
        FirebaseAnalytics.getInstance(activity).logEvent("googleAdRequested", Utils.getUserDetails(activity))
        adLoader?.loadAds(AdRequest.Builder().build(), 8)

        layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = layoutManager
        adapter = UserNewsAdapter(activity, recyclerView, Constants.lstNews, mNativeAdsManager!!)
        recyclerView.adapter = adapter

        newsFromServer
    }

    val newsFromServer: Unit
        get() {

            if (page == 1) progressDialog.show()
            val result = Utils.CallApi(activity).getUserNews(intent.getIntExtra("userId", -1), page)
            result.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    if (progressDialog.isShowing)
                        progressDialog.dismiss()
                    try {
                        if (response.code() != 200) {
                            serverAlert(activity)
                            return
                        }
                        val res = response.body()!!.string()
                        Log.i("response", "onResponse: $res")
                        val reader: Reader = StringReader(res)
                        val gson = GsonBuilder()
                                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                                .serializeNulls()
                                .create()
                        val news = gson.fromJson(reader, News::class.java)
                        if (news != null) {
                            if (news.success) {
                                totalPages = news.totalPages
                                if (page == 1) {
                                    if (news.advertisement != null && news.advertisement!!.size > 0) {
                                        Constants.lstAdvs.clear()
                                        Constants.lstAdvs.addAll(news.advertisement!!)
                                    }
                                }
                                setAdapter(news.data)
                            } else {
                                setAdapter(null)
                                showAlert(activity, news.message)
                            }

                        } else {
                            serverAlert(activity)
                        }
                        Log.i("TAG", "onResponse: " + "page = " + page + "-\n total - " + totalPages + "\n" + Constants.lstNews.size)
                    } catch (e: IOException) {
                        e.printStackTrace()
                        adapter.setLoaded()
                        serverAlert(activity)
                    } catch (e: IllegalStateException) {
                        e.printStackTrace()
                        adapter.setLoaded()
                        serverAlert(activity)
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        adapter.setLoaded()
                        serverAlert(activity)
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    t.printStackTrace()
                    adapter.setLoaded()
                    if (progressDialog.isShowing)
                        progressDialog.dismiss()
                }
            })
        }


    private fun setAdapter(newData: ArrayList<NewsPojo>?) {
        if (page == 1) {
            Constants.lstNews.clear()
            adapter.setOnLoadMoreListener(null)
            if (newData != null) {
                Constants.lstNews.addAll(newData)
                setAdData()
                //adapter.notifyDataSetChanged()
                adapter.setOnLoadMoreListener(object : OnLoadMoreListener {
                    override fun onLoadMore() {
                        if (page < totalPages) {
                            page += 1
                            newsFromServer
                        }
                    }
                })
            }
            adapter.notifyDataSetChanged()
            adapter.setLoaded()
        } else {
            if (newData != null)
                Constants.lstNews.addAll(newData)
            setAdData()
            adapter.notifyDataSetChanged()
            adapter.setLoaded()
        }
    }

    private var currentAdPosition = 9
    private fun setAdData() {
        if (storeUserData.getBoolean(Constants.IS_PREMIUM)) {
            return
        }

        for (i in currentAdPosition until Constants.lstNews.size) {
            if ((i + 1) % 10 == 0) {
                Constants.lstNews.add(i, NewsPojo(isAd = true))
            }
            currentAdPosition = i + 10
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean { // handle arrow click here
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    val profile: Unit
        get() {
            progressDialog.show()
            val result = Utils.CallApi(activity).profileById(intent.getIntExtra("userId", -1))
            result.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    if (progressDialog.isShowing) progressDialog.dismiss()

                    val responseData = response.body()!!.string()
                    Log.i("response", "onResponse: $responseData")
                    if (!TextUtils.isEmpty(responseData)) {
                        val jsonObject = JSONObject(responseData)
                        if (jsonObject.getBoolean("success")) {
                            tvName.text = jsonObject.getString("firstName") + " " + jsonObject.getString("lastName")
                            tvContactDetail.text = jsonObject.optString("countryCode") + jsonObject.getString("mobileNo")
                            tvWhatsapp.text = jsonObject.optString("countryCode") + jsonObject.getString("mobileNo")
                            tvEmail.text = jsonObject.optString("email")
                            edAddress.text =
                                    jsonObject.getString("address") +
                                            "\n" + jsonObject.getString("city")
                            "\n" + jsonObject.getString("taluka")
                            "\n" + jsonObject.getString("pincode")
                            if (jsonObject.getInt("isVerified") == 1) {
                                tvName.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(activity, R.drawable.verified), null)
                            } else {
                                tvName.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
                            }
                            Utils.loadUserImage(activity, jsonObject.getString("profileImage"), image)


                        } else {
                            showAlert(activity, jsonObject.getString("message"))
                        }
                    }

                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    if (progressDialog.isShowing) progressDialog.dismiss()
                    t.printStackTrace()
                }
            })
        }
}