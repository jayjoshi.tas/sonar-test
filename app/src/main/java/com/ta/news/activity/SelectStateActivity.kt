package com.ta.news.activity

import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import androidx.core.view.isVisible
import com.google.android.gms.tasks.Task
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.google.gson.GsonBuilder
import com.ta.news.BuildConfig
import com.ta.news.R
import com.ta.news.adapter.StateAdapter
import com.ta.news.pojo.StateList
import com.ta.news.utils.Constants
import com.ta.news.utils.RetrofitHelper
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.activity_select_state.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.io.Reader
import java.io.StringReader
import java.lang.reflect.Modifier
import java.security.MessageDigest

class SelectStateActivity : BaseActivity() {
    var count = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_state)
        activity = this
        storeUserData = StoreUserData(activity)
        if (storeUserData.getString(Constants.STATE_LIST).isNotEmpty()) {
            val reader: Reader = StringReader(storeUserData.getString(Constants.STATE_LIST))
            val gson = GsonBuilder()
                    .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                    .serializeNulls()
                    .create()
            val states = gson.fromJson(reader, StateList::class.java)
            recyclerView.adapter = StateAdapter(activity, states.data)
            progress.isVisible = false
        } else {
            checkRemoteConfig()
        }
    }

    private var mFirebaseRemoteConfig: FirebaseRemoteConfig? = null
    var taskIsSuccessful = false
    private fun checkRemoteConfig() {
        showProgress()
        var bundle = Utils.getUserDetails(activity)
        bundle.putInt("count", count)
        FirebaseAnalytics.getInstance(activity).logEvent("remoteConfig", bundle)

        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance()
        val configSettings = FirebaseRemoteConfigSettings.Builder()
                .build()
        mFirebaseRemoteConfig?.setConfigSettingsAsync(configSettings)
        var cacheExpiration: Long = 0
        mFirebaseRemoteConfig!!.fetch(cacheExpiration)
                .addOnCompleteListener(this) { task: Task<Void?> ->
                    Log.d("RemoteConfig", "${task.isSuccessful}")
                    taskIsSuccessful = task.isSuccessful
                    if (taskIsSuccessful) {
                        mFirebaseRemoteConfig!!.activateFetched()
                        storeUserData.setInt(Constants.SPLASH_IMAGE_TIME, if (mFirebaseRemoteConfig!!.getString("splashTime").trim().isEmpty()) 6000 else mFirebaseRemoteConfig!!.getString("splashTime").toInt())
                        storeUserData.setInt(Constants.LIVE_VERSION, if (mFirebaseRemoteConfig!!.getString("androidLiveVersionCode").trim().isEmpty()) BuildConfig.VERSION_CODE else mFirebaseRemoteConfig!!.getString("androidLiveVersionCode").toInt())
                        storeUserData.setBoolean(Constants.FORCE_UPDATE, mFirebaseRemoteConfig!!.getBoolean("isAndroidForceUpdate"))
                        storeUserData.setString(Constants.URL, mFirebaseRemoteConfig!!.getString("baseUrl"))
                        storeUserData.setString(Constants.SHARE_TEXT, mFirebaseRemoteConfig!!.getString("shareText"))
                        storeUserData.setString(Constants.STATE_LIST, mFirebaseRemoteConfig!!.getString("stateList"))
                        storeUserData.setBoolean(Constants.IS_FACEBOOK, mFirebaseRemoteConfig!!.getBoolean("isFacebookAds"))
                        if (storeUserData.getString(Constants.USER_STATE).isNotEmpty()) {
                            var reader = StringReader(storeUserData.getString(Constants.STATE_LIST))
                            var gson = GsonBuilder()
                                    .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                                    .serializeNulls()
                                    .create()
                            var states = gson.fromJson(reader, StateList::class.java)
                            for (state in states.data) {
                                if (storeUserData.getString(Constants.USER_STATE) == state.nm) {
                                    storeUserData.setString(Constants.URL, state.url)
                                }
                            }
                        }
                        val reader: Reader = StringReader(storeUserData.getString(Constants.STATE_LIST))
                        val gson = GsonBuilder()
                                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                                .serializeNulls()
                                .create()
                        val states = gson.fromJson(reader, StateList::class.java)
                        recyclerView.adapter = StateAdapter(activity, states.data)
                        progress.isVisible = false
                        Log.i("TAG", "onComplete: ")
                    } else {
                        Log.i("TAG", "false")
                        //Toast.makeText(activity, "Retrying to fetch data!", Toast.LENGTH_SHORT).show();
                    }
                }
                .addOnFailureListener {
                    getRemoteData()

                }
                .addOnCanceledListener {
                    getRemoteData()
                }
    }

    private fun getKeyHash(): String {
        val info: PackageInfo
        try {
            info = packageManager.getPackageInfo(BuildConfig.APPLICATION_ID, PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                var md: MessageDigest = MessageDigest.getInstance("MD5")
                md.update(signature.toByteArray())
                return String(Base64.encode(md.digest(), 0))
            }
        } catch (e1: Exception) {
            e1.printStackTrace()
        }
        return ""
    }

    private fun getRemoteData() {

        val retrofitHelper = RetrofitHelper(activity)
        var call: Call<ResponseBody> =
                retrofitHelper.api().getRemoteData("https://www.tilva-artsoft.com/piplanapaneMH.php", getKeyHash().trim())
        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseData = body.body()!!.string()
                Log.i("TAG", "onSuccess: $responseData")
                if (!TextUtils.isEmpty(responseData)) {
                    val jsonObject = JSONObject(responseData)
                    storeUserData.setInt(Constants.SPLASH_IMAGE_TIME, if (jsonObject.getString("splashTime").trim().isEmpty()) 6000 else jsonObject.getString("splashTime").toInt())
                    storeUserData.setInt(Constants.LIVE_VERSION, if (jsonObject.getString("androidLiveVersionCode").trim().isEmpty()) BuildConfig.VERSION_CODE else jsonObject.getString("androidLiveVersionCode").toInt())
                    storeUserData.setBoolean(Constants.FORCE_UPDATE, jsonObject.getBoolean("isAndroidForceUpdate"))
                    storeUserData.setString(Constants.URL, jsonObject.getString("baseUrl"))
                    storeUserData.setString(Constants.SHARE_TEXT, jsonObject.getString("shareText"))
                    storeUserData.setString(Constants.STATE_LIST, jsonObject.getString("stateList"))
                    storeUserData.setBoolean(Constants.IS_FACEBOOK, jsonObject.getBoolean("isFacebookAds"))
                    if (storeUserData.getString(Constants.USER_STATE).isNotEmpty()) {
                        var reader = StringReader(storeUserData.getString(Constants.STATE_LIST))
                        var gson = GsonBuilder()
                                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                                .serializeNulls()
                                .create()
                        var states = gson.fromJson(reader, StateList::class.java)
                        for (state in states.data) {
                            if (storeUserData.getString(Constants.USER_STATE) == state.nm) {
                                storeUserData.setString(Constants.URL, state.url)
                            }
                        }
                    }
                    val reader: Reader = StringReader(storeUserData.getString(Constants.STATE_LIST))
                    val gson = GsonBuilder()
                            .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                            .serializeNulls()
                            .create()
                    val states = gson.fromJson(reader, StateList::class.java)
                    recyclerView.adapter = StateAdapter(activity, states.data)
                    progress.isVisible = false
                }
            }

            override fun onError(code: Int, error: String) {
                dismissProgress()
            }
        })
    }
}