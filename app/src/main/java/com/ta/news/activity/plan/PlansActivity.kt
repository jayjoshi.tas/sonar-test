package com.ta.news.activity.plan

import android.app.Activity
import android.content.Intent
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import com.ta.news.R
import com.ta.news.activity.BaseActivity
import com.ta.news.activity.post.NewPostCategoryActivity
import com.ta.news.adapter.PlanAdapter
import com.ta.news.controls.CustomDialog
import com.ta.news.pojo.Plans
import com.ta.news.pojo.PlansPojo
import com.ta.news.utils.Constants
import com.ta.news.utils.RetrofitHelper
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.activity_plans.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.io.Reader
import java.io.StringReader
import java.lang.reflect.Modifier

class PlansActivity : BaseActivity(), PaymentResultListener {

    var elseId = ""
    lateinit var adapter: PlanAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = this
        storeUserData = StoreUserData(activity)
        setContentView(R.layout.activity_plans)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val upArrow = ContextCompat.getDrawable(activity, R.drawable.left_arrow)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            upArrow?.colorFilter = BlendModeColorFilter(ContextCompat.getColor(activity, R.color.white), BlendMode.SRC_ATOP)
        } else {
            upArrow?.setColorFilter(ContextCompat.getColor(activity, R.color.white), PorterDuff.Mode.SRC_ATOP)
        }
        supportActionBar?.setHomeAsUpIndicator(upArrow)
        getPlans()

        buyForElse.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                buyForElse.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary))
                edMobile.setText("")
                userDetails.text = ""
                elseId = ""
            } else {
                buyForElse.setTextColor(ContextCompat.getColor(activity, R.color.d_grey))
            }
            llMobile.isVisible = isChecked
            btnVerify.isVisible = isChecked
            userDetails.isVisible = isChecked
            btnBuy.isVisible = !isChecked
        }
        btnVerify.setOnClickListener {
            if (buyForElse.isChecked && Utils.isEmpty(edMobile)) {
                showAlert(activity, getString(R.string.enter_mobile_number))
            } else if (buyForElse.isChecked && edMobile.text.toString() == storeUserData.getString(Constants.USER_PHONE_NUMBER)) {
                showAlert(activity, getString(R.string.buying_for_yourself))
            } else {
                getUserDetails()
            }
        }
        buyForMe.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                buyForMe.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary))
            } else {
                buyForMe.setTextColor(ContextCompat.getColor(activity, R.color.d_grey))
            }
        }

        btnBuy.setOnClickListener {
            if (!cbTerms.isChecked) {
                showAlert(activity, getString(R.string.accept_terms))
            } else if (adapter.selected == -1) {
                showAlert(activity, getString(R.string.please_select_plan))
            } else if (buyForElse.isChecked && Utils.isEmpty(edMobile)) {
                showAlert(activity, getString(R.string.please_enter_phone_number))
            } else if (buyForElse.isChecked && edMobile.text.toString() == storeUserData.getString(Constants.USER_PHONE_NUMBER)) {
                showAlert(activity, getString(R.string.buying_for_yourself))
            } else if (buyForMe.isChecked) {
                val dialog = CustomDialog(activity)
                dialog.show()
                dialog.setCancelable(false)
                dialog.setMessage(getString(R.string.continue_for_payment))
                dialog.setPositiveButton(R.string.ok, View.OnClickListener {
                    dialog.dismiss()
                    startPayment(adapter.list[adapter.selected])
                })
            } else if (buyForElse.isChecked) {
                val dialog = CustomDialog(activity)
                dialog.show()
                dialog.setCancelable(false)
                dialog.setMessage(String.format(getString(R.string.confirmation_for_plan_buy), edMobile.text.toString()))
                dialog.setPositiveButton(R.string.ok, View.OnClickListener {
                    dialog.dismiss()
                    startPayment(adapter.list[adapter.selected])
                })
            }
        }

        edMobile.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                userDetails.text = ""
                elseId = ""
                btnBuy.isVisible = false
                userDetails.isVisible = false
                btnVerify.isVisible = true
            }

            override fun afterTextChanged(s: Editable?) {

            }
        })

        terms.setOnClickListener {
            startActivity(Intent(activity, PostTermsActivity::class.java))
        }
    }

    private fun getUserDetails() {
        showProgress()
        val retrofitHelper = RetrofitHelper(activity)
        var call: Call<ResponseBody> =
                retrofitHelper.api().getProfileByMobile(
                        "+" + countryCode.selectedCountryCode.toString(),
                        edMobile.text.toString()
                )
        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                try {
                    val responseData = body.body()!!.string()
                    Log.i("response", "onResponse: $responseData")
                    if (!TextUtils.isEmpty(responseData)) {
                        val jsonObject = JSONObject(responseData)
                        if (jsonObject.getBoolean("success")) {
                            btnVerify.isVisible = false
                            btnBuy.isVisible = true
                            userDetails.isVisible = true
                            elseId = jsonObject.getString("userId")
                            userDetails.text = "${jsonObject.getString("firstName")} ${jsonObject.getString("lastName")}\n" +
                                    "${jsonObject.getString("address")}\n${jsonObject.getString("city")}\n${jsonObject.getString("taluka")}-${jsonObject.getString("pincode")}"
                        } else {
                            showAlert(activity, getString(R.string.no_user_found))
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onError(code: Int, error: String) {
                dismissProgress()
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun startPayment(plan: PlansPojo) {

        /*
        *  You need to pass current activity in order to let Razorpay create CheckoutActivity
        * */
        val activity: Activity = this
        val co = Checkout()

        try {
            val options = JSONObject()
            options.put("name", "#" + plan.id)
            options.put("description", plan.planName)
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png")
            options.put("theme.color", "#37a000")
            options.put("currency", "INR")
            //options.put("order_id", storeUserData.getString(Constants.USER_ID) + "_" + System.currentTimeMillis())
            options.put("amount", plan.price.toDouble() * 100)//pass amount in currency subunits

            val prefill = JSONObject()
            prefill.put("email", storeUserData.getString(Constants.USER_EMAIL))
            prefill.put("contact", storeUserData.getString(Constants.USER_COUNTRY_CODE) + storeUserData.getString(Constants.USER_PHONE_NUMBER))

            options.put("prefill", prefill)
            co.open(activity, options)
        } catch (e: Exception) {
            Toast.makeText(activity, "Error in payment: " + e.message, Toast.LENGTH_LONG).show()
            e.printStackTrace()
        }
    }

    private fun getPlans() {
        showProgress()
        val result = Utils.CallApi(activity).getPlans()
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                dismissProgress()
                if (response.code() != 200) {
                    serverAlert(activity)
                    return
                }
                try {
                    val res = response.body()!!.string()
                    Log.i("response", "onResponse: $res")
                    val reader: Reader = StringReader(res)
                    val gson = GsonBuilder()
                            .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                            .serializeNulls()
                            .create()
                    val plans = gson.fromJson(reader, Plans::class.java)
                    if (plans != null) {
                        if (plans.success && plans.data.isNotEmpty()) {
                            adapter = PlanAdapter(activity, plans.data)
                            rvPlans.isNestedScrollingEnabled = false
                            rvPlans.adapter = adapter
                        } else {
                            showAlert(activity, plans.message)
                        }
                    }

                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                } catch (e: JsonSyntaxException) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                dismissProgress()
            }
        })
    }

    private fun addOrder(transactionId: String) {
        showProgress()
        val result = Utils.CallApi(activity).addOrder(
                if (buyForElse.isChecked) elseId else storeUserData.getString(Constants.USER_ID),
                adapter.list[adapter.selected].id,
                adapter.list[adapter.selected].price,
                adapter.list[adapter.selected].totalPost,
                transactionId, storeUserData.getString(Constants.USER_ID)
        )
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                dismissProgress()
                if (response.code() != 200) {
                    serverAlert(activity)
                    return
                }
                try {
                    val res = response.body()!!.string()
                    Log.i("response", "onResponse: $res")
                    var json = JSONObject(res)
                    val dialog = CustomDialog(activity)
                    dialog.show()
                    dialog.setCancelable(false)
                    dialog.setMessage(json.getString("message"))
                    dialog.setPositiveButton(R.string.ok, View.OnClickListener {
                        if (json.getBoolean("success")) {
                            startActivity(Intent(activity, NewPostCategoryActivity::class.java))
                            finish()
                        } else {
                            dialog.dismiss()
                        }
                    })

                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                } catch (e: JsonSyntaxException) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                dismissProgress()
            }
        })
    }

    override fun onPaymentSuccess(transactionId: String?) {
        addOrder(transactionId!!)
    }

    override fun onPaymentError(p0: Int, p1: String?) {
        showAlert(activity, p1!!)
    }
}