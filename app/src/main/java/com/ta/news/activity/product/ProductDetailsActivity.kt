package com.ta.news.activity.product

import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.ta.news.R
import com.ta.news.activity.BaseActivity
import com.ta.news.adapter.HomeSliderPagerAdapter
import com.ta.news.pojo.ProductPojo
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.activity_product_details.*

class ProductDetailsActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_details)
        activity = this
        storeUserData = StoreUserData(activity)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val upArrow = ContextCompat.getDrawable(activity, R.drawable.left_arrow)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            upArrow?.colorFilter = BlendModeColorFilter(ContextCompat.getColor(activity, R.color.white), BlendMode.SRC_ATOP)
        } else {
            upArrow?.setColorFilter(ContextCompat.getColor(activity, R.color.white), PorterDuff.Mode.SRC_ATOP)
        }
        supportActionBar?.setHomeAsUpIndicator(upArrow)
        var productPojo = intent.getSerializableExtra("pojo") as ProductPojo
        tvProductName.text = productPojo.name
        tvProductdesc.text = productPojo.description
        tvProductPrice.text = "₹ ${productPojo.salePrice}"

        viewPager.clipToPadding = false;
        viewPager.setPadding(40, 0, 40, 0)
        viewPager.adapter = HomeSliderPagerAdapter(activity, productPojo.image)
        indicator.setViewPager(viewPager)

    }
}