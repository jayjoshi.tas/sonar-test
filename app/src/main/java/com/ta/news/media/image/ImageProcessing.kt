package com.ta.news.media.image

import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.os.Build
import com.ta.news.media.FileProcessing
import kotlin.collections.ArrayList

/**
 * Created by Alhazmy13 on 8/15/16.
 * MediaPicker
 */
internal object ImageProcessing {
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    fun processMultiImage(context: Context?, data: Intent?): ArrayList<String> {
        val listOfImgs: ArrayList<String> = ArrayList()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2 && null == data!!.data) {
            val clipdata = data.clipData
            for (i in 0 until (clipdata?.itemCount ?: 0)) {
                val selectedImage = clipdata!!.getItemAt(i).uri
                val selectedImagePath = FileProcessing.getPath(context, selectedImage)
                listOfImgs.add(selectedImagePath)
            }
        }
        return listOfImgs
    }
}