package com.ta.news.media.image

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.DialogFragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.ta.news.R
import com.ta.news.media.FileProcessing
import com.ta.news.media.Utility
import kotlinx.android.synthetic.main.dialog_image_picker.*
import java.io.*
import java.lang.ref.WeakReference
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by Alhazmy13 on 10/26/15.
 * MediaPicker
 */
class ImageActivity : AppCompatActivity() {
    private var destination: File? = null
    private var mImageUri: Uri? = null
    private var mImgConfig: ImageConfig? = null
    private var listOfImgs: ArrayList<String> = ArrayList()
    private var alertDialog: AlertDialog? = null
    private var optionFragment: OptionFragment? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        optionFragment = OptionFragment(this@ImageActivity)
        val intent = intent
        if (intent != null) {
            mImgConfig = intent.getSerializableExtra(ImageTags.Tags.IMG_CONFIG) as ImageConfig
        }
        if (savedInstanceState == null) {
            pickImageWrapper()
            listOfImgs = ArrayList()
        }
        if (mImgConfig!!.debug) Log.d(ImageTags.Tags.TAG, mImgConfig.toString())
    }

    override fun onStop() {
        if (alertDialog != null) alertDialog!!.dismiss()
        super.onStop()
    }

    private fun pickImage() {
        Utility.createFolder(mImgConfig!!.directory)
        destination = File(mImgConfig!!.directory, Utility.getRandomString() + mImgConfig!!.extension.value)
        when (mImgConfig!!.mode) {
            ImagePicker.Mode.CAMERA -> startActivityFromCamera()
            ImagePicker.Mode.GALLERY -> if (mImgConfig!!.allowMultiple && mImgConfig!!.allowOnlineImages) startActivityFromGalleryMultiImg() else startActivityFromGallery()
            ImagePicker.Mode.CAMERA_AND_GALLERY -> {
                //showFromCameraOrGalleryAlert()
                optionFragment?.show(
                        supportFragmentManager,
                        OptionFragment::class.java.simpleName
                )
            }
            else -> {
            }
        }
    }

    private fun showFromCameraOrGalleryAlert() {
        alertDialog = AlertDialog.Builder(this)
                .setTitle(getString(R.string.media_picker_select_from))
                .setPositiveButton(getString(R.string.media_picker_camera)) { dialogInterface, i ->
                    if (mImgConfig!!.debug) Log.d(ImageTags.Tags.TAG, "Alert Dialog - Start From Camera")
                    startActivityFromCamera()
                    alertDialog!!.dismiss()
                }
                .setNegativeButton(getString(R.string.media_picker_gallery)) { dialogInterface, i ->
                    if (mImgConfig!!.debug) Log.d(ImageTags.Tags.TAG, "Alert Dialog - Start From Gallery")
                    if (mImgConfig!!.allowMultiple) startActivityFromGalleryMultiImg() else startActivityFromGallery()
                    alertDialog!!.dismiss()
                }
                .setOnCancelListener {
                    if (mImgConfig!!.debug) Log.d(ImageTags.Tags.TAG, "Alert Dialog - Canceled")
                    alertDialog!!.dismiss()
                    finish()
                }
                .create()
        if (alertDialog != null) alertDialog!!.show()
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()
    }

    private fun startActivityFromGallery() {
        mImgConfig!!.isImgFromCamera = false
        val photoPickerIntent = Intent(Intent.ACTION_PICK)
        photoPickerIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, !mImgConfig!!.allowOnlineImages)
        photoPickerIntent.type = "image/*"
        startActivityForResult(photoPickerIntent, ImageTags.IntentCode.REQUEST_CODE_SELECT_PHOTO)
        if (mImgConfig!!.debug) Log.d(ImageTags.Tags.TAG, "Gallery Start with Single Image mode")
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    fun startActivityFromGalleryMultiImg() {
        mImgConfig!!.isImgFromCamera = false
        val photoPickerIntent = Intent()
        photoPickerIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, !mImgConfig!!.allowOnlineImages)
        photoPickerIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        photoPickerIntent.action = Intent.ACTION_GET_CONTENT
        photoPickerIntent.type = "image/*"
        startActivityForResult(Intent.createChooser(photoPickerIntent, "Select Picture"), ImageTags.IntentCode.REQUEST_CODE_SELECT_MULTI_PHOTO)
        if (mImgConfig!!.debug) Log.d(ImageTags.Tags.TAG, "Gallery Start with Multiple Images mode")
    }

    fun startActivityFromCamera() {
        mImgConfig!!.isImgFromCamera = true
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        mImageUri = FileProvider.getUriForFile(this, this.applicationContext.packageName + ".provider", destination!!)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri)
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), ImageTags.IntentCode.CAMERA_REQUEST)
        if (mImgConfig!!.debug) Log.d(ImageTags.Tags.TAG, "Camera Start")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (mImageUri != null) {
            outState.putString(ImageTags.Tags.CAMERA_IMAGE_URI, mImageUri.toString())
            outState.putSerializable(ImageTags.Tags.IMG_CONFIG, mImgConfig)
        }
        outState.putBoolean(ImageTags.Tags.IS_ALERT_SHOWING, if (alertDialog == null) false else alertDialog!!.isShowing)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        if (savedInstanceState.containsKey(ImageTags.Tags.CAMERA_IMAGE_URI)) {
            mImageUri = Uri.parse(savedInstanceState.getString(ImageTags.Tags.CAMERA_IMAGE_URI))
            destination = File(mImageUri?.path)
            mImgConfig = savedInstanceState.getSerializable(ImageTags.Tags.IMG_CONFIG) as ImageConfig?
        }
        if (savedInstanceState.getBoolean(ImageTags.Tags.IS_ALERT_SHOWING, false)) {
            if (alertDialog == null) pickImage() else alertDialog!!.show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (mImgConfig!!.debug) Log.d(ImageTags.Tags.TAG, "onActivityResult() called with: " + "requestCode = [" + requestCode + "]," +
                " resultCode = [" + resultCode + "], data = [" + data + "]")
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                ImageTags.IntentCode.CAMERA_REQUEST -> CompressImageTask(destination!!.absolutePath, mImgConfig
                        , this@ImageActivity).execute()
                ImageTags.IntentCode.REQUEST_CODE_SELECT_PHOTO -> processOneImage(data)
                ImageTags.IntentCode.REQUEST_CODE_SELECT_MULTI_PHOTO -> processMutliPhoto(data)
                else -> {
                }
            }
        } else {
            val intent = Intent()
            intent.action = "net.alhazmy13.mediapicker.rxjava.image.service"
            intent.putExtra(ImageTags.Tags.PICK_ERROR, "user did not select any image")
            sendBroadcast(intent)
            finish()
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private fun processMutliPhoto(data: Intent?) {
        //Check if the intent contain only one image
        if (data!!.clipData == null) {
            processOneImage(data)
        } else {
            //intent has multi images
            listOfImgs = ImageProcessing.processMultiImage(this, data)
            if (listOfImgs != null && listOfImgs?.size > 0) {
                CompressImageTask(listOfImgs, mImgConfig, this@ImageActivity).execute()
            } else {
                //For 'Select pic from Google Photos - app Crash' fix
                val check = data.clipData.toString()
                if (check != null && check.contains("com.google.android.apps.photos")) {
                    val clipdata = data.clipData
                    for (i in 0 until clipdata!!.itemCount) {
                        val selectedImage = clipdata.getItemAt(i).uri
                        val selectedImagePath = FileProcessing.getPath(this@ImageActivity, selectedImage)
                        listOfImgs.add(selectedImagePath)
                    }
                    CompressImageTask(listOfImgs, mImgConfig, this@ImageActivity).execute()
                }
            }
        }
    }

    fun processOneImage(data: Intent?) {
        try {
            val selectedImage = data!!.data
            if (selectedImage != null) {
                val rawPath = selectedImage.toString()
                //For 'Select pic from Google Drive - app Crash' fix
                if (rawPath.contains("com.google.android.apps.docs.storage")) {
                    val fileTempPath = cacheDir.path
                    SaveImageFromGoogleDriveTask(fileTempPath, mImgConfig, selectedImage, this@ImageActivity).execute()
                } else {
                    val selectedImagePath = FileProcessing.getPath(this, selectedImage)
                    CompressImageTask(selectedImagePath,
                            mImgConfig, this@ImageActivity).execute()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun finishActivity(path: List<String>) {
        val resultIntent = Intent()
        resultIntent.putExtra(ImagePicker.Companion.EXTRA_IMAGE_PATH, path as Serializable)
        setResult(Activity.RESULT_OK, resultIntent)
        finish()
    }

    private fun pickImageWrapper() {
        if (Build.VERSION.SDK_INT >= 23) {
            val permissionsNeeded: MutableList<String> = ArrayList()
            val permissionsList: MutableList<String> = ArrayList()
            if ((mImgConfig!!.mode == ImagePicker.Mode.CAMERA || mImgConfig!!.mode == ImagePicker.Mode.CAMERA_AND_GALLERY) && !addPermission(permissionsList, Manifest.permission.CAMERA)) permissionsNeeded.add(getString(R.string.media_picker_camera))
            if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE)) permissionsNeeded.add(getString(R.string.media_picker_gallery))
            if (permissionsList.size > 0) {
                if (permissionsNeeded.size > 0) {
                    // Need Rationale
                    //val message = StringBuilder(getString(R.string.media_picker_you_need_to_grant_access_to) + " " + permissionsNeeded[0])
                    //for (i in 1 until permissionsNeeded.size) message.append(", ").append(permissionsNeeded[i])
                    /*showMessageOKCancel(message.toString(),
                            DialogInterface.OnClickListener { dialog, which ->
                                when (which) {
                                    DialogInterface.BUTTON_POSITIVE -> ActivityCompat.requestPermissions(this@ImageActivity, permissionsList.toTypedArray(),
                                            ImageTags.IntentCode.REQUEST_CODE_ASK_PERMISSIONS)
                                    else -> onBackPressed()
                                }
                            })*/
                    ActivityCompat.requestPermissions(this@ImageActivity, permissionsList.toTypedArray(),
                            ImageTags.IntentCode.REQUEST_CODE_ASK_PERMISSIONS)
                    return
                }
                ActivityCompat.requestPermissions(this@ImageActivity, permissionsList.toTypedArray(),
                        ImageTags.IntentCode.REQUEST_CODE_ASK_PERMISSIONS)
                return
            }
            pickImage()
        } else {
            pickImage()
        }
    }

    private fun showMessageOKCancel(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton(getString(R.string.media_picker_ok), okListener)
                .setNegativeButton(getString(R.string.media_picker_cancel), okListener)
                .create()
                .show()
    }

    private fun addPermission(permissionsList: MutableList<String>, permission: String): Boolean {
        if (ActivityCompat.checkSelfPermission(this@ImageActivity, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission)
            // Check for Rationale Option
            return ActivityCompat.shouldShowRequestPermissionRationale(this@ImageActivity, permission)
        }
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            ImageTags.IntentCode.REQUEST_CODE_ASK_PERMISSIONS -> {
                val perms: MutableMap<String, Int> = HashMap()
                // Initial
                perms[Manifest.permission.CAMERA] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED
                // Fill with results
                var i = 0
                while (i < permissions.size) {
                    perms[permissions[i]] = grantResults[i]
                    i++
                }
                // Check for ACCESS_FINE_LOCATION
                if (perms[Manifest.permission.CAMERA] == PackageManager.PERMISSION_GRANTED
                        && perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted
                    pickImage()
                } else {
                    // Permission Denied
                    Toast.makeText(this@ImageActivity, getString(R.string.media_picker_some_permission_is_denied), Toast.LENGTH_SHORT)
                            .show()
                    onBackPressed()
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private class CompressImageTask : AsyncTask<Void, Void, Void> {
        private val mImgConfig: ImageConfig?
        private val listOfImgs: List<String?>?
        private var destinationPaths: MutableList<String>
        private var mContext: WeakReference<ImageActivity?>

        internal constructor(listOfImgs: List<String?>?, imageConfig: ImageConfig?, context: ImageActivity?) {
            this.listOfImgs = listOfImgs
            mContext = WeakReference(context)
            mImgConfig = imageConfig
            destinationPaths = ArrayList()
        }

        internal constructor(absolutePath: String?, imageConfig: ImageConfig?, context: ImageActivity?) {
            val list: MutableList<String?> = ArrayList()
            list.add(absolutePath)
            listOfImgs = list
            mContext = WeakReference(context)
            destinationPaths = ArrayList()
            mImgConfig = imageConfig
        }

        protected override fun doInBackground(vararg params: Void): Void? {
            for (mPath in listOfImgs!!) {
                val file = File(mPath)
                var destinationFile: File
                destinationFile = if (mImgConfig!!.isImgFromCamera) {
                    file
                } else {
                    File(mImgConfig.directory, Utility.getRandomString() + mImgConfig.extension.value)
                }
                destinationPaths.add(destinationFile.absolutePath)
                try {
                    Utility.compressAndRotateIfNeeded(file, destinationFile, mImgConfig.compressLevel.value, mImgConfig.reqWidth, mImgConfig.reqHeight)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
            return null
        }

        override fun onPostExecute(aVoid: Void?) {
            super.onPostExecute(aVoid)
            val context = mContext.get()
            if (context != null) {
                context.finishActivity(destinationPaths)
                val intent = Intent()
                intent.action = "net.alhazmy13.mediapicker.rxjava.image.service"
                intent.putExtra(ImageTags.Tags.IMAGE_PATH, destinationPaths as Serializable)
                context.sendBroadcast(intent)
            }
        }
    }

    private class SaveImageFromGoogleDriveTask internal constructor(absolutePath: String, imageConfig: ImageConfig?, uri: Uri, context: ImageActivity) : AsyncTask<Void, Void, Void>() {
        private val mImgConfig: ImageConfig
        private val listOfImgs: List<String>
        private val destinationPaths: MutableList<String?>
        private val destinationUris: List<Uri>
        private val mContext: WeakReference<ImageActivity>
        protected override fun doInBackground(vararg params: Void): Void? {
            for (i in listOfImgs.indices) {
                val path = listOfImgs[i]
                val uriPath = destinationUris[i]
                try {
                    val fileName = "drive_img_" + System.currentTimeMillis() + ".jpg"
                    val fullImagePath = "$path/$fileName"
                    val isFileSaved = saveFile(uriPath, fullImagePath)
                    if (isFileSaved) {
                        destinationPaths.add(fullImagePath)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            return null
        }

        override fun onPostExecute(aVoid: Void?) {
            super.onPostExecute(aVoid)
            CompressImageTask(destinationPaths, mImgConfig, mContext.get()).execute()
        }

        var filenotfoundexecption = false

        //For Google Drive
        @Throws(IOException::class)
        fun saveFile(sourceuri: Uri?, destination: String?): Boolean {
            filenotfoundexecption = false
            val originalsize: Int
            var input: InputStream? = null
            try {
                input = mContext.get()!!.contentResolver.openInputStream(sourceuri!!)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
                filenotfoundexecption = true
            }
            try {
                originalsize = input!!.available()
                val bis: BufferedInputStream
                val bos: BufferedOutputStream
                try {
                    bis = BufferedInputStream(input)
                    bos = BufferedOutputStream(FileOutputStream(destination, false))
                    val buf = ByteArray(originalsize)
                    bis.read(buf)
                    do {
                        bos.write(buf)
                    } while (bis.read(buf) != -1)
                } catch (e: IOException) {
                    filenotfoundexecption = true
                    return false
                }
            } catch (e: NullPointerException) {
                filenotfoundexecption = true
            }
            return true
        }

        init {
            val list: MutableList<String> = ArrayList()
            list.add(absolutePath)
            listOfImgs = list
            val uris: MutableList<Uri> = ArrayList()
            uris.add(uri)
            destinationUris = uris
            mContext = WeakReference(context)
            destinationPaths = ArrayList()
            mImgConfig = imageConfig!!
        }
    }

    companion object {
        fun getCallingIntent(activity: Context?, imageConfig: ImageConfig?): Intent {
            val intent = Intent(activity, ImageActivity::class.java)
            intent.putExtra(ImageTags.Tags.IMG_CONFIG, imageConfig)
            return intent
        }
    }


}

class OptionFragment(var activity: ImageActivity) : BottomSheetDialogFragment() {
    var bottomSheetDialog: BottomSheetDialog? = null
    lateinit var bottomSheet: FrameLayout

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        isCancelable = false
        bottomSheetDialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog

        bottomSheetDialog!!.setOnShowListener { mDialog: DialogInterface ->
            val dialog = mDialog as BottomSheetDialog
            bottomSheet = dialog.findViewById(R.id.design_bottom_sheet)!!
            BottomSheetBehavior.from<FrameLayout>(bottomSheet).state =
                    BottomSheetBehavior.STATE_EXPANDED
            BottomSheetBehavior.from<FrameLayout>(bottomSheet).skipCollapsed = true
            BottomSheetBehavior.from<FrameLayout>(bottomSheet).isHideable = true
            val behavior: BottomSheetBehavior<*> =
                    BottomSheetBehavior.from<FrameLayout>(bottomSheet)
        }
        bottomSheetDialog!!.setOnDismissListener { dialog: DialogInterface? -> }
        return bottomSheetDialog!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(
                DialogFragment.STYLE_NORMAL,
                R.style.CustomBottomSheetDialogTheme
        )
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_image_picker, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_gallery.setOnClickListener {
            activity.startActivityFromGalleryMultiImg()
            dismiss()
        }
        btn_camera.setOnClickListener {
            activity.startActivityFromCamera()
            dismiss()
        }
        btn_cancel.setOnClickListener {
            dismiss()
            Handler().postDelayed({ activity.finish() }, 150)
        }
    }
    /*override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        activity.finish()
    }*/
}