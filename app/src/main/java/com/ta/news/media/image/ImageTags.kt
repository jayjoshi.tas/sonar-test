package com.ta.news.media.image

class ImageTags {
    object Tags {
        const val TAG = "ImagePicker"
        const val CAMERA_IMAGE_URI = "cameraImageUri"
        const val IMAGE_PATH = "IMAGE_PATH"
        const val IMAGE_PICKER_DIR = "/mediapicker/images/"
        const val IMG_CONFIG = "IMG_CONFIG"
        const val PICK_ERROR = "PICK_ERROR"
        const val IS_ALERT_SHOWING = "IS_ALERT_SHOWING"
    }

    object Action {
        const val SERVICE_ACTION = "net.alhazmy13.mediapicker.rxjava.image.service"
    }

    object IntentCode {
        const val REQUEST_CODE_SELECT_MULTI_PHOTO = 5341
        const val CAMERA_REQUEST = 1888
        const val REQUEST_CODE_ASK_PERMISSIONS = 123
        const val REQUEST_CODE_SELECT_PHOTO = 43
    }
}