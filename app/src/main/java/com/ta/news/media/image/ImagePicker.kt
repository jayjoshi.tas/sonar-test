package com.ta.news.media.image

import android.annotation.TargetApi
import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Environment
import java.lang.ref.WeakReference

/**
 * Created by Alhazmy13 on 10/26/15.
 * MediaPicker
 */
class ImagePicker internal constructor(builder: Builder) {
    /**
     * The type Builder.
     */
    class Builder(context: Activity) : ImagePickerBuilderBase {
        // Required params
        val context: WeakReference<Activity>
        val imageConfig: ImageConfig
        override fun compressLevel(compressLevel: ComperesLevel?): Builder {
            imageConfig.compressLevel = compressLevel!!
            return this
        }

        override fun mode(mode: Mode?): Builder {
            imageConfig.mode = mode!!
            return this
        }

        override fun directory(directory: String?): Builder {
            imageConfig.directory = directory!!
            return this
        }

        override fun directory(directory: Directory?): Builder {
            when (directory) {
                Directory.DEFAULT -> imageConfig.directory = Environment.getExternalStorageDirectory().toString() + ImageTags.Tags.IMAGE_PICKER_DIR
                else -> {
                }
            }
            return this
        }

        override fun extension(extension: Extension?): Builder {
            imageConfig.extension = extension!!
            return this
        }

        override fun scale(minWidth: Int, minHeight: Int): Builder {
            imageConfig.reqHeight = minHeight
            imageConfig.reqWidth = minWidth
            return this
        }

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
        override fun allowMultipleImages(allowMultiple: Boolean): Builder {
            imageConfig.allowMultiple = allowMultiple
            return this
        }

        override fun enableDebuggingMode(debug: Boolean): Builder {
            imageConfig.debug = debug
            return this
        }

        override fun allowOnlineImages(allowOnlineImages: Boolean): Builder {
            imageConfig.allowOnlineImages = allowOnlineImages
            return this
        }

        override fun build(): ImagePicker {
            return ImagePicker(this)
        }

        /**
         * Gets context.
         *
         * @return the context
         */
        fun getContext(): Activity? {
            return context.get()
        }

        /**
         * Instantiates a new Builder.
         *
         * @param context the context
         */
        init {
            this.context = WeakReference(context)
            imageConfig = ImageConfig()
        }
    }

    /**
     * The enum Extension.
     */
    enum class Extension(val value: String) {
        /**
         * Png extension.
         */
        PNG(".png"),

        /**
         * Jpg extension.
         */
        JPG(".jpg");

        /**
         * Gets value.
         *
         * @return the value
         */

    }

    /**
     * The enum Comperes level.
     */
    enum class ComperesLevel(val value: Int) {
        /**
         * Hard comperes level.
         */
        HARD(20),

        /**
         * Medium comperes level.
         */
        MEDIUM(50),

        /**
         * Soft comperes level.
         */
        SOFT(80),

        /**
         * None comperes level.
         */
        NONE(100);

        /**
         * Gets value.
         *
         * @return the value
         */

    }

    /**
     * The enum Mode.
     */
    enum class Mode(val value: Int) {
        /**
         * Camera mode.
         */
        CAMERA(0),

        /**
         * Gallery mode.
         */
        GALLERY(1),

        /**
         * Camera and gallery mode.
         */
        CAMERA_AND_GALLERY(2);

        /**
         * Gets value.
         *
         * @return the value
         */

    }

    /**
     * The enum Directory.
     */
    enum class Directory(val value: Int) {
        /**
         * Default directory.
         */
        DEFAULT(0);

        /**
         * Gets value.
         *
         * @return the value
         */

    }

    companion object {
        /**
         * The constant IMAGE_PICKER_REQUEST_CODE.
         */
        const val IMAGE_PICKER_REQUEST_CODE = 42141

        /**
         * The constant EXTRA_IMAGE_PATH.
         */
        const val EXTRA_IMAGE_PATH = "EXTRA_IMAGE_PATH"
    }

    /**
     * Instantiates a new Image picker.
     *
     * @param builder the builder
     */
    init {

        // Required
        val context = builder.context

        // Optional
        val imageConfig = builder.imageConfig
        val callingIntent: Intent = ImageActivity.Companion.getCallingIntent(context.get(), imageConfig)
        context.get()!!.startActivityForResult(callingIntent, IMAGE_PICKER_REQUEST_CODE)
    }
}