package com.ta.news.utils

/**
 * Created by arthtilva on 22-Feb-17.
 */
interface OnLoadMoreListener {
    fun onLoadMore()
}