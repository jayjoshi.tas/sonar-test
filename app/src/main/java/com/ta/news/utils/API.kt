package com.ta.news.utils

import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by arthtilva on 07-Nov-16.
 */


interface API {

    companion object {
        const val version = "v3"
    }

    @FormUrlEncoded
    @POST("$version/category/getCategory")
    fun newsCategory(
            @Field("cmd") cmd: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/contentPost/getPost")
    fun getPost(
            @Field("parentId") parentId: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/order/addOrder")
    fun addOrder(
            @Field("userId") userId: String,
            @Field("planId") planId: Int,
            @Field("price") price: String,
            @Field("totalPost") totalPost: String,
            @Field("transactionId") transactionId: String,
            @Field("paymentBy") paymentBy: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/plans/setFeaturedPost")
    fun setFeaturedPost(
            @Field("userId") userId: String,
            @Field("planId") planId: Int,
            @Field("price") price: String,
            @Field("postId") postId: Int,
            @Field("categoryId") categoryId: String,
            @Field("planDay") planDay: String,
            @Field("transactionId") transactionId: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/news/getNews")
    fun news(
            @Field("categoryId") categoryId: Int,
            @Field("page") page: Int,
            @Field("search") search: String,
            @Field("districtId") districtId: String,
            @Field("deviceType") deviceType: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("$version/auth/addUpdateYourBusiness")
    fun addUpdatebusiness(
            @Field("userId") userId: String,
            @Field("name") name: String,
            @Field("address") address: String,
            @Field("contactNo") contactNo: String,
            @Field("contactPerson") contactPerson: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/news/getNews")
    fun news(
            @Field("postId") postId: Int,
            @Field("deviceType") deviceType: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/book/getBook")
    fun books(
            @Field("page") page: Int,
            @Field("search") search: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/news/newsHistory")
    fun newsHistory(
            @Field("userId") userId: String,
            @Field("page") page: Int
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST
    fun getRemoteData(
            @Url url: String,
            @Field("secure") secure: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/auth/loginRegister")
    fun register(
            @Field("firstName") firstName: String,
            @Field("lastName") lastName: String,
            @Field("email") email: String,
            @Field("countryCode") countryCode: String,
            @Field("mobileNo") mobileNo: String,
            @Field("address") address: String,
            @Field("city") city: String,
            @Field("taluka") taluka: String,
            @Field("pincode") pinCode: String,
            @Field("deviceToken") deviceToken: String,
            @Field("deviceType") deviceType: String,
            @Field("duplicate") duplicate: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/auth/updateProfile")
    fun updateProfile(
            @Field("firstName") firstName: String,
            @Field("lastName") lastName: String,
            @Field("email") email: String,
            @Field("address") address: String,
            @Field("city") city: String,
            @Field("taluka") taluka: String,
            @Field("pincode") pinCode: String,
            @Field("userId") userId: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/news/addNews")
    fun addNews(
            @Field("userId") userId: String,
            @Field("categoryId") categoryId: Int,
            @Field("newsTitle") newsTitle: String,
            @Field("newsDetails") newsDetails: String,
            @Field("address") address: String,
            @Field("approxPrice") approxPrice: String,
            @Field("districtId") districtId: Int,
            @Field("version") version: Int
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/contactRequest/addContactRequest")
    fun contactRequest(
            @Field("userId") userId: String,
            @Field("message") message: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/page/getAds")
    fun getAds(
            @Field("deviceType") deviceType: String
    ): Call<ResponseBody>

    @GET("$version/faq/getFaq")
    fun getFaq(): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/auth/profileById")
    fun profileById(
            @Field("userId") userId: Int
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/auth/profileByMobile")
    fun getProfileByMobile(
            @Field("countryCode") countryCode: String,
            @Field("mobileNo") mobileNo: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/general/splashScreen")
    fun splashScreen(
            @Field("userId") userId: String,
            @Field("androidVersionCode") androidVersionCode: Int
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/news/getUserProfileNews")
    fun getUserNews(
            @Field("userId") userId: Int,
            @Field("page") page: Int
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/general/splashScreen")
    fun splashScreen(
            @Field("userId") userId: String,
            @Field("androidVersionCode") androidVersionCode: Int,
            @Field("error") error: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/news/newsDelete")
    fun deleteNews(
            @Field("userId") userId: String,
            @Field("postId") postId: Int
    ): Call<ResponseBody>

    @POST("$version/commodity/getCommodityName")
    fun commodityName(): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/commodity/getCommodityPrice")
    fun commodityPrice(
            @Field("commodityId") commodityId: Int
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/page/getPage")
    fun getPage(
            @Field("pageName") pageName: String
    ): Call<ResponseBody>

    @POST
    fun getVideos(
            @Url url: String
    ): Call<ResponseBody>

    @Multipart
    @POST("$version/auth/profileImageUpload")
    fun profileImageUpload(
            @Part image: MultipartBody.Part,
            @Part("userId") userId: RequestBody
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/comment/addComment")
    fun addComment(
            @Field("userId") userId: String,
            @Field("postId") postId: Int,
            @Field("newsUserId") newsUserId: Int,
            @Field("comment") comment: String,
            @Field("commentUserName") commentUserName: String,
            @Field("notification") notification: Int
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/comment/getComment")
    fun getComments(
            @Field("postId") postId: Int,
            @Field("page") page: Int
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/comment/commentDelete")
    fun commentDelete(
            @Field("commentId") commentId: Int,
            @Field("postId") postId: Int
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/comment/commentStatus")
    fun changeStatus(
            @Field("postId") postId: Int,
            @Field("notificationStatus") status: Int
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/auth/sendOtp")
    fun sendOtp(
            @Field("countryCode") countryCode: String,
            @Field("mobileNo") mobileNo: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/auth/verifyOtp")
    fun verifyOtp(
            @Field("otp") otp: String,
            @Field("sessionId") sessionId: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/listing/addSpamRequest")
    fun postSpam(
            @Field("userId") userId: String,
            @Field("postId") postId: Int,
            @Field("categoryId") spamId: Int,
            @Field("message") message: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/news/addNewsUserAllowed")
    fun addNewsUserAllowed(
            @Field("userId") userId: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("$version/product/getCategoryWiseProduct")
    fun getCategoryWiseProduct(
            @Field("categoryId") categoryId: Int,
            @Field("page") page: Int,
    ): Call<ResponseBody>


    @GET("$version/plans/getPlanType")
    fun getPlanType(): Call<ResponseBody>

    @GET("$version/product/homeScreen")
    fun homeScreen(): Call<ResponseBody>

    @GET("$version/plans/getFeaturePlans")
    fun getFeaturePlans(): Call<ResponseBody>

    @GET("$version/plans/getPlans")
    fun getPlans(): Call<ResponseBody>

    @GET("$version/district/getDistrict")
    fun getDistrict(): Call<ResponseBody>

    @GET("$version/listing/getSpamCategory")
    fun getSpamCategory(): Call<ResponseBody>
}