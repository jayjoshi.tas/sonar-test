package com.ta.news.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import com.ta.news.R
import com.ta.news.adapter.AdsAdapter
import com.ta.news.pojo.News
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.fragment_whats_app_connect.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.io.Reader
import java.io.StringReader
import java.lang.reflect.Modifier

/**
 * Created by arthtilva on 06-Nov-16.
 */
class WhatsAppFragment : BaseFragment() {

    var itemClickListener: ItemClickListener = object : ItemClickListener {
        override fun onClick(view: View?, position: Int) {}
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_whats_app_connect, container, false)
        mActivity = activity!!
        storeUserData = StoreUserData(mActivity)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initProgress()

        buttonStartVerification.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://api.whatsapp.com/send?phone=" + countryCode.selectedCountryCode + fieldPhoneNumber.text.toString()))
            startActivity(browserIntent)
        }
        ads
        loadBannerAds(banner_container, google_ad_view, R.string.fb_whats_app_banner)
    }

    val ads: Unit
        get() {
            Log.i("TAG", "getAds: called")
            val result = Utils.CallApi(mActivity).getAds("android")
            result.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    try {
                        Log.i("TAG", "onResponse: " + response.code())
                        if (response.code() != 200) {
                            serverAlert(mActivity)
                            return
                        }
                        val res = response.body()!!.string()
                        Log.i("response", "onResponse: $res")
                        val reader: Reader = StringReader(res)
                        val gson = GsonBuilder()
                                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                                .serializeNulls()
                                .create()
                        val ads = gson.fromJson(reader, News::class.java)
                        if (ads.advertisement != null && ads.advertisement!!.size > 0) {
                            recyclerView.isNestedScrollingEnabled = false
                            recyclerView.adapter = AdsAdapter(mActivity, ads.advertisement!!, itemClickListener)
                        }
                    } catch (e: IOException) {
                        e.printStackTrace()
                        serverAlert(mActivity)
                    } catch (e: IllegalStateException) {
                        e.printStackTrace()
                        serverAlert(mActivity)
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        serverAlert(mActivity)
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    t.printStackTrace()
                }
            })
        }

    interface ItemClickListener {
        fun onClick(view: View?, position: Int)
    }
}