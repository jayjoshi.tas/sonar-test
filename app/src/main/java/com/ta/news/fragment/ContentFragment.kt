package com.ta.news.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import com.ta.news.R
import com.ta.news.adapter.ContentCategoryAdapter
import com.ta.news.pojo.Content
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.fragment_content.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.io.Reader
import java.io.StringReader
import java.lang.reflect.Modifier
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by arthtilva on 06-Nov-16.
 */
class ContentFragment : BaseFragment() {


    var dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_content, container, false)
        mActivity = activity!!
        storeUserData = StoreUserData(mActivity)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initProgress()
        rvCategory.setHasFixedSize(true)
        rvCategory.layoutManager = LinearLayoutManager(mActivity)
        swipeRefreshCategory.setOnRefreshListener {
            if (Utils.isOnline(mActivity)) {
                getCategoryFromServer()
            } else {
                swipeRefreshCategory.visibility = View.GONE
                ivNoInternet.visibility = View.VISIBLE
                swipeRefreshCategory.isRefreshing = false
            }
        }
        getCategoryFromServer()
    }

    private fun getCategoryFromServer() {
        if (swipeRefreshCategory != null)
            swipeRefreshCategory.isRefreshing = true
        val result = Utils.CallApi(mActivity).getPost("")
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (swipeRefreshCategory != null)
                    swipeRefreshCategory.isRefreshing = false
                if (response.code() != 200) {
                    serverAlert(mActivity)
                    return
                }
                try {
                    val res = response.body()!!.string()
                    Log.i("response", "onResponse: $res")
                    val reader: Reader = StringReader(res)
                    val gson = GsonBuilder()
                            .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                            .serializeNulls()
                            .create()
                    storeUserData.setString(Constants.CONTENT_CATEGORY, res)
                    storeUserData.setString(Constants.CONTENT_CAT_SYNC_DATE, dateFormat.format(Date()))
                    val postCategory = gson.fromJson(reader, Content::class.java)
                    if (postCategory != null) {
                        if (postCategory.success && postCategory.data.size > 0) {
                            rvCategory.adapter = ContentCategoryAdapter(mActivity, postCategory.data)
                        }
                        else {
                            showAlert(mActivity, postCategory.message)
                        }
                    }

                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                } catch (e: JsonSyntaxException) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                if (swipeRefreshCategory != null)
                    swipeRefreshCategory.isRefreshing = false
            }
        })
    }

}