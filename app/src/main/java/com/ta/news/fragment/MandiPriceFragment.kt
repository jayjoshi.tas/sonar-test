package com.ta.news.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import com.ta.news.R
import com.ta.news.adapter.CommodityAdapter
import com.ta.news.pojo.Commodity
import com.ta.news.pojo.CommodityPojo
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.fragment_mandi_price.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.io.Reader
import java.io.StringReader
import java.lang.reflect.Modifier
import java.util.*

/**
 * Created by arthtilva on 06-Nov-16.
 */
class MandiPriceFragment : BaseFragment() {

    var commodity: Commodity? = null
    var lstCommodity: ArrayList<CommodityPojo> = ArrayList()
    lateinit var adapter: CommodityAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_mandi_price, container, false)
        mActivity = activity!!
        storeUserData = StoreUserData(mActivity)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initProgress()
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(mActivity)
        textView.isSelected = true
        textView.ellipsize = TextUtils.TruncateAt.MARQUEE
        edSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (commodity == null) {
                    return
                }
                lstCommodity.clear()
                if (s.isNotEmpty()) {
                    for (c in commodity!!.data) {
                        if (c.variety.toLowerCase().contains(s.toString().toLowerCase()) ||
                                c.commodity.toLowerCase().contains(s.toString().toLowerCase()) ||
                                c.nameGu.toLowerCase().contains(s.toString().toLowerCase())) {
                            lstCommodity.add(c)
                        }
                    }
                    if (s.toString().isNotEmpty()) {
                        btnCancel.visibility = View.VISIBLE
                    } else {
                        btnCancel.visibility = View.GONE
                    }
                } else {
                    lstCommodity.addAll(commodity!!.data)
                }
                if (adapter != null) adapter.notifyDataSetChanged()
            }

            override fun afterTextChanged(s: Editable) {}
        })
        btnCancel.setOnClickListener {
            edSearch.setText("")
            Utils.hideKB(mActivity, edSearch)
        }
        getCommodity()


        loadBannerAds(banner_container, googleAdView, R.string.fb_whats_app_banner)
    }

    fun getCommodity() {
        progressDialog.show()
        val result = Utils.CallApi(mActivity).commodityName()
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (mActivity != null) if (progressDialog.isShowing) progressDialog.dismiss()
                try {
                    if (response.code() != 200) {
                        serverAlert(mActivity)
                        return
                    }
                    val res = response.body()!!.string()
                    Log.i("response", "onResponse: $res")
                    val reader: Reader = StringReader(res)
                    val gson = GsonBuilder()
                            .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                            .create()
                    commodity = gson.fromJson(reader, Commodity::class.java)
                    //Constants.COMMODITY_BASE_URL = commodity!!.base_url
                    lstCommodity.clear()
                    lstCommodity.addAll(commodity!!.data)
                    adapter = CommodityAdapter(mActivity, lstCommodity)
                    recyclerView.adapter = adapter
                } catch (e: IOException) {
                    e.printStackTrace()
                    serverAlert(mActivity)
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                    serverAlert(mActivity)
                } catch (e: JsonSyntaxException) {
                    e.printStackTrace()
                    serverAlert(mActivity)
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                if (progressDialog.isShowing) progressDialog.dismiss()
                t.printStackTrace()
            }
        })
    }
}