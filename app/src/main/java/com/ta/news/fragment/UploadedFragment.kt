package com.ta.news.fragment

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import com.ta.news.R
import com.ta.news.adapter.UploadedAdapter
import com.ta.news.pojo.News
import com.ta.news.utils.Constants
import com.ta.news.utils.OnLoadMoreListener
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.activity_news_detail.*
import kotlinx.android.synthetic.main.fragment_uploaded.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.io.Reader
import java.io.StringReader
import java.lang.reflect.Modifier

/**
 * Created by arthtilva on 06-Nov-16.
 */
class UploadedFragment : BaseFragment() {

    var page = 1
    lateinit var layoutManager: LinearLayoutManager
    var totalPages = 1
    lateinit var adapter: UploadedAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_uploaded, container, false)
        mActivity = activity!!
        storeUserData = StoreUserData(mActivity)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initProgress()
        layoutManager = LinearLayoutManager(mActivity)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = layoutManager
        adapter = UploadedAdapter(mActivity, recyclerView, Constants.lstNews, onDeleteListener)
        recyclerView.adapter = adapter
        swipeRefresh.setOnRefreshListener {
            page = 1
            data
        }
        adapter.setOnLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                if (page < totalPages) {
                    page += 1
                    data
                }
            }
        })
    }

    var onDeleteListener = object : OnDeleteListener {
        override fun onDelete(postId: Int) {
            deleteNews(postId)
        }
    }

    interface OnDeleteListener {
        fun onDelete(postId: Int)
    }

    override fun onResume() {
        super.onResume()
        data
    }

    val data: Unit
        get() {
            if (Utils.isOnline(mActivity)) {
                userNews
            } else {
                swipeRefresh.visibility = View.GONE
                ivNoInternet.visibility = View.VISIBLE
            }
        }

    val userNews: Unit
        get() {
            if (page == 1 && swipeRefresh != null) swipeRefresh.isRefreshing = true
            val result = Utils.CallApi(mActivity).newsHistory(storeUserData.getString(Constants.USER_ID), page)
            result.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    if (swipeRefresh != null)
                        swipeRefresh.isRefreshing = false
                    try {
                        if (response.code() != 200) {
                            serverAlert(mActivity)
                            return
                        }
                        val res = response.body()!!.string()
                        Log.i("response", "onResponse: $res")
                        val reader: Reader = StringReader(res)
                        val gson = GsonBuilder()
                                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                                .serializeNulls()
                                .create()
                        val news = gson.fromJson(reader, News::class.java)
                        totalPages = news.totalPages
                        if (page == 1) {
                            Constants.lstNews.clear()
                        } else {
                            //hide progress
                        }
                        if (news.data != null)
                            Constants.lstNews.addAll(news.data!!)
                        Log.i("TAG", "onResponse: " + "page = " + page + "-\n total - " + totalPages + "\n" + Constants.lstNews.size)
                        if (page == 1 && Constants.lstNews.isEmpty()) {
                            swipeRefresh.visibility = View.GONE
                            ivNoInternet.visibility = View.VISIBLE
                            tvMessage.setText(R.string.not_posted)
                            ivWarning.setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.empty_icon))
                        }
                        adapter.notifyDataSetChanged()
                        adapter.setLoaded()
                    } catch (e: IOException) {
                        e.printStackTrace()
                        serverAlert(mActivity)
                    } catch (e: IllegalStateException) {
                        e.printStackTrace()
                        serverAlert(mActivity)
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        serverAlert(mActivity)
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    t.printStackTrace()
                    if (swipeRefresh != null)
                        swipeRefresh.isRefreshing = false
                }
            })
        }


    fun deleteNews(postId: Int) {
        progressDialog.show()
        val result = Utils.CallApi(mActivity).deleteNews(
                storeUserData.getString(Constants.USER_ID),
                postId
        )
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (progressDialog.isShowing) progressDialog.dismiss()

                if (response.code() != 200) {
                    serverAlert(activity)
                    return
                }
                val responseData = response.body()!!.string()
                Log.i("TAG", "onResponse: $responseData")
                if (!TextUtils.isEmpty(responseData)) {
                    val jsonObject = JSONObject(responseData)
                    if (jsonObject.getBoolean("success")) {
                        page = 1
                        data
                    } else {
                        Snackbar.make(btnDeleteFromServer, jsonObject.getString("message"), Snackbar.LENGTH_LONG).show()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                if (progressDialog.isShowing) progressDialog.dismiss()
                t.printStackTrace()
            }
        })
    }
}