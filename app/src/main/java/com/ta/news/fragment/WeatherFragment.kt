package com.ta.news.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.ta.news.R
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.fragment_weather.*

/**
 * Created by arthtilva on 06-Nov-16.
 */
class WeatherFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_weather, container, false)
        mActivity = activity!!
        storeUserData = StoreUserData(mActivity)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initProgress()
        /*webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                if (progressDialog.isShowing) progressDialog.dismiss()
            }
        }
        webView.settings.setSupportZoom(true)
        webView.settings.javaScriptEnabled = true
        webView.settings.builtInZoomControls = true
        webView.settings.displayZoomControls = true
        webView.setInitialScale(1)
        webView.settings.loadWithOverviewMode = true
        webView.settings.useWideViewPort = true
        if (Utils.isOnline(mActivity)) {
            progressDialog.show()
            //webView.loadUrl("http://satellite.imd.gov.in/img/animation3d/3Dasiasec_ir1_3d.htm?animPace=5")
            webView.loadUrl("https://www.windy.com/-Rain-thunder-rain?rain,23.373,72.751,6")
        } else {
            webView.visibility = View.GONE
            ivNoInternet.visibility = View.VISIBLE
        }*/

        Glide.with(mActivity).load("https://mausam.imd.gov.in/Satellite/3Dasiasec_ir1.jpg?time=${System.currentTimeMillis()}").into(image)
    }
}