package com.ta.news.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.activeandroid.Model
import com.activeandroid.query.Delete
import com.activeandroid.query.Select
import com.activeandroid.query.Update
import com.ta.news.R
import com.ta.news.adapter.NotificationAdapter
import com.ta.news.controls.CustomDialog
import com.ta.news.pojo.Notification
import com.ta.news.utils.StoreUserData
import kotlinx.android.synthetic.main.fragment_notification.*

/**
 * Created by arthtilva on 06-Nov-16.
 */
class NotificationFragment : BaseFragment() {
    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapter: NotificationAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_notification, container, false)
        mActivity = activity!!
        storeUserData = StoreUserData(mActivity)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initProgress()
        layoutManager = LinearLayoutManager(mActivity)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = layoutManager
        swipeRefreshNoti.setOnRefreshListener {
            getData()
            swipeRefreshNoti.isRefreshing = false
        }
        btnDeleteAll.setOnClickListener {
            val dialog = CustomDialog(mActivity)
            dialog.show()
            dialog.setMessage(R.string.notification_delete_confirmation)
            dialog.setPositiveButton(R.string.yes, View.OnClickListener {
                dialog.dismiss()
                Delete().from(Notification::class.java).execute<Model>()
                getData()
            })
            dialog.setNegativeButton(R.string.no, View.OnClickListener { dialog.dismiss() })
        }
    }

    override fun onResume() {
        super.onResume()
        getData()
    }

    fun getData() {
        val list = Select().from(Notification::class.java).orderBy("id DESC").execute<Notification>()
        adapter = NotificationAdapter(mActivity, list)
        recyclerView.adapter = adapter
        swipeRefreshNoti.isRefreshing = false
        Update(Notification::class.java).set("isRead = 1").where("isRead = 0").execute()
        if (list.isEmpty()) {
            swipeRefreshNoti.visibility = View.GONE
            ivNoInternet.visibility = View.VISIBLE
            tvMessage.setText(R.string.no_notification)
            ivWarning.setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.empty_icon))
        }
    }
}