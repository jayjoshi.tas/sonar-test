package com.ta.news.fragment

import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.ta.news.R
import com.ta.news.pojo.News
import kotlinx.android.synthetic.main.dialog_call_advertisement.*

class CallAdvOptionsFragment(var ad: News.Ads) : BottomSheetDialogFragment() {
    var bottomSheetDialog: BottomSheetDialog? = null
    lateinit var bottomSheet: FrameLayout

    init {
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        bottomSheetDialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog

        bottomSheetDialog!!.setOnShowListener { mDialog: DialogInterface ->
            val dialog = mDialog as BottomSheetDialog
            bottomSheet = dialog.findViewById(R.id.design_bottom_sheet)!!
            BottomSheetBehavior.from<FrameLayout>(bottomSheet).state =
                    BottomSheetBehavior.STATE_EXPANDED
            BottomSheetBehavior.from<FrameLayout>(bottomSheet).skipCollapsed = true
            BottomSheetBehavior.from<FrameLayout>(bottomSheet).isHideable = true
            val behavior: BottomSheetBehavior<*> =
                    BottomSheetBehavior.from<FrameLayout>(bottomSheet)
        }
        bottomSheetDialog!!.setOnDismissListener { dialog: DialogInterface? -> }
        return bottomSheetDialog!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(
                DialogFragment.STYLE_NORMAL,
                R.style.CustomBottomSheetDialogTheme
        )
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_call_advertisement, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvTitle.text = ad.adName + " સાથે જોડાવા માટે..."
        btn_cancel.setOnClickListener { dismiss() }

        if (ad.androidUrl.isNullOrEmpty()) {
            btnWebsite.isVisible = false
        } else {
            btnWebsite.setOnClickListener {
                try {
                    startActivity(
                            Intent(Intent.ACTION_VIEW, Uri.parse(ad.androidUrl))
                    )
                } catch (e: Exception) {
                    Toast.makeText(activity, "વોટ્સએપ ફોનમાં ઇન્સ્ટોલ કરેલું નથી.", Toast.LENGTH_SHORT).show()
                }
            }
        }
        if (ad.whatsappNo.isNullOrEmpty()) {
            btnWhatsApp.isVisible = false
        } else {
            btnWhatsApp.setOnClickListener {
                try {
                    startActivity(
                            Intent(Intent.ACTION_VIEW, Uri.parse("http://api.whatsapp.com/send?phone=" + ad.whatsappNo.replace("+", ""))).setPackage("com.whatsapp")
                    )
                } catch (e: Exception) {
                    Toast.makeText(activity, "વોટ્સએપ ફોનમાં ઇન્સ્ટોલ કરેલું નથી.", Toast.LENGTH_SHORT).show()
                }
            }
        }
        if (ad.mobileNo.isNullOrEmpty()) {
            btnCall.isVisible = false
        } else {
            btnCall.setOnClickListener {
                try {
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + ad.mobileNo))
                    startActivity(browserIntent)
                } catch (e: Exception) {
                    Toast.makeText(activity, "કંઈક ખોટું થયું. પછીથી ફરી પ્રયત્ન કરો.", Toast.LENGTH_SHORT).show()
                }
            }
        }

    }
    /*override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        activity.finish()
    }*/
}