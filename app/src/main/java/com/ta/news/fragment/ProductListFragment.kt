package com.ta.news.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import com.ta.news.R
import com.ta.news.adapter.HomeSliderPagerAdapter
import com.ta.news.adapter.ProductListAdapter
import com.ta.news.pojo.Product
import com.ta.news.pojo.ProductCategory
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.activity_product_details.*
import kotlinx.android.synthetic.main.fragment_product_list.*
import kotlinx.android.synthetic.main.fragment_product_list.indicator
import kotlinx.android.synthetic.main.fragment_product_list.viewPager
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.io.Reader
import java.io.StringReader
import java.lang.reflect.Modifier
import java.util.*


class ProductListFragment : BaseFragment() {
    var data = ArrayList<ProductCategory>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_product_list, container, false)
        mActivity = activity!!
        storeUserData = StoreUserData(mActivity)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initProgress()
        homeScreen()
    }

    private fun homeScreen() {
        progressDialog.show()
        val result = Utils.CallApi(mActivity).homeScreen()
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (progressDialog.isShowing) progressDialog.dismiss()
                if (response.code() != 200) {
                    serverAlert(mActivity)
                    return
                }
                try {
                    val res = response.body()!!.string()
                    Log.i("response", "onResponse: $res")
                    val reader: Reader = StringReader(res)
                    val gson = GsonBuilder()
                            .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                            .serializeNulls()
                            .create()
                    val product = gson.fromJson(reader, Product::class.java)

                    for (i in product.data.indices) {
                        if (product.data[i].productArray.size > 0) {
                            data.add(product.data[i])
                        }
                    }
                    rvProductList.adapter = ProductListAdapter(mActivity, data)
                    viewPager.clipToPadding = false;
                    viewPager.setPadding(40, 0, 40, 0)
                    viewPager.adapter = HomeSliderPagerAdapter(mActivity, product.slider)
                    indicator.setViewPager(viewPager)
                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                } catch (e: JsonSyntaxException) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                if (progressDialog.isShowing) progressDialog.dismiss()

                t.printStackTrace()
            }
        })
    }

}