package com.ta.news.fragment

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import com.ta.news.R
import com.ta.news.adapter.BooksAdapter
import com.ta.news.activity.post.NewPostAddActivity
import com.ta.news.pojo.Books
import com.ta.news.pojo.BooksPojo
import com.ta.news.utils.Constants
import com.ta.news.utils.OnLoadMoreListener
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.fragment_books.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.io.Reader
import java.io.StringReader
import java.lang.reflect.Modifier

/**
 * Created by arthtilva on 06-Nov-16.
 */
class BooksFragment : BaseFragment() {

    var page = 1 /*, categoryId*/
    var totalPages = 1
    lateinit var adapter: BooksAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_books, container, false)
        mActivity = activity!!
        storeUserData = StoreUserData(mActivity)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initProgress()
        adapter = BooksAdapter(mActivity, recyclerView, Constants.lstBooks)
        recyclerView.adapter = adapter
        //categoryId = getArguments().getInt("categoryId");
        data
        Log.i("TAG", "onCreateView: 1")
        /*swipeRefreshBook.setOnRefreshListener {
            page = 1
            data
            Log.i("TAG", "onCreateView: 2")
        }*/

        adapter.setOnLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                if (page < totalPages) {
                    Constants.lstBooks.add(BooksPojo(true))
                    Handler().post { adapter.notifyItemInserted(Constants.lstBooks.size - 1) }
                    page += 1
                    data
                    Log.i("TAG", "onCreateView: 3")
                }
            }
        })
        btnNewPost.setOnClickListener {
            if (Utils.isOnline(mActivity)) { //startActivity(new Intent(mActivity, NewPostCategoryActivity.class));
                startActivity(Intent(mActivity, NewPostAddActivity::class.java))
            } else {
                internetAlert(mActivity)
            }
        }
        btnSearch.setOnClickListener {
            if (!Utils.isEmpty(edSearch)) {
                page = 1
                data
                Log.i("TAG", "onCreateView: 4")
                btnCancel.visibility = View.VISIBLE
                Utils.hideKB(mActivity, edSearch)
            }
        }
        btnCancel.setOnClickListener {
            edSearch.setText("")
            btnCancel.visibility = View.GONE
            page = 1
            data
            Log.i("TAG", "onCreateView: 5")
            Utils.hideKB(mActivity, edSearch)
        }
        textView.isSelected = true
        textView.ellipsize = TextUtils.TruncateAt.MARQUEE
        edSearch.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                btnSearch.performClick()
                return@OnEditorActionListener true
            }
            false
        })
    }

    val data: Unit
        get() {
            if (Utils.isOnline(mActivity)) {
                newsFromServer
            } else {
                recyclerView.visibility = View.GONE
                ivNoInternet.visibility = View.VISIBLE
            }
        }

    //adapter.notifyItemRangeChanged(Constants.lstBooks.size(), Constants.lstBooks.size() - news.lstBooks.size());
    val newsFromServer: Unit
        get() {
            Log.i("TAG", "getNewsFromServer: called")
            if (page == 1)  progressDialog.show()
            val result = Utils.CallApi(mActivity).books(page, edSearch.text.toString())
            result.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                   progressDialog.dismiss()
                    try {
                        Log.i("TAG", "onResponse: " + response.code())
                        if (response.code() != 200) {
                            serverAlert(mActivity)
                            return
                        }
                        val res = response.body()!!.string()
                        Log.i("response", "onResponse: $res")
                        val reader: Reader = StringReader(res)
                        val gson = GsonBuilder()
                                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                                .create()
                        val books = gson.fromJson(reader, Books::class.java)
                        //Constants.BOOKS_BASE_URL = books.base_url
                        totalPages = books.totalPages
                        if (page == 1) {
                            Constants.lstBooks.clear()
                        } else {
                            Constants.lstBooks.removeAt(Constants.lstBooks.size - 1)
                            adapter.notifyItemRemoved(Constants.lstBooks.size)
                        }
                        Constants.lstBooks.addAll(books.data)
                        Log.i("TAG", "onResponse: " + "page = " + page + "-\n total - " + totalPages + "\n" + Constants.lstBooks.size)
                        recyclerView.postDelayed({
                            adapter.notifyDataSetChanged()
                            adapter.setLoaded()
                        }, 100)
                    } catch (e: IOException) {
                        e.printStackTrace()
                        serverAlert(mActivity)
                    } catch (e: IllegalStateException) {
                        e.printStackTrace()
                        serverAlert(mActivity)
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        serverAlert(mActivity)
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    t.printStackTrace()
                    progressDialog.dismiss()
                }
            })
        }
}