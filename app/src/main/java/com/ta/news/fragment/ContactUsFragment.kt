package com.ta.news.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.snackbar.Snackbar
import com.ta.news.R
import com.ta.news.controls.CustomDialog
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.fragment_contact_us.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by arthtilva on 06-Nov-16.
 */
class ContactUsFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_contact_us, container, false)
        mActivity = activity!!
        storeUserData = StoreUserData(mActivity)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initProgress()
        btnSend.setOnClickListener {
            if (!Utils.isOnline(mActivity)) {
                internetAlert(mActivity)
            } else if (edDescription.text.toString().length < 50) {
                showAlert(mActivity, getString(R.string.minimum_contact_message))
            } else {
                contactRequest()
            }
        }
        edDescription.setText(storeUserData.getString(Constants.CONTACT_US_MESSAGE))
        edDescription.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                storeUserData.setString(Constants.CONTACT_US_MESSAGE, s.toString())
            }

            override fun afterTextChanged(s: Editable) {}
        })
        btnCall.text = "${getString(R.string.call_919427270271)} ${Constants.supportContact}"
        btnCall.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("tel:${Constants.supportContact}")
            startActivity(intent)
        }
        showAlert(mActivity, getString(R.string.contact_us_notice))
    }

    fun contactRequest() {
        progressDialog.show()
        val result = Utils.CallApi(mActivity).contactRequest(
                storeUserData.getString(Constants.USER_ID),
                edDescription.text.toString().trim()
        )
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (progressDialog.isShowing) progressDialog.dismiss()

                val responseData = response.body()!!.string()
                Log.i("response", "onResponse: $responseData")
                if (!TextUtils.isEmpty(responseData)) {
                    val jsonObject = JSONObject(responseData)
                    if (jsonObject.getBoolean("success")) {
                        storeUserData.setString(Constants.CONTACT_US_MESSAGE, "")
                        val dialog = CustomDialog(mActivity)
                        dialog.show()
                        dialog.setMessage(jsonObject.getString("message"))
                        dialog.setPositiveButton(R.string.ok, View.OnClickListener {
                            dialog.dismiss()
                            mActivity.onBackPressed()
                        })
                    } else {
                        Snackbar.make(btnSend, jsonObject.getString("message"), Snackbar.LENGTH_LONG).show()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                if (progressDialog.isShowing) progressDialog.dismiss()
                t.printStackTrace()
            }
        })
    }
}