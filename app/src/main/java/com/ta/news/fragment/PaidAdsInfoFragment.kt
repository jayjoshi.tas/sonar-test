package com.ta.news.fragment

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ta.news.R
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.activity_terms_and_conditions.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by arthtilva on 06-Nov-16.
 */
class PaidAdsInfoFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_paid_ads_info, container, false)
        mActivity = activity!!
        storeUserData = StoreUserData(mActivity)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initProgress()
        if (Utils.isOnline(mActivity)) {
            getPaidAdsInfo()
        } else {
            webView.visibility = View.GONE
            ivNoInternet.visibility = View.VISIBLE
        }
    }

    fun getPaidAdsInfo() {
        progressDialog.show()
        val result = Utils.CallApi(mActivity).getPage("advertisementPlan")
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (progressDialog.isShowing) progressDialog.dismiss()
                val responseData = response.body()!!.string()
                Log.i("response", "onResponse: $responseData")
                if (!TextUtils.isEmpty(responseData)) {
                    val jsonObject = JSONObject(responseData)
                    if (jsonObject.getBoolean("success")) {
                        webView.loadData(jsonObject.getString("data"), "text/html; charset=UTF-8", null)
                    } else {
                        showAlert(mActivity, jsonObject.getString("message"))
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                if (progressDialog.isShowing) progressDialog.dismiss()
                t.printStackTrace()
            }
        })
    }
}