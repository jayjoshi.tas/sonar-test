package com.ta.news.fragment

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.snackbar.Snackbar
import com.ta.news.R
import com.ta.news.controls.CustomDialog
import com.ta.news.activity.auth.AddBusinessActivity
import com.ta.news.media.ImageFilePath
import com.ta.news.media.image.ImagePicker
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.fragment_profile.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException

/**
 * Created by arthtilva on 06-Nov-16.
 */
class ProfileFragment : BaseFragment() {

    var isImageSelected = false
    var selectedImagePath: String? = ""
    private var mCurrentPhotoPath: String = ""
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mActivity = activity!!
        storeUserData = StoreUserData(mActivity)
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initProgress()
        if (Utils.isOnline(mActivity)) {
            profile
        }


        Log.i("TAG", "onViewCreated: "+storeUserData.getString(Constants.USER_ID))
        registerAsBusiness.setOnClickListener(View.OnClickListener {
            startActivity(Intent(mActivity, AddBusinessActivity::class.java))

        })
        btnUpdate.setOnClickListener {
            if (Utils.isEmpty(edFirstName)) {
                showAlert(mActivity, getString(R.string.enter_name))
            } else if (Utils.isEmpty(edLastName)) {
                showAlert(mActivity, getString(R.string.enter_last_name))
            } else if (edEmail.text.toString().isNotEmpty() && !Utils.isValidEmail(edEmail)) {
                showAlert(mActivity, getString(R.string.enter_valid_email))
            } else if (Utils.isEmpty(edMobile)) {
                showAlert(mActivity, getString(R.string.enter_mobile_number))
            } else if (Utils.isEmpty(edAddress)) {
                showAlert(mActivity, getString(R.string.enter_address))
            } else if (Utils.isEmpty(edCity)) {
                showAlert(mActivity, getString(R.string.enter_city))
            } else if (Utils.isEmpty(edTaluka)) {
                showAlert(mActivity, getString(R.string.enter_taluka))
            } else if (Utils.isEmpty(edPincode)) {
                showAlert(mActivity, getString(R.string.enter_pincode))
            } else if (!Utils.isOnline(mActivity)) {
                internetAlert(mActivity)
            } else {
                updateProfile()
            }
        }
        profileImage.setOnClickListener {
            ImagePicker.Builder(mActivity)
                    .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                    .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                    .directory(ImagePicker.Directory.DEFAULT)
                    .extension(ImagePicker.Extension.JPG)
                    .scale(800, 800)
                    .allowMultipleImages(false)
                    .enableDebuggingMode(false)
                    .build()
        }


    }


    fun updateProfile() {
        progressDialog.show()
        val result = Utils.CallApi(mActivity).updateProfile(
                edFirstName.text.toString().trim(),
                edLastName.text.toString().trim(),
                edEmail.text.toString().trim(),
                edAddress.text.toString().trim(),
                edCity.text.toString().trim(),
                edTaluka.text.toString().trim(),
                edPincode.text.toString().trim(),
                storeUserData.getString(Constants.USER_ID))
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (progressDialog.isShowing) progressDialog.dismiss()
                if (response.code() != 200) {
                    serverAlert(mActivity)
                    return
                }
                val responseData = response.body()!!.string()
                Log.i("TAG", "onResponse: $responseData")
                if (!TextUtils.isEmpty(responseData)) {
                    val jsonObject = JSONObject(responseData)
                    if (jsonObject.getBoolean("success")) {
                        storeUserData.setString(Constants.USER_FIRST_NAME, edFirstName.text.toString())
                        storeUserData.setString(Constants.USER_LAST_NAME, edLastName.text.toString())
                        storeUserData.setString(Constants.USER_EMAIL, edEmail.text.toString())
                        storeUserData.setString(Constants.USER_ADDRESS, edAddress.text.toString())
                        storeUserData.setString(Constants.USER_CITY, edCity.text.toString())
                        storeUserData.setString(Constants.USER_TALUKA, edTaluka.text.toString())
                        storeUserData.setString(Constants.USER_PINCODE, edPincode.text.toString())
                        val dialog = CustomDialog(mActivity)
                        dialog.show()
                        dialog.setMessage(jsonObject.getString("message"))
                        dialog.setPositiveButton(R.string.ok, View.OnClickListener {
                            dialog.dismiss()
                            mActivity.onBackPressed()
                        })
                    } else {
                        Snackbar.make(btnUpdate, jsonObject.getString("message"), Snackbar.LENGTH_LONG).show()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                if (progressDialog.isShowing) progressDialog.dismiss()
                t.printStackTrace()
            }
        })
    }

    //{"success":true,"userId":"3","firstName":"arth","lastName":"","email":"tilva.arth@gmail.com","countryCode":"","mobileNo":"+919033975383","address":"gg","pincode":"360005"}
    val profile: Unit
        get() {
            if (storeUserData.getString(Constants.USER_ID).isNotEmpty()) {
                progressDialog.show()
                val result = Utils.CallApi(mActivity).profileById(storeUserData.getString(Constants.USER_ID).toInt())
                result.enqueue(object : Callback<ResponseBody> {
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        if (progressDialog.isShowing) progressDialog.dismiss()

                        val responseData = response.body()!!.string()
                        Log.i("response", "onResponse: $responseData")
                        if (!TextUtils.isEmpty(responseData)) {
                            val jsonObject = JSONObject(responseData)
                            //{"success":true,"userId":"3","firstName":"arth","lastName":"","email":"tilva.arth@gmail.com","countryCode":"","mobileNo":"+919033975383","address":"gg","pincode":"360005"}
                            if (jsonObject.getBoolean("success")) {
                                edFirstName.setText(jsonObject.getString("firstName"))
                                edLastName.setText(jsonObject.getString("lastName"))
                                edEmail.setText(jsonObject.getString("email"))
                                edMobile.setText(storeUserData.getString(Constants.USER_COUNTRY_CODE) + storeUserData.getString(Constants.USER_PHONE_NUMBER))
                                edAddress.setText(jsonObject.getString("address"))
                                edCity.setText(jsonObject.getString("city"))
                                edTaluka.setText(jsonObject.getString("taluka"))
                                edPincode.setText(jsonObject.getString("pincode"))
                                Glide.with(mActivity).load(jsonObject.getString("profileImage").replace(" ".toRegex(), "%20")).apply(RequestOptions().transform(RoundedCorners(6)).centerCrop()
                                        .placeholder(ContextCompat.getDrawable(mActivity, R.drawable.user)))
                                        .into(profileImage)
                                storeUserData.setString(Constants.USER_FIRST_NAME, edFirstName.text.toString())
                                storeUserData.setString(Constants.USER_LAST_NAME, edLastName.text.toString())
                                storeUserData.setString(Constants.USER_EMAIL, edEmail.text.toString())
                                storeUserData.setString(Constants.USER_ADDRESS, edAddress.text.toString())
                                storeUserData.setString(Constants.USER_CITY, edCity.text.toString())
                                storeUserData.setString(Constants.USER_TALUKA, edTaluka.text.toString())
                                storeUserData.setString(Constants.USER_PINCODE, edPincode.text.toString())
                                storeUserData.setString(Constants.USER_IMAGE, jsonObject.getString("profileImage"))
                            } else {
                                showAlert(mActivity, jsonObject.getString("message"))
                            }
                        }

                    }

                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        if (progressDialog.isShowing) progressDialog.dismiss()
                        t.printStackTrace()
                    }
                })
            } else {
            }
        }

    fun setImage(path: String) {
        isImageSelected = true
        CropImage.activity(Uri.fromFile(File(path)))
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setAllowRotation(true)
                .setRequestedSize(450, 450, CropImageView.RequestSizeOptions.RESIZE_INSIDE)
                .start(mActivity, this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            Log.i("TAG", "onActivityResult: $requestCode")
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                Log.i("TAG", "onActivityResult: came here")
                val result = CropImage.getActivityResult(data)
                val resultUri = result.uri
                Glide.with(mActivity).load(resultUri).apply(RequestOptions().transform(RoundedCorners(6)).centerCrop()
                        .placeholder(ContextCompat.getDrawable(mActivity, R.drawable.user)))
                        .into(profileImage)
                uploadImage(ImageFilePath.getPath(mActivity, resultUri))
            }
        }
    }

    fun uploadImage(image: String?) {
        progressDialog.show()
        val result: Call<ResponseBody>
        val userId = RequestBody.create(MediaType.parse("multipart/form-data"), storeUserData.getString(Constants.USER_ID))
        val requestFile = RequestBody.create(MediaType.parse("image/jpeg"), File(image))
        val body = MultipartBody.Part.createFormData("profileImage", File(image).name, requestFile)
        result = Utils.CallApi(mActivity).profileImageUpload(body, userId)
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (progressDialog.isShowing) progressDialog.dismiss()
                try {
                    val res = response.body()!!.string()
                    Log.i("API_Response", res)
                    val `object` = JSONObject(res)
                    if (!`object`.getBoolean("success")) {
                        showAlert(mActivity, `object`.getString("message"))
                    } else {
                        Glide.with(mActivity).load(`object`.getString("image").replace(" ".toRegex(), "%20")).apply(RequestOptions().transform(RoundedCorners(6)).centerCrop()
                                .placeholder(ContextCompat.getDrawable(mActivity, R.drawable.user)))
                                .into(profileImage)
                        storeUserData.setString(Constants.USER_IMAGE, `object`.getString("image"))
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                if (progressDialog.isShowing) progressDialog.dismiss()
                Log.i("TAG", "onFailure: " + t.message)
                Toast.makeText(mActivity, "Error Response : " + t.message + "" + call.toString(), Toast.LENGTH_LONG).show()
            }
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (mCurrentPhotoPath != null) {
            outState.putString("cameraImageUri", mCurrentPhotoPath)
        }
    }
}