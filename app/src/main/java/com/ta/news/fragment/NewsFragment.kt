package com.ta.news.fragment

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.facebook.ads.AdError
import com.facebook.ads.NativeAdsManager
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import com.ta.news.R
import com.ta.news.activity.FaqActivity
import com.ta.news.activity.plan.PlansActivity
import com.ta.news.activity.post.NewPostCategoryActivity
import com.ta.news.adapter.NewsAdapter
import com.ta.news.pojo.DistrictPojo
import com.ta.news.pojo.News
import com.ta.news.pojo.NewsCategory
import com.ta.news.pojo.NewsPojo
import com.ta.news.utils.Constants
import com.ta.news.utils.OnLoadMoreListener
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.fragment_news.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.io.Reader
import java.io.StringReader
import java.lang.reflect.Modifier
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by arthtilva on 06-Nov-16.
 */
class NewsFragment : BaseFragment() {

    private var mNativeAdsManager: NativeAdsManager? = null
    private var adLoader: AdLoader? = null
    var page = 1
    var categoryId = 0
    lateinit var layoutManager: LinearLayoutManager
    var totalPages = 1
    lateinit var adapter: NewsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_news, container, false)
        mActivity = activity!!
        storeUserData = StoreUserData(mActivity)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initProgress()

        /*AppLovinSdk.getInstance(mActivity).nativeAdService.loadNextAd(object : AppLovinNativeAdLoadListener {
            override fun onNativeAdsLoaded(nativeAds: List<AppLovinNativeAd>) {
                // List of one native ad loaded; do something with this, e.g. render into your custom view.
                Log.i("TAG", "onNativeAdsLoaded: ")
            }

            override fun onNativeAdsFailedToLoad(errorCode: Int) {
                // Native ads failed to load for some reason, likely a network error.
                // Compare errorCode to the available constants in AppLovinErrorCodes.
                Log.i("TAG", "onNativeAdsFailedToLoad: ")
                if (errorCode == AppLovinErrorCodes.NO_FILL) {
                    // No ad was available for this placement
                }
                // else if (errorCode == .... ) { ... }
            }
        })*/


        mNativeAdsManager = NativeAdsManager(mActivity, getString(R.string.fb_news_list_native), 8)
        mNativeAdsManager!!.setListener(object : NativeAdsManager.Listener {
            override fun onAdsLoaded() {
                Log.d("TestData", "AdError : Loaded")
            }

            override fun onAdError(adError: AdError?) {
                if (adError != null) {
                    Log.d("TestData", "AdError : ${adError.errorCode} :  ${adError.errorMessage}")
                }
                FirebaseAnalytics.getInstance(mActivity).logEvent("facebookAdFailed", Utils.getUserDetails(mActivity))
            }
        })


        val builder: AdLoader.Builder = AdLoader.Builder(mActivity, getString(R.string.google_news_list_native))
        adLoader = builder.forUnifiedNativeAd { unifiedNativeAd ->
            Log.i("MainActivity", "onUnifiedNativeAdLoaded:ad loaded ")
            adapter.unifiedNativeAd.add(unifiedNativeAd)
        }.withAdListener(
                object : AdListener() {
                    override fun onAdFailedToLoad(errorCode: Int) {
                        Log.e("MainActivity", "The previous native ad failed to load. Attempting to"
                                + " load another.${adLoader?.isLoading} $errorCode")
                        FirebaseAnalytics.getInstance(mActivity).logEvent("googleAdFailed", Utils.getUserDetails(mActivity))
                    }
                }).build()

        if (storeUserData.getBoolean(Constants.IS_FACEBOOK)) {
            FirebaseAnalytics.getInstance(mActivity).logEvent("facebookAdRequested", Utils.getUserDetails(mActivity))
            mNativeAdsManager?.loadAds()
        }
        FirebaseAnalytics.getInstance(mActivity).logEvent("googleAdRequested", Utils.getUserDetails(mActivity))
        adLoader?.loadAds(AdRequest.Builder().build(), 8)

        layoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        rvNews.setHasFixedSize(true)
        rvNews.layoutManager = layoutManager
        adapter = NewsAdapter(mActivity, rvNews, Constants.lstNews, categoryId, mNativeAdsManager!!)
        rvNews.adapter = adapter
        categoryId = arguments!!.getInt("categoryId")
        data
        swipeRefreshNews.setOnRefreshListener {
            swipeRefreshNews.isRefreshing = false
            currentAdPosition = 9
            page = 1
            data
        }
        //TODO: Check all search edit text- cutting from bottom edge.
        btnNewPost.setOnClickListener {
            when {
                storeUserData.getInt(Constants.USER_STATUS) != 1 -> {
                    showAlert(mActivity, storeUserData.getString(Constants.REASON_MESSAGE))
                }
                Utils.isOnline(mActivity) -> {
                    checkNewsAllowed()
                }
                else -> {
                    internetAlert(mActivity)
                }
            }
        }
        btnSearch.setOnClickListener {
            if (!Utils.isEmpty(edSearch)) {
                currentAdPosition = 9
                page = 1
                data
                btnCancel.visibility = View.VISIBLE
                Utils.hideKB(mActivity, edSearch)
            }
        }
        btnCancel.setOnClickListener {
            edSearch.setText("")
            btnCancel.visibility = View.GONE
            currentAdPosition = 9
            page = 1
            data
            Utils.hideKB(mActivity, edSearch)
        }
        textView.isSelected = true
        textView.ellipsize = TextUtils.TruncateAt.MARQUEE
        edSearch.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                btnSearch.performClick()
                return@OnEditorActionListener true
            }
            false
        })

        btnFilter.setOnClickListener {
            val districtFragment = DistrictFilterFragment(itemClickListener)
            var bundle = Bundle()
            districtFragment.arguments = bundle
            districtFragment.show(
                    childFragmentManager,
                    DistrictFragment::class.java.simpleName
            )
        }
        getDistrictList()

        val newsCategory = Gson().fromJson(StringReader(storeUserData.getString(Constants.NEWS_CATEGORY)), NewsCategory::class.java)
        btnFilter.isVisible = newsCategory.districtDisplay

        btnHelp.setOnClickListener {
            startActivity(Intent(activity, FaqActivity::class.java))
        }
    }

    interface ItemClickListener {
        fun onDistrictSelect(pojo: DistrictPojo)
    }

    var itemClickListener = object : ItemClickListener {
        override fun onDistrictSelect(pojo: DistrictPojo) {
            currentAdPosition = 9
            page = 1
            data
        }
    }
    val data: Unit
        get() {
            if (Utils.isOnline(mActivity)) {
                newsFromServer
            } else {
                swipeRefreshNews.visibility = View.GONE
                ivNoInternet.visibility = View.VISIBLE
            }
        }

    val newsFromServer: Unit
        get() {
            count.isVisible = !storeUserData.getString(Constants.DISTRICT_ID).isEmpty()
            if (page == 1 && swipeRefreshNews != null) progressDialog.show()
            val result = Utils.CallApi(mActivity).news(
                    categoryId,
                    page,
                    edSearch.text.toString().trim(), storeUserData.getString(Constants.DISTRICT_ID), "android")
            result.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    if (progressDialog.isShowing)
                        progressDialog.dismiss()
                    try {
                        if (response.code() != 200) {
                            serverAlert(mActivity)
                            return
                        }
                        val res = response.body()!!.string()
                        Log.i("response", "onResponse: $res")
                        val reader: Reader = StringReader(res)
                        val gson = GsonBuilder()
                                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                                .serializeNulls()
                                .create()
                        val news = gson.fromJson(reader, News::class.java)
                        if (news != null) {
                            if (news.success) {
                                totalPages = news.totalPages
                                if (page == 1) {
                                    if (news.advertisement != null && news.advertisement!!.size > 0) {
                                        Constants.lstAdvs.clear()
                                        Constants.lstAdvs.addAll(news.advertisement!!)
                                    }
                                }
                                setAdapter(news.data)
                            } else {
                                setAdapter(null)
                                showAlert(mActivity, news.message)
                            }

                        } else {
                            serverAlert(mActivity)
                        }
                        Log.i("TAG", "onResponse: " + "page = " + page + "-\n total - " + totalPages + "\n" + Constants.lstNews.size)
                    } catch (e: IOException) {
                        e.printStackTrace()
                        adapter.setLoaded()
                        serverAlert(mActivity)
                    } catch (e: IllegalStateException) {
                        e.printStackTrace()
                        adapter.setLoaded()
                        serverAlert(mActivity)
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        adapter.setLoaded()
                        serverAlert(mActivity)
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    t.printStackTrace()
                    adapter.setLoaded()
                    if (progressDialog.isShowing)
                        progressDialog.dismiss()
                }
            })
        }


    private fun setAdapter(newData: ArrayList<NewsPojo>?) {
        if (page == 1) {
            Constants.lstNews.clear()
            adapter.setOnLoadMoreListener(null)
            if (newData != null) {
                Constants.lstNews.addAll(newData)
                setAdData()
                adapter.setOnLoadMoreListener(object : OnLoadMoreListener {
                    override fun onLoadMore() {
                        if (page < totalPages) {
                            progress.show()
                            page += 1
                            data
                        }
                    }
                })
            }
            adapter.notifyDataSetChanged()
            adapter.setLoaded()
        } else {
            progress.hide()
            if (newData != null)
                Constants.lstNews.addAll(newData)
            setAdData()
            adapter.notifyDataSetChanged()
            adapter.setLoaded()
        }
    }

    private var currentAdPosition = 9
    private fun setAdData() {
        if (storeUserData.getBoolean(Constants.IS_PREMIUM)) {
            return
        }

        for (i in currentAdPosition until Constants.lstNews.size) {
            if ((i + 1) % 10 == 0) {
                Constants.lstNews.add(i, NewsPojo(isAd = true))
            }
            currentAdPosition = i + 10
        }
    }


    private fun getDistrictList() {
        var dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        if (dateFormat.format(Date()) != storeUserData.getString(Constants.NEWS_CAT_SYNC_DATE) || TextUtils.isEmpty(storeUserData.getString(Constants.DISTRICT_LIST))) {
            val result = Utils.CallApi(mActivity).getDistrict()
            result.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    if (response.code() != 200) {
                        return
                    }
                    try {
                        val res = response.body()!!.string()
                        Log.i("response", "onResponse: $res")
                        storeUserData.setString(Constants.DISTRICT_LIST, res)
                    } catch (e: IOException) {
                        e.printStackTrace()
                    } catch (e: IllegalStateException) {
                        e.printStackTrace()
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    t.printStackTrace()
                    if (swipeRefreshNews != null)
                        swipeRefreshNews.isRefreshing = false
                }
            })
        }
    }

    private fun checkNewsAllowed() {
        val result = Utils.CallApi(mActivity).addNewsUserAllowed(storeUserData.getString(Constants.USER_ID))
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.code() != 200) {
                    return
                }
                try {
                    val res = response.body()!!.string()
                    Log.i("response", "onResponse: $res")
                    var json = JSONObject(res)
                    if (json.getBoolean("success")) {
                        startActivity(Intent(mActivity, NewPostCategoryActivity::class.java))
                    } else {
                        startActivity(Intent(mActivity, PlansActivity::class.java))
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                } catch (e: JsonSyntaxException) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                if (swipeRefreshNews != null)
                    swipeRefreshNews.isRefreshing = false
            }
        })
    }
}