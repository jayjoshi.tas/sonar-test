package com.ta.news.fragment

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import com.ta.news.R
import com.ta.news.activity.FaqActivity
import com.ta.news.activity.plan.PlansActivity
import com.ta.news.activity.post.NewPostCategoryActivity
import com.ta.news.adapter.NewsCategoryAdapter
import com.ta.news.pojo.NewsCategory
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import com.ta.news.utils.Utils
import kotlinx.android.synthetic.main.fragment_category.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.io.Reader
import java.io.StringReader
import java.lang.reflect.Modifier
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by arthtilva on 06-Nov-16.
 */
class CategoryFragment : BaseFragment() {


    var dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_category, container, false)
        mActivity = activity!!
        storeUserData = StoreUserData(mActivity)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initProgress()
        rvCategory.setHasFixedSize(true)
        rvCategory.layoutManager = LinearLayoutManager(mActivity)
        swipeRefreshCategory.setOnRefreshListener {
            if (Utils.isOnline(mActivity)) {
                getCategoryFromServer()
            } else {
                swipeRefreshCategory.visibility = View.GONE
                ivNoInternet.visibility = View.VISIBLE
                swipeRefreshCategory.isRefreshing = false
            }
        }
        btnNewPost.setOnClickListener {
            when {
                storeUserData.getInt(Constants.USER_STATUS) != 1 -> {
                    showAlert(mActivity, storeUserData.getString(Constants.REASON_MESSAGE))
                }
                Utils.isOnline(mActivity) -> {
                    checkNewsAllowed()
                }
                else -> {
                    internetAlert(mActivity)
                }
            }
        }
        textView.isSelected = true
        textView.ellipsize = TextUtils.TruncateAt.MARQUEE
        btnHelp.setOnClickListener {
            startActivity(Intent(activity, FaqActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()
        getData()
    }

    private fun getData() {
        if (!Utils.isOnline(mActivity)) {
            swipeRefreshCategory.visibility = View.GONE
            ivNoInternet.visibility = View.VISIBLE
            swipeRefreshCategory.isRefreshing = false
        } else if (storeUserData.getString(Constants.CATEGORY_BASE_URL).isEmpty() || dateFormat.format(Date()) != storeUserData.getString(Constants.NEWS_CAT_SYNC_DATE) || storeUserData.getString(Constants.NEWS_CATEGORY).isEmpty()) {
            getCategoryFromServer()
        } else {
            try {
                val reader: Reader = StringReader(storeUserData.getString(Constants.NEWS_CATEGORY))
                val gson = GsonBuilder()
                        .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                        .serializeNulls()
                        .create()
                val newsCategory = gson.fromJson(reader, NewsCategory::class.java)
                if (newsCategory != null) {
                    rvCategory.adapter = NewsCategoryAdapter(mActivity, newsCategory.data)
                    storeUserData.setBoolean(Constants.IS_SURVEY_FORM_AVAILABLE, newsCategory.isSurveyForm)
                    btnNewPost.isEnabled = true
                } else {
                    getCategoryFromServer()
                }
            } catch (e: Exception) {
                getCategoryFromServer()
            }
        }
    }


    private fun getCategoryFromServer() {
        if (swipeRefreshCategory != null)
            swipeRefreshCategory.isRefreshing = true
        val result = Utils.CallApi(mActivity).newsCategory("get")
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (swipeRefreshCategory != null)
                    swipeRefreshCategory.isRefreshing = false
                if (response.code() != 200) {
                    serverAlert(mActivity)
                    return
                }
                try {
                    val res = response.body()!!.string()
                    Log.i("response", "onResponse: $res")
                    val reader: Reader = StringReader(res)
                    val gson = GsonBuilder()
                            .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                            .serializeNulls()
                            .create()
                    storeUserData.setString(Constants.NEWS_CATEGORY, res)
                    storeUserData.setString(Constants.NEWS_CAT_SYNC_DATE, dateFormat.format(Date()))
                    val newsCategory = gson.fromJson(reader, NewsCategory::class.java)
                    if (newsCategory != null) {
                        if (newsCategory.success && newsCategory.data.size > 0) {
                            storeUserData.setString(Constants.CATEGORY_BASE_URL, newsCategory.base_url)
                            rvCategory.adapter = NewsCategoryAdapter(mActivity, newsCategory.data)
                            storeUserData.setBoolean(Constants.IS_SURVEY_FORM_AVAILABLE, newsCategory.isSurveyForm)
                            btnNewPost.isEnabled = true
                        } else {
                            showAlert(mActivity, newsCategory.message)
                        }
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                } catch (e: JsonSyntaxException) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                if (swipeRefreshCategory != null)
                    swipeRefreshCategory.isRefreshing = false
            }
        })
    }

    private fun checkNewsAllowed() {
        val result = Utils.CallApi(mActivity).addNewsUserAllowed(storeUserData.getString(Constants.USER_ID))
        result.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.code() != 200) {
                    return
                }
                try {
                    val res = response.body()!!.string()
                    Log.i("response", "onResponse: $res")
                    var json = JSONObject(res)
                    if (json.getBoolean("success")) {
                        startActivity(Intent(mActivity, NewPostCategoryActivity::class.java))
                    } else {
                        startActivity(Intent(activity, PlansActivity::class.java))
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                } catch (e: JsonSyntaxException) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                if (swipeRefreshCategory != null)
                    swipeRefreshCategory.isRefreshing = false
            }
        })
    }

}