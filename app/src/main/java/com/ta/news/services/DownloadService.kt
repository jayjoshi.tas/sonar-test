package com.ta.news.services

import android.app.IntentService
import android.content.Intent
import android.util.Log
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL

class DownloadService : IntentService {
    constructor() : super("DownloadService") {}
    constructor(name: String?) : super(name) {}

    override fun onHandleIntent(intent: Intent?) {
        try {
            if (intent != null && intent.extras != null) {
                val stringUrl = intent.extras!!.getString("url")
                val fileName = stringUrl!!.substring(stringUrl.lastIndexOf('/') + 1)
                val splashImage = File(filesDir.absolutePath + "/" + fileName)
                val url = URL(stringUrl)
                val urlConnection = url.openConnection() as HttpURLConnection
                urlConnection.requestMethod = "GET"
                //urlConnection.setDoOutput(true);
                urlConnection.connect()
                val fileOutput = FileOutputStream(splashImage)
                val inputStream = urlConnection.inputStream
                val buffer = ByteArray(1024)
                var bufferLength = 0
                while (inputStream.read(buffer).also { bufferLength = it } > 0) {
                    fileOutput.write(buffer, 0, bufferLength)
                }
                fileOutput.close()
                Log.i("TAG", "Download complete : $stringUrl")
            }
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
}