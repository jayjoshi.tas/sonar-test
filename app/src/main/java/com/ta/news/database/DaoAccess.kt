//package com.ta.news.database
//
//import androidx.lifecycle.LiveData
//import androidx.room.*
//
//@Dao
//interface DaoAccess {
//    @Insert
//    fun insertTask(note: TblNews?): Long?
//
//    @Query("SELECT * FROM TblNews ORDER BY created_at desc")
//    fun fetchAllTasks(): LiveData<List<TblNews?>?>?
//
//    @Query("SELECT * FROM TblNews WHERE id =:taskId")
//    fun getTask(taskId: Int): LiveData<TblNews?>?
//
//    @Update
//    fun updateTask(note: TblNews?)
//
//    @Delete
//    fun deleteTask(note: TblNews?)
//
//
//
//}