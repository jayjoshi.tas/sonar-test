package com.ta.news.receiver

import android.content.Context
import com.ta.news.R
import com.ta.news.utils.Utils
import net.gotev.uploadservice.ServerResponse
import net.gotev.uploadservice.UploadInfo
import net.gotev.uploadservice.UploadServiceBroadcastReceiver

/**
 * Created by Arth on 20-Aug-17.
 */
class UploadMediaReceiver : UploadServiceBroadcastReceiver() {
    override fun onProgress(context: Context, uploadInfo: UploadInfo) { // your implementation
    }

    override fun onError(context: Context, uploadInfo: UploadInfo, serverResponse: ServerResponse?, exception: Exception) {
        super.onError(context, uploadInfo, serverResponse, exception)
    }

    override fun onCompleted(context: Context, uploadInfo: UploadInfo, serverResponse: ServerResponse) { // your implementation
        Utils.showToast(context, R.string.file_uploaded_successfully)
    }

    override fun onCancelled(context: Context, uploadInfo: UploadInfo) { // your implementation
    }
}