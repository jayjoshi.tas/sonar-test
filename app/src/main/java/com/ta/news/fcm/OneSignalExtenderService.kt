package com.ta.news.fcm

import android.util.Log
import androidx.core.app.NotificationCompat
import com.onesignal.NotificationExtenderService
import com.onesignal.OSNotificationReceivedResult
import com.ta.news.pojo.Notification
import com.ta.news.utils.Constants
import com.ta.news.utils.StoreUserData
import org.json.JSONException
import java.math.BigInteger

class OneSignalExtenderService : NotificationExtenderService() {
    override fun onNotificationProcessing(receivedResult: OSNotificationReceivedResult): Boolean { // Read Properties from result
        val overrideSettings = OverrideSettings()
        overrideSettings.extender = NotificationCompat.Extender { builder: NotificationCompat.Builder -> builder.setColor(BigInteger("FF37A000", 16).toInt()) }
        Log.i("TAG", "onNotificationProcessing: " + receivedResult.payload.additionalData)
        Log.i("TAG", "onNotificationProcessing: " + receivedResult.payload.body)
        if (receivedResult.payload.additionalData != null) {
            try {
                when (receivedResult.payload.additionalData.getInt("cmd")) {
                    1 -> {
                        val notification = Notification()
                        notification.message = receivedResult.payload.additionalData.getString("message")
                        notification.time = System.currentTimeMillis()
                        notification.isRead = false
                        notification.postId = -1
                        if (receivedResult.payload.additionalData.has("postId")) {
                            notification.postId = receivedResult.payload.additionalData.getInt("postId")
                        }
                        notification.save()
                    }
                    2 -> StoreUserData(this).clearData(this)
                    6 -> if (StoreUserData(this).getString(Constants.USER_ID).isEmpty()) { //not to show notification
                        return true
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
        val displayedResult = displayNotification(overrideSettings)
        Log.d("OneSignalExample", "Notification displayed with id: " + displayedResult.androidNotificationId)
        return true
    }
}