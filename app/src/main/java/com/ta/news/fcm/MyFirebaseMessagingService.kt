//package com.ta.news.fcm
//
//import android.app.NotificationChannel
//import android.app.NotificationManager
//import android.app.PendingIntent
//import android.content.Context
//import android.content.Intent
//import android.graphics.Color
//import android.media.RingtoneManager
//import android.os.Build
//import android.util.Log
//import androidx.core.app.NotificationCompat
//import com.bumptech.glide.Glide
//import com.google.firebase.messaging.FirebaseMessagingService
//import com.google.firebase.messaging.RemoteMessage
//import com.ta.news.R
//import com.ta.news.main.MainActivity
//import com.ta.news.main.MainApplication
//import com.ta.news.main.NewsDetailActivity
//import com.ta.news.main.SplashActivity
//import com.ta.news.pojo.Notification
//import com.ta.news.utils.Constants
//import com.ta.news.utils.StoreUserData
//
///**
// * Created by arthtilva on 15-08-2016.
// */
//class MyFirebaseMessagingService : FirebaseMessagingService() {
//    override fun onNewToken(s: String) {
//        super.onNewToken(s)
//        StoreUserData(this).setString(Constants.USER_FCM, s)
//    }
//
//    override fun onMessageReceived(remoteMessage: RemoteMessage) { // TODO(developer): Handle FCM messages here.
//        Log.d(TAG,
//                "From: " + remoteMessage.from)
//        if (remoteMessage.data.isNotEmpty()) {
//
//            var data = remoteMessage.data
//            if (data["cmd"] != null) {
//                if (data["cmd"]!!.toInt() == 1) {
//                    val notification = Notification()
//                    notification.message = data["message"]!!
//                    notification.time = System.currentTimeMillis()
//                    notification.isRead = false
//                    notification.postId = -1
//                    if (data.contains("postId")) {
//                        notification.postId = data["postId"]!!.toInt()
//                    }
//                    notification.save()
//                } else if (data["cmd"]!!.toInt() == 2) {
//                    StoreUserData(this).clearData(this)
//                }
//            }
//            Log.d(TAG,
//                    "Message data payload: " + remoteMessage.data["message"])
//            sendNotification(data)
//        }
//    }
//
//    private fun sendNotification(data: MutableMap<String, String>) {
//        var intent = Intent(this, SplashActivity::class.java)
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
//        if (data["cmd"] != null) {
//            if (data["cmd"]!!.toInt() == 1) {
//                if (StoreUserData(this).getString(Constants.USER_ID).isNotEmpty()) {
//                    if (data.containsKey("postId") && data["postId"] != null) {
//                        intent = Intent(MainApplication.instance, NewsDetailActivity::class.java)
//                        intent.putExtra("openCmd", 6)
//                        intent.putExtra("postId", data["postId"]!!.toInt())
//                    } else {
//                        intent = Intent(MainApplication.instance, MainActivity::class.java)
//                        intent.putExtra("openCmd", data["cmd"]!!.toInt())
//                    }
//                }
//            } else if (data["cmd"]!!.toInt() == 2) {
//                intent = Intent(MainApplication.instance, SplashActivity::class.java)
//            } else if (data["cmd"]!!.toInt() == 3 || data["cmd"]!!.toInt() == 4) {
//                if (StoreUserData(this).getString(Constants.USER_ID).isNotEmpty()) {
//                    intent = Intent(MainApplication.instance, MainActivity::class.java)
//                    intent.putExtra("openCmd", data["cmd"]!!.toInt())
//                }
//            } else if (data["cmd"]!!.toInt() == 5) {
//
//            } else if (data["cmd"]!!.toInt() == 6 && StoreUserData(this).getString(Constants.USER_ID).isNotEmpty()) {
//                intent = Intent(MainApplication.instance, NewsDetailActivity::class.java)
//                intent.putExtra("openCmd", data["cmd"]!!.toInt())
//                intent.putExtra("postId", data["postId"]!!.toInt())
//            }
//        }
//
//        val pendingIntent = PendingIntent.getActivity(
//                this, notificationId, intent,
//                PendingIntent.FLAG_ONE_SHOT
//        )
//
//        val notificationManager =
//                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//        val channelId = "signals"
//        val importance = NotificationManager.IMPORTANCE_HIGH
//        val channelName: CharSequence = getString(R.string.app_name)
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            val notificationChannel =
//                    NotificationChannel(channelId, channelName, importance)
//            notificationChannel.enableLights(true)
//            notificationChannel.lightColor = Color.RED
//            notificationChannel.enableVibration(true)
//            notificationChannel.vibrationPattern = longArrayOf(100, 200)
//            notificationManager.createNotificationChannel(notificationChannel)
//        }
//
//        val defaultSoundUri =
//                RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
//        val notificationBuilder =
//                NotificationCompat.Builder(this, channelId)
//                        .setContentTitle(data["message"])
//                        .setContentText(data["title"])
//                        .setSmallIcon(R.drawable.ic_notification_white)
//                        .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
//                        .setAutoCancel(true)
//                        .setVibrate(longArrayOf(100, 200))
//                        .setSound(defaultSoundUri)
//                        .setContentIntent(pendingIntent)
//        if (data["big_picture"] != null && data["big_picture"].toString().isNotEmpty()) {
//            val futureTarget = Glide.with(this).asBitmap()
//                    .load(data["big_picture"])
//                    .submit()
//            notificationBuilder.setStyle(NotificationCompat.BigPictureStyle().bigPicture(futureTarget.get()))
//        }
//        notificationId++
//        notificationManager.notify(
//                notificationId,
//                notificationBuilder.build()
//        )
//    }
//
//    companion object {
//        var notificationId = 0
//        private const val TAG = "MyFirebaseMsgService"
//    }
//}