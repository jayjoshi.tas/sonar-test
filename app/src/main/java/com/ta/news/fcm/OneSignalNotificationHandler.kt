package com.ta.news.fcm

import android.content.Intent
import android.net.Uri
import com.onesignal.OSNotificationOpenResult
import com.onesignal.OneSignal.NotificationOpenedHandler
import com.ta.news.R
import com.ta.news.activity.MainActivity
import com.ta.news.activity.MainApplication
import com.ta.news.activity.SplashActivity
import com.ta.news.activity.post.NewsDetailActivity
import org.json.JSONException

class OneSignalNotificationHandler : NotificationOpenedHandler {

    var icon = R.drawable.ic_notification_white

    override fun notificationOpened(result: OSNotificationOpenResult) {
        if (result.notification.payload.launchURL != null && result.notification.payload.launchURL.isNotEmpty()) {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(result.notification.payload.launchURL))
            browserIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            MainApplication.instance!!.startActivity(browserIntent)
        } else if (result.notification.payload.additionalData != null) {
            when (result.notification.payload.additionalData.optInt("cmd")) {
                1 -> try {
                    if (result.notification.payload.additionalData.has("postId") && result.notification.payload.additionalData.getString("postId") != null) {
                        val intent = Intent(MainApplication.instance, NewsDetailActivity::class.java)
                        intent.putExtra("openCmd", 6)
                        intent.putExtra("postId", result.notification.payload.additionalData.optInt("postId"))
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                        MainApplication.instance!!.startActivity(intent)
                    } else {
                        val intent = Intent(MainApplication.instance, MainActivity::class.java)
                        intent.putExtra("openCmd", result.notification.payload.additionalData.optInt("cmd"))
                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_NEW_TASK)
                        MainApplication.instance!!.startActivity(intent)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                2 -> {
                    val intent = Intent(MainApplication.instance, SplashActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_NEW_TASK)
                    MainApplication.instance!!.startActivity(intent)
                }
                3, 4 -> {
                    val intentMessage = Intent(MainApplication.instance, MainActivity::class.java)
                    intentMessage.putExtra("openCmd", result.notification.payload.additionalData.optInt("cmd"))
                    intentMessage.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_NEW_TASK)
                    MainApplication.instance!!.startActivity(intentMessage)
                }
                5 -> {
                    val intentMessage = Intent(MainApplication.instance, MainActivity::class.java)
                    intentMessage.putExtra("openCmd", result.notification.payload.additionalData.optInt("cmd"))
                    intentMessage.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_NEW_TASK)
                    MainApplication.instance!!.startActivity(intentMessage)
                }
                6 -> {
                    val intentNews = Intent(MainApplication.instance, NewsDetailActivity::class.java)
                    intentNews.putExtra("openCmd", result.notification.payload.additionalData.optInt("cmd"))
                    intentNews.putExtra("postId", result.notification.payload.additionalData.optInt("postId"))
                    intentNews.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_NEW_TASK)
                    MainApplication.instance!!.startActivity(intentNews)
                }
            }
        } else {
            val intent = Intent(MainApplication.instance, SplashActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_NEW_TASK)
            MainApplication.instance!!.startActivity(intent)
        }
    }
}