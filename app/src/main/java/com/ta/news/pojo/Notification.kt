package com.ta.news.pojo

import com.activeandroid.Model
import com.activeandroid.annotation.Column
import com.activeandroid.annotation.Table
import com.google.gson.annotations.SerializedName

/**
 * Created by arthtilva on 26-Feb-17.
 */
@Table(name = "Notification")
class Notification : Model() {
    @Column(name = "message")
    var message: String = ""
    @Column(name = "postId")
    var postId = -1
    @Column(name = "time")
    var time: Long = 0
    @Column(name = "isRead")
    var isRead = false
}