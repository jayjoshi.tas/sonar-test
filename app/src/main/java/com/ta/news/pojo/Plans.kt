package com.ta.news.pojo

import java.io.Serializable

/**
 * Created by arthtilva on 07-Nov-16.
 */
class Plans {
    var data: ArrayList<PlansPojo> = ArrayList()
    var success = false
    var message = ""
}

class PlansPojo : Serializable {
    var id = 0
    var planId: String = ""
    var planName: String = ""
    var totalPost: String = ""
    var planDay: String = ""
    var price: String = ""
    var isRecommended = 0
}