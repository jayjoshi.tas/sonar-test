package com.ta.news.pojo

import java.io.Serializable

/**
 * Created by arthtilva on 07-Nov-16.
 */
class Faq {
    var data: ArrayList<FaqPojo> = ArrayList()
    var success = false
    var message = ""
}

class FaqPojo : Serializable {
    var id = 0
    var question: String = ""
    var answer: String = ""
}