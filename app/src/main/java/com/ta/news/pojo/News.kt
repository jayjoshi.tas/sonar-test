package com.ta.news.pojo

import java.io.Serializable
import java.util.*

/**
 * Created by arthtilva on 07-Nov-16.
 */
class News {
    var data: ArrayList<NewsPojo>? = null
    var advertisement: ArrayList<Ads>? = null
    var totalPages = 0
    var success = false
    var base_url = ""
    var message: String = ""

    inner class Ads {
        var adImage: String = ""
        var folderPath: String = ""
        var androidUrl: String = ""
        var mobileNo: String = ""
        var whatsappNo: String = ""
        var adName: String = ""
    }

    companion object {
        const val IMAGE = 1
        const val AUDIO = 2
        const val VIDEO = 3
        const val PDF = 4
    }
}


class MediaPOJO : Serializable {
    var mediaName: String = ""
    var folderPath: String = ""
    var thumbFolderPath: String? = ""
    var thumbImage: String? = ""
    var type = 0

    //book
    var images: String = ""
}

class NewsPojo : Serializable {
    var isAd = false
    var isAdShown = false
    var postId = 0
    var newsTitle: String = ""
    var newsDetails: String = ""
    var address: String = ""

    //User news history
    var status: String = ""
    var categoryId = 0
    var statusCode = 0
    var notificationStatus = 0
    var reason: String = ""
    var addDate: String = ""
    var userId = 0
    var firstName: String = ""
    var lastName: String = ""
    var mobileNo: String = ""
    var countryCode: String = ""
    var profileImage: String = ""
    var approxPrice: String = ""
    var isVerified = false
    var mediaArray = ArrayList<MediaPOJO>()

    constructor()
    constructor(isAd: Boolean) {
        this.isAd = isAd
    }
}