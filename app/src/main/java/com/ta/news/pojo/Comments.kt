package com.ta.news.pojo

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

/**
 * Created by arthtilva on 07-Nov-16.
 */
class Comments {
    var data: List<CommentPojo> = ArrayList()
    var success = false
    var totalPages = 0
}

class CommentPojo : Serializable {
    var isLoader = false
    var userId = 0
    var firstName: String = ""
    var lastName: String = ""
    var mobileNo: String = ""
    var countryCode: String = ""
    var image: String = ""
    var isVerified = 0
    var id = 0
    var comment: String = ""
    var addDate: String = ""

    constructor()
    constructor(loader: Boolean) {
        isLoader = loader
    }
}