package com.ta.news.pojo

import java.io.Serializable

/**
 * Created by arthtilva on 07-Nov-16.
 */
class PlanType {
    var data: ArrayList<PlanTypePojo> = ArrayList()
    var success = false
    var message = ""
}

class PlanTypePojo : Serializable {
    var title: String = ""
    var type: String = ""
    var description: String = ""
    var amount = 0.0
}