package com.ta.news.pojo

import com.activeandroid.Model
import java.util.*

/**
 * Created by arthtilva on 07-Nov-16.
 */
class District {
    var data = ArrayList<DistrictPojo>()
    var message: String = ""
    var success = false
}

class DistrictPojo {
    var id = 0
    var districtGu: String = ""
    var districtEn: String = ""
    //for multi-select in new post
    var isSelected = false
}