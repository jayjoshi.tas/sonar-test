package com.ta.news.pojo

import java.io.Serializable
import java.util.*

/**
 * Created by arthtilva on 07-Nov-16.
 */
class Product {
    var data: List<ProductCategory> = ArrayList()
    var success = false
    var base_url = ""
    var slider = ArrayList<Media>()
}

class ProductCategory : Serializable {
    var id = 0
    var totalPages = 0
    var success = false
    var name: String = ""
    var isLoader = false
    var image: String = ""
    var productArray = ArrayList<ProductPojo>()
    var data = ArrayList<ProductPojo>()

}

class ProductPojo(loader: Boolean) : Serializable {
    var id = ""
    var userId: String = ""
    var categoryId: String = ""
    var name: String = ""
    var isLoader = loader
    var regularPrice: String = ""
    var salePrice: String = ""
    var cityId: String = ""
    var description: String = ""
    var deliveryAvailable: String = ""
    var district: String = ""
    var startDate: String = ""
    var endDate: String = ""
    var isFeatured: String = ""
    var image = ArrayList<Media>()

}

class Media : Serializable {
    var id = ""
    var productId = ""
    var image = ""

}