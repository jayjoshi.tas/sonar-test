package com.ta.news.pojo

import java.io.Serializable

class Content {

    var success = false
    var base_url: String = ""
    var message: String = ""
    var data = ArrayList<ContentCategory>()
    var post = ArrayList<ContentPost>()


}

class ContentCategory : Serializable {
    var id: String = ""
    var name: String = ""
    var image: String = ""
    var addDate: String = ""
}

class ContentPost : Serializable {
    var id: String = ""
    var title: String = ""
    var details: String = ""
    var thumbImage: String = ""
    var addDate: String = ""
    var mediaArray: ArrayList<MediaArrayBean> = ArrayList()
}

class MediaArrayBean : Serializable {
    var postId = ""
    var image = ""
    var type = 0

    companion object {
        const val IMAGE = 1
        const val VIDEO = 2
        const val PDF = 3
    }
}