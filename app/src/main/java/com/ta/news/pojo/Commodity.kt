package com.ta.news.pojo

import java.io.Serializable
import java.util.*

/**
 * Created by arthtilva on 07-Nov-16.
 */
class Commodity {
    var data = ArrayList<CommodityPojo>()
    var success = false
    var base_url: String = ""
}

class CommodityPojo : Serializable {
    var id = 0
    var commodity: String = ""
    var variety: String = ""
    var nameGu: String = ""
    var commodityImage: String = ""
}

class CommodityPrice {
    var data = ArrayList<CommodityPricePojo>()
    var success = false
    var priceNotice = ""
}

class CommodityPricePojo : Serializable {
    var districtEn: String = ""
    var districtGu: String = ""
    var marketEnName: String = ""
    var marketGuName: String = ""
    var minPrice: String = ""
    var maxPrice: String = ""
    var modalPrice: String = ""
    var income: String = ""
    var addDate: String = ""
}