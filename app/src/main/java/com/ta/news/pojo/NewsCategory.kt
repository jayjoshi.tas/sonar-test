package com.ta.news.pojo

import java.io.Serializable
import java.util.*

/**
 * Created by arthtilva on 07-Nov-16.
 */
class NewsCategory {
    var data = ArrayList<CategoryPojo>()
    var isSurveyForm = false
    var districtDisplay = false
    var message: String = ""
    var oneSignalId: String = ""
    var success = false
    var url = false
    var base_url: String = ""
}

class CategoryPojo : Serializable {
    var id = 0
    var categoryName: String = ""
    var image: String = ""
    var hintTitle: String = ""
    var hintDescription: String = ""

    //for multi-select in new post
    var isSelected = false
}