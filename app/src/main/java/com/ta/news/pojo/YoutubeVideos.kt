package com.ta.news.pojo

class YoutubeVideos {
    var data: ArrayList<Data> = ArrayList()
    var success = false

    inner class Data {
        var id: String = ""
        var videos_title: String = ""
        var videos_url: String = ""
    }
}