package com.ta.news.pojo

class StateList {
    var data: ArrayList<StatePojo> = ArrayList()

    class StatePojo {
        var id: String = ""
        var nm: String = ""
        var lcl: String = ""
        var url: String = ""
        var lng: String = ""
        var down: String = ""
        var live: Boolean = true
    }
}
//{"data":[{"id":"1","nm":"Gujarat","lcl":"ગુજરાત","url":"http://piplanapane.com/piplanapane/","lng":"gu","live":true,"down":""},{"id":"2","nm":"Maharashtra","lcl":"महाराष्ट्र","url":"http://139.59.64.162/piplanapane/","lng":"mr","live":true,"down":""}]}