package com.ta.news.pojo

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by arthtilva on 06-Sep-18.
 */
//@Table(name = "splashScreens")
class SplashScreens {
    var splashArray = ArrayList<Pojo>()

    inner class Pojo {
        var title: String = ""
        var image: String = ""
        var androidAdvUrl: String = ""
    }
}