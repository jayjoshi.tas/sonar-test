ALTER TABLE OfflineNews ADD COLUMN profileImage TEXT(255) NOT NULL default '';
ALTER TABLE OfflineNews ADD COLUMN userId int NOT NULL default -1;
ALTER TABLE OfflineNews ADD COLUMN isVerified boolean NOT NULL default 0;